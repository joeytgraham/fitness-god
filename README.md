# BreatheEasy

A website that provides air pollution information on major cities around the globe.

## Website
[https://breathe-easy.me](https://breathe-easy.me)

## Team Members
| Name | EID | GitLab ID |
|---|---|---|
| Claire Mathieu  | cam8356 | @clairemat |
| Emaan Haseem | eh27758 | @emaanhaseem |
| Joseph Graham | jtg2474 | @joeytgraham |
| Kiana Fithian | kkf448 | @kfithian |
| Shan Rizvi | shr544 | @shanrizvi2 |

## Project Leader
| Phase | Project Leader |
| --- | --- |
| Phase 1 | Claire Mathieu |
| Phase 2 | Joseph Graham |
| Phase 3 | Emaan Haseem |
| Phase 4 | Kiana Fithian |

## Completion Times
*Time is tracked in hours. Project leader's name is bolded.*
### Phase I:
| Name | Estimated | Actual |
| --- | --- | --- |
| **Claire Mathieu** | 20hrs | 50hrs |
| Emaan Haseem | 10hrs | 20hrs |
| Joseph Graham | 10hrs | 20hrs |
| Kiana Fithian | 15hrs | 20hrs |
| Shan Rizvi | 10hrs | 20hrs |

### Phase II:
| Name | Estimated | Actual |
| --- | --- | --- |
| Claire Mathieu | 30hrs | 59.5hrs |
| Emaan Haseem | 20hrs | 35hrs |
| **Joseph Graham** | 25hrs | 50hrs |
| Kiana Fithian | 35hrs | 45hrs |
| Shan Rizvi | 30hrs | 45hrs |

### Phase III:
| Name | Estimated | Actual |
| --- | --- | --- |
| Claire Mathieu | 30hrs | 39hrs |
| **Emaan Haseem** | 35hrs | 40hrs |
| Joseph Graham | 30hrs | 24hrs |
| Kiana Fithian | 30hrs | 41hrs |
| Shan Rizvi | 20hrs | 35hrs |

### Phase IV:
| Name | Estimated | Actual |
| --- | --- | --- |
| Claire Mathieu | 20hrs | 15hrs |
| Emaan Haseem | 10hrs | 10hrs |
| Joseph Graham | 15hrs | 20hrs |
| **Kiana Fithian** | 20hrs | 14hrs |
| Shan Rizvi | 15hrs | 15hrs |

## Pipelines
https://gitlab.com/joeytgraham/fitness-god/-/pipelines

## Git SHA
| Phase | Git SHA |
| --- | --- |
| Phase 1 | 02ef4c8f2a8f1e198ad2a35849fb211eb1f9bf02 |
| Phase 2 | eb5707fc2a9b5baeee1bf348f4f9792097381ad0 |
| Phase 3 | f5d0853741ac3bd18059b56b1ea3cb50a766e777 |
| Phase 4 | 1a36740c714bc402e8fc240cbf0c6853c4e2e72b |

## Video Presentation
https://youtu.be/2gEPISYYN4E

## How to deploy locally
Attention! This application requires secrets to function. Create a .env.local file in both frontend/ and backend/ with your own appropriate secrets.

#### Using docker (recommended)

##### Building the docker containers
This is required the first time you run the application and every time you add a new dependency.
```
$ make build-docker-images
```

##### Starting the application
```
$ make up
```
the webapp will be availible at http://localhost:3000  
the api will be availible at http://localhost:8080

the webapp will look to read api data from http://localhost:8080

frontend/ is mounted in the frontend container so any changes made in frontend/ will still recompile with npm  
backend/ is mounted in the backend container so any changes made in backend/ will still restart the flask app

##### Stopping the application
in a seperate terminal
```
$ make down
```
this will stop and remove the docker containers holding the frontend and the backend

##### Troubleshooting
if the deployment fails, rebuild the docker images and make sure all dependencies are listed in backend/requirements.txt and frontend/package.json

#### Without docker

##### Deploying the frontend
```
$ cd frontend
$ npm install
$ npm start
```
the webapp will be availible at http://localhost:3000  
the webapp will look to read api data from http://localhost:8080

##### Deploying the backend
```
$ cd backend
$ pip install -r requirements.txt
$ source .env.local
$ python main.py
```
the api will be availible at http://localhost:8080

##### Stopping

both the frontend and backend can be stopped with Control+C
