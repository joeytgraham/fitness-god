from sqlalchemy import create_engine, Column, String, Integer, Date, Float, ForeignKey
from sqlalchemy.orm import sessionmaker, close_all_sessions, declarative_base
import os

"""
A file used to init a local db with sql alchemy using the models of the REST API.
"""

NAME = os.environ["RDS_DB_NAME"]
USER = os.environ["RDS_USERNAME"]
PASSWORD = os.environ["RDS_PASSWORD"]
HOST = os.environ["RDS_HOSTNAME"]
PORT = os.environ["RDS_PORT"]

# an Engine, which the Session will use for connection
# resources
engine = create_engine(f"postgresql+psycopg2://{USER}:{PASSWORD}@{HOST}:{PORT}/{NAME}")

# create a configured "Session" class
Session = sessionmaker(bind=engine)

# create a Session
session = Session()

Base = declarative_base()


class EconomicStatus(Base):
    """
    A class to represent the economic status of a country.
    """

    __tablename__ = "economic_status"
    id = Column(Integer, primary_key=True)
    text = Column(String())

    def __str__(self):
        return f"{self.id}: {self.text}"


class Country(Base):
    """
    A class to represent static information about a country.
    """

    __tablename__ = "countries"
    alpha2code = Column(String(), primary_key=True)
    country_name = Column(String(), nullable=False)
    capital_id = Column(Integer)
    region = Column(String())
    population = Column(Integer)
    latitude = Column(Float)
    longitude = Column(Float)
    economic_status = Column(Integer, ForeignKey("economic_status.id"))
    flag_url = Column(String())


class City(Base):
    """
    A class to represent the static information of a city.
    """

    __tablename__ = "cities"
    id = Column(Integer, primary_key=True)
    city_name = Column(String(), nullable=False)
    region = Column(String())
    country = Column(String(), nullable=False)
    latitude = Column(Float)
    longitude = Column(Float)
    maps_place_photo_reference = Column(String())


class Day(Base):
    """
    A class to represent global air quality averages on a given day.
    """

    __tablename__ = "days"
    id = Column(Date(), primary_key=True)
    avg_aqi = Column(Float)
    avg_co = Column(Float)
    avg_no = Column(Float)
    avg_no2 = Column(Float)
    avg_o3 = Column(Float)
    avg_so2 = Column(Float)
    avg_pm2_5 = Column(Float)
    avg_pm10 = Column(Float)
    avg_nh3 = Column(Float)


class DailyCityInfo(Base):
    """
    A class to represent air quality information for a given city on a given day.
    """

    __tablename__ = "daily_city_info"
    id = Column(Integer, primary_key=True)
    city_id = Column(Integer, ForeignKey("cities.id"))
    date = Column(Date())
    aqi = Column(Float)
    co = Column(Float)
    no = Column(Float)
    no2 = Column(Float)
    o3 = Column(Float)
    so2 = Column(Float)
    pm2_5 = Column(Float)
    pm10 = Column(Float)
    nh3 = Column(Float)


class DailyCountryInfo(Base):
    """
    A class to represent average air quality information of a country on a given day.
    """

    __tablename__ = "daily_country_info"
    id = Column(Integer, primary_key=True)
    country_id = Column(String())
    date = Column(Date())
    avg_aqi = Column(Float)
    avg_co = Column(Float)
    avg_no = Column(Float)
    avg_no2 = Column(Float)
    avg_o3 = Column(Float)
    avg_so2 = Column(Float)
    avg_pm2_5 = Column(Float)
    avg_pm10 = Column(Float)
    avg_nh3 = Column(Float)


def create_all():
    """
    Creates a table for each of the model classes if they do not already exist.
    """
    Base.metadata.create_all(engine)


def drop_all():
    """
    Drops all tables for the model classes if they exist.
    """
    close_all_sessions()
    Base.metadata.drop_all(engine)
