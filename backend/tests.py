# pylint: disable = invalid-name
# pylint: disable = missing-docstring

from unittest import main, TestCase
import datetime
import json
import os
import csv
from sqlalchemy import func

"""
tests.py - This file contains all the python unit tests
"""

# initialize the app
from init_db import (
    session,
    create_all,
    drop_all,
    Country,
    City,
    Day,
    DailyCityInfo,
    DailyCountryInfo,
    EconomicStatus,
)
from main import app

BACKEND_PATH = os.path.dirname(os.path.abspath(__file__))


def load_economic_status():
    """loads in all the economic statuses from test data"""
    with open(BACKEND_PATH + "/test_data/test_data_economic_status.csv") as csvfile:
        reader = csv.reader(csvfile, delimiter=",", quotechar='"')
        # skip the header row
        next(reader)
        for row in reader:
            eco_status = EconomicStatus(id=row[0], text=row[1])
            session.add(eco_status)
        session.commit()


def load_countries():
    """loads in all the countries from our test data"""
    with open(BACKEND_PATH + "/test_data/test_data_countries.csv") as csvfile:
        reader = csv.reader(csvfile, delimiter=",", quotechar='"')
        # skip the header row
        next(reader)
        for row in reader:
            country = Country(
                alpha2code=row[0],
                country_name=row[1],
                capital_id=row[2],
                region=row[3],
                population=row[4],
                latitude=row[5],
                longitude=row[6],
                economic_status=row[7],
                flag_url=row[8],
            )
            session.add(country)
        session.commit()


def load_cities():
    """loads in all the cities from our test data"""
    with open(BACKEND_PATH + "/test_data/test_data_cities.csv") as csvfile:
        reader = csv.reader(csvfile, delimiter=",", quotechar='"')
        # skip the header row
        next(reader)
        for row in reader:
            city = City(
                id=row[0],
                city_name=row[1],
                region=row[2],
                country=row[3],
                latitude=row[4],
                longitude=row[5],
                maps_place_photo_reference=row[6],
            )
            session.add(city)
        session.commit()


def load_daily_city_info():
    """loads in all the daily city information from our test data"""
    with open(BACKEND_PATH + "/test_data/test_data_daily_city_info.csv") as csvfile:
        reader = csv.reader(csvfile, delimiter=",", quotechar='"')
        # skip the header row
        next(reader)
        for row in reader:
            daily_city_info = DailyCityInfo(
                id=row[0],
                city_id=row[1],
                date=row[2],
                aqi=row[3],
                co=row[4],
                no=row[5],
                no2=row[6],
                o3=row[7],
                so2=row[8],
                pm2_5=row[9],
                pm10=row[10],
                nh3=row[11],
            )
            session.add(daily_city_info)
        session.commit()


def load_days():
    """loads in all the days data from our test data"""
    rows = session.query(
        DailyCityInfo.date,
        func.avg(DailyCityInfo.aqi),
        func.avg(DailyCityInfo.co),
        func.avg(DailyCityInfo.no),
        func.avg(DailyCityInfo.no2),
        func.avg(DailyCityInfo.o3),
        func.avg(DailyCityInfo.so2),
        func.avg(DailyCityInfo.pm2_5),
        func.avg(DailyCityInfo.pm10),
        func.avg(DailyCityInfo.nh3),
    ).group_by(DailyCityInfo.date)

    for row in rows:
        day = Day(
            id=row[0],
            avg_aqi=row[1],
            avg_co=row[2],
            avg_no=row[3],
            avg_no2=row[4],
            avg_o3=row[5],
            avg_so2=row[6],
            avg_pm2_5=row[7],
            avg_pm10=row[8],
            avg_nh3=row[9],
        )

        session.add(day)

    session.commit()


def load_daily_country_info():
    """loads in all the daily country info from our test data"""
    rows = (
        session.query(
            City.country,
            DailyCityInfo.date,
            func.avg(DailyCityInfo.aqi),
            func.avg(DailyCityInfo.co),
            func.avg(DailyCityInfo.no),
            func.avg(DailyCityInfo.no2),
            func.avg(DailyCityInfo.o3),
            func.avg(DailyCityInfo.so2),
            func.avg(DailyCityInfo.pm2_5),
            func.avg(DailyCityInfo.pm10),
            func.avg(DailyCityInfo.nh3),
        )
        .join(DailyCityInfo, DailyCityInfo.city_id == City.id)
        .group_by(City.country, DailyCityInfo.date)
    )

    for row in rows:
        daily_country_info = DailyCountryInfo(
            country_id=row[0],
            date=row[1],
            avg_aqi=row[2],
            avg_co=row[3],
            avg_no=row[4],
            avg_no2=row[5],
            avg_o3=row[6],
            avg_so2=row[7],
            avg_pm2_5=row[8],
            avg_pm10=row[9],
            avg_nh3=row[10],
        )
        session.add(daily_country_info)
    session.commit()


def eco_to_int(economic_status_string):
    """converts given economic status string into an integer"""
    if economic_status_string == "Low income":
        return 0
    elif economic_status_string == "Lower middle income":
        return 1
    elif economic_status_string == "Upper middle income":
        return 2
    elif economic_status_string == "High income":
        return 3
    else:
        return -1


class Tests(TestCase):
    """main test class"""

    @classmethod
    def setUpClass(cls):
        """connect to the database and load the test data"""
        create_all()
        load_economic_status()
        load_countries()
        load_cities()
        load_daily_city_info()
        load_daily_country_info()
        load_days()
        cls.client = app.test_client()

    # --------------
    # Helper Methods
    # --------------

    def validate_aq_data(self, aq_data):
        """helper method used for checking if air quality data is valid"""
        datetime.datetime.strptime(aq_data["date"], "%Y-%m-%d")
        self.assertGreaterEqual(aq_data["aqi"], 0)
        self.assertLessEqual(aq_data["aqi"], 500)
        self.assertGreaterEqual(aq_data["co"], 0)
        self.assertGreaterEqual(aq_data["nh3"], 0)
        self.assertGreaterEqual(aq_data["no"], 0)
        self.assertGreaterEqual(aq_data["no2"], 0)
        self.assertGreaterEqual(aq_data["o3"], 0)
        self.assertGreaterEqual(aq_data["pm10"], 0)
        self.assertGreaterEqual(aq_data["pm2_5"], 0)
        self.assertGreaterEqual(aq_data["so2"], 0)

    def validate_sorted(self, response, fields):
        """
        validate that data was correctly sorted by the api
        fields: list of two-element tuples where the first element is the name
            of the field and the second element is a boolean, True for asc,
            False for desc
        """
        if len(response) != 0:
            old_values = [None] * len(fields)
            for index in range(len(fields)):
                old_values[index] = response[0][fields[index][0]]

        for instance in response:
            # check the first sort condition
            if fields[0][1] == "asc":
                self.assertGreaterEqual(instance[fields[0][0]], old_values[0])
            else:
                self.assertLessEqual(instance[fields[0][0]], old_values[0])

            # check tie-breaking conditions
            index = 1
            # only check condition if previous condition was tied
            while (
                index < len(fields)
                and old_values[index - 1] == instance[fields[index - 1][0]]
            ):
                if fields[index][1] == "asc":
                    self.assertGreaterEqual(
                        instance[fields[index][0]], old_values[index]
                    )
                else:
                    self.assertLessEqual(instance[fields[index][0]], old_values[index])
                index += 1

            # update the old_values
            for index in range(len(fields)):
                old_values[index] = instance[fields[index][0]]

    def run_sort_tests(self, url, sort_order_lists):
        """
        run tests again the sort feature of the API
        url: the target endpoint
        sort_order_lists: a list of of lists of two-element tuples where the
            first element is the name of the field and the second element
            is a boolean, True for asc, False for desc
        """
        for order_list in sort_order_lists:
            with self.subTest(id=order_list):
                params = "&".join(
                    ["sort_by=" + sort[1] + "(" + sort[0] + ")" for sort in order_list]
                )

                response = self.client.get(url + "?" + params)
                d = json.loads(response.get_data(as_text=True))
                self.assertEqual(response.status_code, 200)
                self.validate_sorted(d["results"], order_list)

    def validate_filtered(self, response, filter_list):
        """
        validate that the API response was filtered correctly
        filters: list of three-element tuples where the first element is the name
              of the field and the second element is a string specifying the
              operator to compare the fields and the third element is the
              value to compare the field with
        """
        for instance in response:
            # there is an and/or
            if len(filter_list) > 2:
                if filter_list[0] == "and":
                    # just loop thru list starting past 1, check as normal
                    for f in filter_list[1:]:
                        as_operator = {
                            "eq": self.assertEqual,
                            "neq": self.assertNotEqual,
                            "lte": self.assertLessEqual,
                            "lt": self.assertLess,
                            "gte": self.assertGreaterEqual,
                            "gt": self.assertGreater,
                        }
                        op = as_operator.get(f[1])
                        op(instance[f[0]], f[2])
            # no and/or, verify as normal
            else:
                for f in filter_list:
                    as_operator = {
                        "eq": self.assertEqual,
                        "neq": self.assertNotEqual,
                        "lte": self.assertLessEqual,
                        "lt": self.assertLess,
                        "gte": self.assertGreaterEqual,
                        "gt": self.assertGreater,
                    }
                    op = as_operator.get(f[1])
                    op(instance[f[0]], f[2])

    def run_filtered_tests(self, url, filter_lists):
        """
        runs through a bunch of tests that tests filtering data
        """
        for filter_list in filter_lists:
            with self.subTest(id=filter_list):
                # this has an and/or operator
                # print("FILTER LIST : " + str(filter_list))
                # print(filter_list[0])
                if len(filter_list) > 2:
                    # print("GOT HERE \n\n\n")
                    params = (
                        "filter="
                        + filter_list[0]
                        + "("
                        + ",".join(
                            [
                                f[1] + "(" + f[0] + "," + str(f[2]) + ")"
                                for f in filter_list[1:]
                            ]
                        )
                        + ")"
                    )
                else:
                    params = "filter=" + ",".join(
                        [f[1] + "(" + f[0] + "," + str(f[2]) + ")" for f in filter_list]
                    )
                # print("\n\nPARAMS = \n\n" + params)
                response = self.client.get(url + "?" + params)
                d = json.loads(response.get_data(as_text=True))
                self.assertEqual(response.status_code, 200)
                # print("\n\nRESULTS = \n\n" + str(d["results"]))
                self.validate_filtered(d["results"], filter_list)

    # -------------
    # General Tests
    # -------------

    def test_health_check(self):
        """
        check that the health check route works properly
        """
        url = "/"

        response = self.client.get(url)
        self.assertEqual(
            response.get_data(as_text=True), "Welcome to the BreatheEasy API."
        )
        self.assertEqual(response.status_code, 200)

    def test_query_param_parse(self):
        """
        test that query paramaters are being parsed in
        """
        url = "/api/countries"

        params1 = {"sort_by": "asc(name)"}
        params2 = {"sort_by[]": "asc(name)"}

        response = self.client.get(url, query_string=params1)
        d1 = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, 200)

        response = self.client.get(url, query_string=params2)
        d2 = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(d1, d2)

    def test_query_param_combine(self):
        """tests that we can get multiple query parameters for one call"""
        url = "/api/countries"

        params1 = {"sort_by": "asc(region)", "sort_by[]": "asc(latest_aqi)"}

        response = self.client.get(url, query_string=params1)
        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, 200)
        country_iter = iter(d["results"])
        first_country = next(country_iter)
        old_region = first_country["region"]
        old_aqi = first_country["latest_aqi"]
        for country in country_iter:
            self.assertGreaterEqual(country["region"], old_region)
            if country["region"] == old_region:
                self.assertGreaterEqual(country["latest_aqi"], old_aqi)
            old_region = country["region"]
            old_aqi = country["latest_aqi"]

    def test_sort_empty_list(self):
        """test that we can sort a list that is empty with no issues"""
        url = "/api/countries"
        params = {"sort_by": "asc(economic_status)", "filter": "eq(region, Antarctica)"}
        response = self.client.get(url, query_string=params)
        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, 200)
        self.validate_filtered(d["results"], [("region", "eq", "Antarctica")])
        self.validate_sorted(d["results"], [("economic_status", "asc")])

    # ---------
    # Countries
    # ---------

    def test_countries_num_results(self):
        """Test to make sure we have the expected number of countries in response"""
        url = "/api/countries"

        response = self.client.get(url)
        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(d["results"]), d["size"])

    def test_countries_all(self):
        """Test entries of countries is correct"""
        url = "/api/countries"

        response = self.client.get(url)
        d = json.loads(response.get_data(as_text=True))
        for entry in d["results"]:
            with self.subTest(id=entry["id"]):
                self.assertIsInstance(entry["capital"], str)
                self.assertIn(
                    entry["economic_status"],
                    [
                        "N/A",
                        "High income",
                        "Upper middle income",
                        "Lower middle income",
                        "Low income",
                    ],
                )
                self.assertIsInstance(entry["flag_url"], str)
                self.assertIsInstance(entry["id"], str)
                self.assertEqual(len(entry["id"]), 2)
                self.assertLessEqual(entry["lat"], 90)
                self.assertGreaterEqual(entry["lat"], -90)
                self.assertGreaterEqual(entry["latest_aqi"], 0)
                self.assertLessEqual(entry["latest_aqi"], 500)
                self.assertLessEqual(entry["lng"], 180)
                self.assertGreaterEqual(entry["lng"], -180)
                self.assertIsInstance(entry["name"], str)
                self.assertIsInstance(entry["population"], int)
                self.assertGreater(entry["population"], 0)
                self.assertIn(
                    entry["region"],
                    ["Europe", "Asia", "Africa", "Americas", "Oceania", "Antartica"],
                )

    def test_countries_instance(self):
        """Test expected country response given alpha2code id"""
        url = "/api/countries/US"

        response = self.client.get(url)
        result = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(result["capital"], "Washington")
        self.assertEqual(result["cities"], [{"id": 248, "name": "Washington"}])
        self.assertEqual(result["economic_status"], "High income")
        self.assertEqual(result["flag_url"], "https://restcountries.eu/data/usa.svg")
        self.assertEqual(result["id"], "US")
        self.assertEqual(result["lat"], 38)
        self.assertEqual(result["latest_aqi"], 30.556)
        self.assertEqual(result["lng"], -97)
        self.assertEqual(result["name"], "United States of America")
        self.assertEqual(result["population"], 323947000)
        self.assertEqual(result["region"], "Americas")
        # create a datetime object to verify that the date is formatted correctly
        self.assertEqual(result["worst_day"], "2020-11-28")

    def test_countries_error_result(self):
        """Test expected error response given invalid country alpha2code id"""
        url = "/api/countries/ZZ"

        response = self.client.get(url)
        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, 404)
        self.assertEqual(d, {"error": "Could not find country with id ZZ"})

    def test_countries_limit(self):
        """tests that valid limit value works for countries"""
        url = "/api/countries?limit=5"

        response = self.client.get(url)
        result = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(result["results"]), 5)
        self.assertEqual(result["limit"], 5)
        self.assertEqual(result["size"], 5)
        self.assertEqual(result["offset"], 0)
        self.assertEqual(result["total_count"], 10)

    def test_countires_limit_value_error(self):
        """tests that an invalid limit value results in an error"""
        url = "/api/countries?limit=goofygoober"

        response = self.client.get(url)
        result = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(result, {"error": "goofygoober is an invalid value for limit"})

    def test_countires_limit_negative_error(self):
        """tests that a negative limit results in an error"""
        url = "/api/countries?limit=-10"

        response = self.client.get(url)
        result = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(result, {"error": "-10 is an invalid value for limit"})

    def test_countires_offset(self):
        """tests that a valid offset value for countries works"""
        url = "/api/countries?offset=5&sort_by=asc(id)"

        response = self.client.get(url)
        result = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(result["limit"], 20)
        self.assertEqual(result["offset"], 5)
        self.assertEqual(result["results"][0].get("name"), "Japan")
        self.assertEqual(result["size"], 5)
        self.assertEqual(result["total_count"], 10)

    def test_countires_offset_value_error(self):
        """
        test that an invalid offset value for countires results in an error
        """
        url = "/api/countries?offset=cabobobs"

        response = self.client.get(url)
        result = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(result, {"error": "cabobobs is an invalid value for offset"})

    def test_countires_offset_more_than_total(self):
        """
        test that an offset value more than the total number of countries results in an error
        """
        url = "/api/countries?offset=2000000"

        response = self.client.get(url)
        result = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, 404)
        self.assertEqual(result, {"error": "2000000 is an invalid value for offset"})

    def test_countires_offset_negative(self):
        """test that a negative offset value for countires results in an error"""
        url = "/api/countries?offset=-10"

        response = self.client.get(url)
        result = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(result, {"error": "-10 is an invalid value for offset"})

    def test_countires_limit_zero_offset_not_zero_error(self):
        """test that when limit is 0, any positive offset value for countires results in an error"""
        url = "/api/countries?limit=0&offset=5"

        response = self.client.get(url)
        result = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, 404)
        self.assertEqual(result, {"error": "5 is an invalid value for offset"})

    def test_countires_limit_zero_offset_zero(self):
        """test that when limit is 0, 0 offset value for countires is valid"""
        url = "/api/countries?limit=0&offset=0"

        response = self.client.get(url)
        result = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(result["limit"], 0)
        self.assertEqual(result["offset"], 0)
        self.assertEqual(result["results"], [])
        self.assertEqual(result["size"], 0)
        self.assertEqual(result["total_count"], 10)

    def test_countries_sort(self):
        """test that sorting works for countries"""
        url = "/api/countries"
        orders = [
            [("capital", "asc")],
            [("flag_url", "asc")],
            [("id", "asc")],
            [("lat", "asc")],
            [("latest_aqi", "asc")],
            [("lng", "asc")],
            [("name", "asc")],
            [("population", "asc")],
            [("region", "asc")],
            [("capital", "desc")],
            [("flag_url", "desc")],
            [("id", "desc")],
            [("lat", "desc")],
            [("latest_aqi", "desc")],
            [("lng", "desc")],
            [("name", "desc")],
            [("population", "desc")],
            [("region", "desc")],
            [("region", "asc"), ("name", "desc")],
        ]

        self.run_sort_tests(url, orders)

    def test_countries_sort_eco_status(self):
        """test that sorting the countries by economic status works"""
        url = "/api/countries"
        directions = {"asc": self.assertGreaterEqual, "desc": self.assertLessEqual}
        for direction in directions:
            params = {"sort_by": f"{direction}(economic_status)"}
            response = self.client.get(url, query_string=params)
            d = json.loads(response.get_data(as_text=True))
            self.assertEqual(response.status_code, 200)

            last_status = eco_to_int(d["results"][0]["economic_status"])
            for country in d["results"]:
                new_status = eco_to_int(country["economic_status"])
                directions.get(direction)(new_status, last_status)
                last_status = new_status

    def test_countries_invalid_sort(self):
        """verify that an invalid sort results in an error"""
        url = "/api/countries"
        params = {"sort_by": "hello"}

        response = self.client.get(url, query_string=params)
        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(d, {"error": "hello is an invalid value for sort_by"})

    def test_countries_filter(self):
        """test that filtering countries works"""
        url = "/api/countries"
        filter_lists = [
            [("capital", "lte", "M")],
            [("flag_url", "gte", "X")],
            [("id", "gt", "B")],
            [("lat", "lt", 0)],
            [("latest_aqi", "gte", 50)],
            [("lng", "gt", 0)],
            [("name", "gt", "M")],
            [("population", "lt", 60000)],
            [("region", "neq", "Americas")],
            [("region", "eq", "Americas")],
            [("and"), ("region", "neq", "Americas"), ("region", "neq", "Asia")],
        ]

        self.run_filtered_tests(url, filter_lists)

    def test_countries_filter_eco_status(self):
        """test that filtering countries by economic status works"""
        url = "/api/countries"
        operators = {
            "eq": self.assertEqual,
            "neq": self.assertNotEqual,
            "lte": self.assertLessEqual,
            "lt": self.assertLess,
            "gte": self.assertGreaterEqual,
            "gt": self.assertGreater,
        }
        for op in operators:
            params = {"filter": f"{op}(economic_status, Upper middle income)"}
            response = self.client.get(url, query_string=params)
            d = json.loads(response.get_data(as_text=True))
            self.assertEqual(response.status_code, 200)

            for country in d["results"]:
                operators.get(op)(
                    eco_to_int(country["economic_status"]),
                    eco_to_int("Upper middle income"),
                )

    def test_countries_invalid_filter(self):
        """test that an invalid filter results in an error for countries"""
        url = "/api/countries"
        params = {"filter": "gte(economic_status, Upper income)"}

        response = self.client.get(url, query_string=params)
        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            d, {"error": "Upper income is invalid value for economic_status"}
        )

    # ------
    # Cities
    # ------

    def test_cities_num_results(self):
        """Test to make sure we have the expected number of cities in response"""
        url = "/api/cities"

        response = self.client.get(url)
        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(d["results"]), d["size"])

    def test_cities_all(self):
        """Test entries of cities is correct"""
        url = "/api/cities"

        response = self.client.get(url)
        d = json.loads(response.get_data(as_text=True))
        for entry in d["results"]:
            with self.subTest(id=entry["id"]):
                self.assertIsInstance(entry["country"], str)
                self.assertEqual(len(entry["country"]), 2)
                self.assertGreaterEqual(entry["id"], 0)
                self.assertLessEqual(entry["lat"], 90)
                self.assertGreaterEqual(entry["lat"], -90)
                self.assertGreaterEqual(entry["latest_aqi"], 0)
                self.assertLessEqual(entry["latest_aqi"], 500)
                self.assertLessEqual(entry["lng"], 180)
                self.assertGreaterEqual(entry["lng"], -180)
                self.assertIsInstance(entry["name"], str)

    def test_city(self):
        """Test individual cities is correct"""
        url = "/api/cities/248"

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        entry = json.loads(response.get_data(as_text=True))
        self.assertEqual(entry["country"], "US")
        self.assertEqual(entry["name"], "Washington")
        self.assertEqual(entry["region"], "DC")
        self.assertEqual(entry["id"], 248)
        self.assertEqual(entry["lat"], 38.895)
        self.assertEqual(entry["lng"], -77.037)
        self.assertEqual(
            entry["maps_place_photo_ref"],
            "ATtYBwLWWDJhcBLyAJDtrPWiGL75SItrEhI5pepKtRGCd3KuCKfAFcnSvyol9-WfgC41ZMpmaH08SBGsO4HVX_TK2nZgv0ykKiI94JBoP5SU9bvpwZ3t0MQtPJSuSgAkc9I5bhFDhfXTRPhUALhaXUOP_90UGU8WW2t81QeVnOL2cuAtEqam",
        )
        self.assertEqual(
            entry["past_aq_data"],
            [
                {
                    "aqi": 30.556,
                    "co": 297.07,
                    "date": "2020-12-03",
                    "nh3": 1.38,
                    "no": 0.0,
                    "no2": 21.08,
                    "o3": 67.95,
                    "pm10": 5.92,
                    "pm2_5": 4.38,
                    "so2": 5.9,
                },
                {
                    "aqi": 20.370,
                    "co": 260.35,
                    "date": "2020-12-02",
                    "nh3": 0.88,
                    "no": 0.01,
                    "no2": 13.54,
                    "o3": 45.78,
                    "pm10": 1.7,
                    "pm2_5": 0.95,
                    "so2": 3.52,
                },
                {
                    "aqi": 17.917,
                    "co": 270.37,
                    "date": "2020-12-01",
                    "nh3": 0.35,
                    "no": 0.17,
                    "no2": 14.22,
                    "o3": 18.95,
                    "pm10": 5.41,
                    "pm2_5": 4.36,
                    "so2": 1.7,
                },
                {
                    "aqi": 43.75,
                    "co": 360.49,
                    "date": "2020-11-30",
                    "nh3": 1.05,
                    "no": 0.05,
                    "no2": 23.99,
                    "o3": 32.9,
                    "pm10": 13.57,
                    "pm2_5": 10.58,
                    "so2": 2.38,
                },
                {
                    "aqi": 30.0,
                    "co": 330.45,
                    "date": "2020-11-29",
                    "nh3": 1.41,
                    "no": 0.01,
                    "no2": 25.71,
                    "o3": 45.06,
                    "pm10": 10.27,
                    "pm2_5": 7.24,
                    "so2": 5.01,
                },
                {
                    "aqi": 119.960,
                    "co": 1468.66,
                    "date": "2020-11-28",
                    "nh3": 9.63,
                    "no": 180.6,
                    "no2": 41.13,
                    "o3": 0.0,
                    "pm10": 53.62,
                    "pm2_5": 43.25,
                    "so2": 6.08,
                },
                {
                    "aqi": 51.0,
                    "co": 594.14,
                    "date": "2020-11-27",
                    "nh3": 2.57,
                    "no": 20.79,
                    "no2": 54.84,
                    "o3": 0.02,
                    "pm10": 16.07,
                    "pm2_5": 12.15,
                    "so2": 4.47,
                },
            ],
        )

    def test_cities_error_result(self):
        """Test expected error response given invalid city id"""
        url = "/api/cities/0"

        response = self.client.get(url)
        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, 404)
        self.assertEqual(d, {"error": "Could not find city with id 0"})

    def test_cities_limit(self):
        """tests that a valid limit value works for cities"""
        url = "/api/cities?limit=5"

        response = self.client.get(url)
        result = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(result["results"]), 5)
        self.assertEqual(result["limit"], 5)
        self.assertEqual(result["size"], 5)
        self.assertEqual(result["offset"], 0)
        self.assertEqual(result["size"], 5)
        self.assertEqual(result["total_count"], 10)

    def test_cities_limit_value_error(self):
        """tests that an invalid limit value for cities results in an error"""
        url = "/api/cities?limit=goofygoober"

        response = self.client.get(url)
        result = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(result, {"error": "goofygoober is an invalid value for limit"})

    def test_cities_limit_negative_error(self):
        """tests that a negative limit value for cities results in an error"""
        url = "/api/cities?limit=-10"

        response = self.client.get(url)
        result = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(result, {"error": "-10 is an invalid value for limit"})

    def test_cities_offset(self):
        """tests that a valid offset value for cities works"""
        url = "/api/cities?offset=5"

        response = self.client.get(url)
        result = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(result["limit"], 20)
        self.assertEqual(result["offset"], 5)
        self.assertEqual(result["results"][0].get("name"), "Tokyo")
        self.assertEqual(result["size"], 5)
        self.assertEqual(result["total_count"], 10)

    def test_cities_offset_value_error(self):
        """test that an invalid offset value for cities results in an error"""
        url = "/api/cities?offset=cabobobs"

        response = self.client.get(url)
        result = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(result, {"error": "cabobobs is an invalid value for offset"})

    def test_cities_offset_more_than_total(self):
        """test that an offset value more than the total number of cities results in an error"""
        url = "/api/cities?offset=2000000"

        response = self.client.get(url)
        result = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, 404)
        self.assertEqual(result, {"error": "2000000 is an invalid value for offset"})

    def test_cities_sort(self):
        """test that sorting works for cities"""
        url = "/api/cities"
        orders = [
            [("country", "asc")],
            [("id", "asc")],
            [("lat", "asc")],
            [("latest_aqi", "asc")],
            [("lng", "asc")],
            [("maps_place_photo_ref", "asc")],
            [("name", "asc")],
            [("region", "asc")],
            [("country", "desc")],
            [("id", "desc")],
            [("lat", "desc")],
            [("latest_aqi", "desc")],
            [("lng", "desc")],
            [("maps_place_photo_ref", "desc")],
            [("name", "desc")],
            [("region", "desc")],
            [("country", "asc"), ("id", "desc")],
        ]

        self.run_sort_tests(url, orders)

    def test_cities_invalid_sort(self):
        """verify that an invalid sort results in an error"""
        url = "/api/cities"
        params = {"sort_by": "hello"}

        response = self.client.get(url, query_string=params)
        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(d, {"error": "hello is an invalid value for sort_by"})

    def test_cities_filter(self):
        """test that filtering cities works"""
        url = "/api/cities"
        filter_lists = [
            [("country", "lte", "M")],
            [("id", "gt", 3000)],
            [("lat", "lt", 0)],
            [("latest_aqi", "gte", 50)],
            [("lng", "gt", 0)],
            [("name", "gt", "M")],
            [("maps_place_photo_ref", "lt", "X")],
            [("name", "neq", "Chicago")],
            [("region", "eq", "TX")],
            [("and"), ("region", "neq", "TX"), ("region", "neq", "CA")],
        ]

        self.run_filtered_tests(url, filter_lists)

    def test_cities_invalid_filter(self):
        """test that an invalid filter results in an error for cities"""
        url = "/api/cities"
        params = {"filter": "eq(id,zzz(123))"}

        response = self.client.get(url, query_string=params)
        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(d, {"error": "zzz(123) is invalid value for id"})

    # ----
    # Days
    # ----

    def test_days_num_results(self):
        """Test to make sure we have the expected number of days in response"""
        url = "/api/days"

        response = self.client.get(url)
        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(d["results"]), d["size"])

    def test_days_all(self):
        """Test entries of days is correct"""
        url = "/api/days"

        response = self.client.get(url)
        d = json.loads(response.get_data(as_text=True))
        for entry in d["results"]:
            with self.subTest(id=entry["date"]):
                self.validate_aq_data(entry)

    def test_day(self):
        """Test individual day is correct"""
        url = "/api/days/2020-11-27"

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        entry = json.loads(response.get_data(as_text=True))
        self.assertEqual(entry["date"], "2020-11-27")
        self.assertEqual(entry["aqi"], 111.663)
        self.assertEqual(entry["co"], 1069.451)
        self.assertEqual(entry["nh3"], 7.064)
        self.assertEqual(entry["no"], 8.733)
        self.assertEqual(entry["no2"], 32.132)
        self.assertEqual(entry["o3"], 15.925)
        self.assertEqual(entry["pm10"], 71.276)
        self.assertEqual(entry["pm2_5"], 53.347)
        self.assertEqual(entry["so2"], 13.626)
        self.assertEqual(
            entry["worst_cities"],
            [
                {"id": 43, "lat": 23.723, "lng": 90.409, "name": "Dhaka"},
                {"id": 169, "lat": 9.181, "lng": 7.179, "name": "Abuja"},
                {"id": 175, "lat": 33.69, "lng": 73.055, "name": "Islamabad"},
                {"id": 116, "lat": 28.6, "lng": 77.2, "name": "New Delhi"},
                {"id": 117, "lat": -6.174, "lng": 106.829, "name": "Jakarta"},
                {"id": 68, "lat": 35.209, "lng": 110.733, "name": "Beijing"},
                {"id": 188, "lat": 55.752, "lng": 37.616, "name": "Moscow"},
                {"id": 248, "lat": 38.895, "lng": -77.037, "name": "Washington"},
                {"id": 126, "lat": 35.685, "lng": 139.751, "name": "Tokyo"},
                {"id": 54, "lat": -16.212, "lng": -44.431, "name": "Brasília"},
            ],
        )
        self.assertEqual(
            entry["worst_countries"],
            [
                {"id": "BD", "lat": 24.0, "lng": 90.0, "name": "Bangladesh"},
                {"id": "NG", "lat": 10.0, "lng": 8.0, "name": "Nigeria"},
                {"id": "PK", "lat": 30.0, "lng": 70.0, "name": "Pakistan"},
                {"id": "IN", "lat": 20.0, "lng": 77.0, "name": "India"},
                {"id": "ID", "lat": -5.0, "lng": 120.0, "name": "Indonesia"},
                {"id": "CN", "lat": 35.0, "lng": 105.0, "name": "China"},
                {"id": "RU", "lat": 60.0, "lng": 100.0, "name": "Russian Federation"},
                {
                    "id": "US",
                    "lat": 38.0,
                    "lng": -97.0,
                    "name": "United States of America",
                },
                {"id": "JP", "lat": 36.0, "lng": 138.0, "name": "Japan"},
                {"id": "BR", "lat": -10.0, "lng": -55.0, "name": "Brazil"},
            ],
        )

    def test_days_poor_format(self):
        """Test expected error response given improperly formatted day id"""
        url = "/api/days/2020-13-26"

        response = self.client.get(url)
        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            d, {"error": "Date 2020-13-26 is not formatted correctly (try yyyy-mm-dd)"}
        )

    def test_days_invalid_date(self):
        """Test expected error response given invalid date"""
        url = "/api/days/2020-11-26"

        response = self.client.get(url)
        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, 404)
        self.assertEqual(d, {"error": "Could not find day with id 2020-11-26"})

    def test_days_limit(self):
        """tests that a valid limit value works for days"""
        url = "/api/days?limit=5"

        response = self.client.get(url)
        result = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(result["results"]), 5)
        self.assertEqual(result["limit"], 5)
        self.assertEqual(result["offset"], 0)
        self.assertEqual(result["size"], 5)
        self.assertEqual(result["total_count"], 7)

    def test_days_limit_value_error(self):
        """tests that an invalid limit value for days results in an error"""
        url = "/api/days?limit=goofygoober"

        response = self.client.get(url)
        result = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(result, {"error": "goofygoober is an invalid value for limit"})

    def test_days_limit_negative_error(self):
        """tests that a negative limit value for days results in an error"""
        url = "/api/days?limit=-10"

        response = self.client.get(url)
        result = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(result, {"error": "-10 is an invalid value for limit"})

    def test_days_offset(self):
        """tests that a valid offset value for days works"""
        url = "/api/days?offset=5&sort_by=asc(date)"

        response = self.client.get(url)
        result = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(result["limit"], 20)
        self.assertEqual(result["offset"], 5)
        self.assertEqual(result["results"][0].get("date"), "2020-12-02")
        self.assertEqual(result["size"], 2)
        self.assertEqual(result["total_count"], 7)

    def test_days_sort(self):
        """test that sorting works for days"""
        url = "/api/days"
        orders = [
            [("aqi", "asc")],
            [("co", "asc")],
            [("nh3", "asc")],
            [("no", "asc")],
            [("no2", "asc")],
            [("o3", "asc")],
            [("pm10", "asc")],
            [("pm2_5", "asc")],
            [("so2", "asc")],
            [("aqi", "desc")],
            [("co", "desc")],
            [("nh3", "desc")],
            [("no", "desc")],
            [("no2", "desc")],
            [("o3", "desc")],
            [("pm10", "desc")],
            [("pm2_5", "desc")],
            [("so2", "desc")],
            [("aqi", "asc"), ("pm2_5", "asc")],
        ]

        self.run_sort_tests(url, orders)

    def test_days_sort_date(self):
        """test that sorting days by date works"""
        url = "/api/days"
        directions = {"asc": self.assertGreaterEqual, "desc": self.assertLessEqual}
        for direction in directions:
            params = {"sort_by": f"{direction}(date)"}
            response = self.client.get(url, query_string=params)
            d = json.loads(response.get_data(as_text=True))
            self.assertEqual(response.status_code, 200)
            # skip first result
            last_status = datetime.datetime.strptime(
                d["results"][0]["date"], "%Y-%m-%d"
            ).date()
            for day in d["results"]:
                new_status = datetime.datetime.strptime(day["date"], "%Y-%m-%d").date()
                directions.get(direction)(new_status, last_status)
                last_status = new_status

    def test_days_invalid_sort(self):
        """verify that an invalid sort results in an error"""
        url = "/api/days"
        params = {"sort_by": "hello"}

        response = self.client.get(url, query_string=params)
        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(d, {"error": "hello is an invalid value for sort_by"})

    def test_days_filter(self):
        """test that filtering days works"""
        url = "/api/days"
        filter_lists = [
            [("aqi", "gte", 50)],
            [("co", "gte", 50)],
            [("nh3", "gte", 50)],
            [("no", "gte", 50)],
            [("no2", "gte", 50)],
            [("o3", "gte", 50)],
            [("pm10", "gte", 50)],
            [("pm2_5", "gte", 50)],
            [("so2", "gte", 50)],
        ]

        self.run_filtered_tests(url, filter_lists)

    def test_days_invalid_filter(self):
        """test that an invalid filter results in an error for days"""
        url = "/api/days"
        params = {"filter": "gte(date, 2020-12-32)"}

        response = self.client.get(url, query_string=params)
        d = json.loads(response.get_data(as_text=True))
        self.assertEqual(response.status_code, 400)
        self.assertEqual(d, {"error": "2020-12-32 is invalid value for date"})

    @classmethod
    def tearDownClass(cls):
        """drop all the tables that were created for testing purposes"""
        drop_all()


if __name__ == "__main__":  # pragma: no cover
    main()
