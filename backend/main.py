# pylint: disable = invalid-name
# pylint: disable = missing-docstring

import datetime
import json

from flask import Flask  # type: ignore
from flask_cors import CORS  # type: ignore
from flask import Response, jsonify, request
from sqlalchemy import func  # type: ignore

from init_db import (
    session,
    Country,
    City,
    Day,
    DailyCityInfo,
    DailyCountryInfo,
    EconomicStatus,
)
import parse_query
from response_fields import country_fields, city_fields, day_fields

"""
Main.py - This file holds all of our API endpoints which communicate with our
database and provide data to the frontend.
"""

app = Flask(__name__)
CORS(app)

PRECISION = 3


@app.route("/")
def home():
    """
    NOTE: This route is needed for the default EB health check route
    """
    return "Welcome to the BreatheEasy API."


@app.route("/api/countries", methods=["GET"])
def get_countries():
    """
    Gets information about all countries.
    """
    limit_offset = parse_query.get_limit_offset(request.args)

    # check that limit_offset didn't return a response, else error
    if type(limit_offset) is Response:
        return limit_offset

    latest_day = session.query(func.max(DailyCountryInfo.date)).first()[0]

    order_by = parse_query.get_orders(country_fields, request.args)

    # check that order_by didn't return a response, else error
    if type(order_by) is Response:
        return order_by

    filters = parse_query.get_filters(country_fields, request.args)

    # check that filters didn't return a response, else error
    if type(filters) is Response:
        return filters

    query = (
        session.query(
            Country.alpha2code,
            Country.country_name,
            City.city_name,
            Country.region,
            Country.population,
            Country.latitude,
            Country.longitude,
            EconomicStatus.text,
            Country.flag_url,
            DailyCountryInfo.avg_aqi,
        )
        .join(EconomicStatus, Country.economic_status == EconomicStatus.id)
        .join(DailyCountryInfo, Country.alpha2code == DailyCountryInfo.country_id)
        .join(City, City.id == Country.capital_id)
        .filter(DailyCountryInfo.date == latest_day)
        .filter(*filters)
    )

    countries = query.order_by(*order_by).limit(limit_offset[0]).offset(limit_offset[1])

    total_count = query.count()

    if countries.count() == 0 and limit_offset[1] != 0:
        return Response(
            json.dumps(
                {
                    "error": f"{request.args.get('offset')} is an invalid value for offset"
                }
            ),
            404,
        )

    countries_dicts = [
        {
            "id": row[0],
            "name": row[1],
            "capital": row[2],
            "region": row[3],
            "population": row[4],
            "lat": round(row[5], PRECISION),
            "lng": round(row[6], PRECISION),
            "economic_status": row[7],
            "flag_url": row[8],
            "latest_aqi": round(row[9], PRECISION),
        }
        for row in countries
    ]

    return jsonify(
        {
            "size": len(countries_dicts),
            "results": countries_dicts,
            "total_count": total_count,
            "limit": limit_offset[0],
            "offset": limit_offset[1],
        }
    )


@app.route("/api/countries/<id>", methods=["GET"])
def get_country(id):
    """Get information for a particular country."""
    latest_day = (
        session.query(func.max(DailyCountryInfo.date))
        .filter(DailyCountryInfo.country_id == id)
        .first()
    )

    if latest_day[0] is None:
        return Response(
            json.dumps({"error": f"Could not find country with id {id}"}), 404
        )

    country = (
        session.query(
            Country.alpha2code,
            Country.country_name,
            City.city_name,
            Country.region,
            Country.population,
            Country.latitude,
            Country.longitude,
            EconomicStatus.text,
            Country.flag_url,
            DailyCountryInfo.avg_aqi,
        )
        .join(EconomicStatus, Country.economic_status == EconomicStatus.id)
        .join(DailyCountryInfo, Country.alpha2code == DailyCountryInfo.country_id)
        .join(City, City.id == Country.capital_id)
        .filter(DailyCountryInfo.date == latest_day[0], Country.alpha2code == id)
        .first()
    )

    cities = session.query(City.id, City.city_name).filter(City.country == id)
    worst_day = (
        session.query(DailyCountryInfo.date, DailyCountryInfo.avg_aqi)
        .filter(DailyCountryInfo.country_id == id)
        .order_by(DailyCountryInfo.avg_aqi.desc())
        .first()
    )

    country_dict = {
        "id": country[0],
        "name": country[1],
        "capital": country[2],
        "region": country[3],
        "population": country[4],
        "lat": round(country[5], PRECISION),
        "lng": round(country[6], PRECISION),
        "economic_status": country[7],
        "flag_url": country[8],
        "latest_aqi": round(country[9], PRECISION),
        "cities": [{"id": city[0], "name": city[1]} for city in cities],
        "worst_day": worst_day[0].strftime("%Y-%m-%d"),
    }

    return jsonify(country_dict)


@app.route("/api/cities", methods=["GET"])
def get_cities():
    """Gets information about all cities."""
    limit_offset = parse_query.get_limit_offset(request.args)

    # check that limit_offset didn't return a response, else error
    if type(limit_offset) is Response:
        return limit_offset

    latest_day = session.query(func.max(DailyCityInfo.date)).first()[0]

    order_by = parse_query.get_orders(city_fields, request.args)

    if type(order_by) is Response:
        return order_by

    filters = parse_query.get_filters(city_fields, request.args)

    # check that filters didn't return a response, else error
    if type(filters) is Response:
        return filters

    query = (
        session.query(
            City.id,
            City.city_name,
            City.region,
            City.country,
            City.latitude,
            City.longitude,
            DailyCityInfo.aqi,
            City.maps_place_photo_reference,
        )
        .join(DailyCityInfo, City.id == DailyCityInfo.city_id)
        .filter(DailyCityInfo.date == latest_day)
        .filter(*filters)
    )

    cities = query.order_by(*order_by).limit(limit_offset[0]).offset(limit_offset[1])

    total_count = query.count()

    if cities.count() == 0 and limit_offset[1] != 0:
        return Response(
            json.dumps(
                {
                    "error": f"{request.args.get('offset')} is an invalid value for offset"
                }
            ),
            404,
        )

    cities_dict = [
        {
            "id": row[0],
            "name": row[1],
            "region": row[2],
            "country": row[3],
            "lat": round(row[4], PRECISION),
            "lng": round(row[5], PRECISION),
            "latest_aqi": row[6],
            "maps_place_photo_ref": row[7],
        }
        for row in cities
    ]

    return jsonify(
        {
            "size": len(cities_dict),
            "results": cities_dict,
            "total_count": total_count,
            "limit": limit_offset[0],
            "offset": limit_offset[1],
        }
    )


@app.route("/api/cities/<int:id>", methods=["GET"])
def get_city(id):
    """Get information about a particular city."""
    latest_day = (
        session.query(DailyCityInfo.city_id, func.max(DailyCityInfo.date))
        .group_by(DailyCityInfo.city_id)
        .filter(DailyCityInfo.city_id == id)
        .first()
    )

    if latest_day is None:
        return Response(json.dumps({"error": f"Could not find city with id {id}"}), 404)

    ranked_cities = (
        session.query(
            City.id,
            City.city_name,
            City.region,
            City.country,
            City.latitude,
            City.longitude,
            DailyCityInfo.aqi,
            DailyCityInfo.date,
            func.rank()
            .over(order_by=DailyCityInfo.aqi, partition_by=DailyCityInfo.date)
            .label("rank"),
            City.maps_place_photo_reference,
        )
        .join(DailyCityInfo, DailyCityInfo.city_id == City.id)
        .filter(DailyCityInfo.date == latest_day[1])
        .subquery()
    )

    city = session.query(ranked_cities).filter(ranked_cities.c.id == id).first()

    past_aq_data = (
        session.query(DailyCityInfo)
        .filter(DailyCityInfo.city_id == id)
        .order_by(DailyCityInfo.date.desc())
        .limit(7)
    )

    past_aq_dict = [
        {
            "date": day.date.strftime("%Y-%m-%d"),
            "aqi": round(day.aqi, PRECISION),
            "co": round(day.co, PRECISION),
            "no": round(day.no, PRECISION),
            "no2": round(day.no2, PRECISION),
            "o3": round(day.o3, PRECISION),
            "so2": round(day.so2, PRECISION),
            "pm2_5": round(day.pm2_5, PRECISION),
            "pm10": round(day.pm10, PRECISION),
            "nh3": round(day.nh3, PRECISION),
        }
        for day in past_aq_data
    ]

    city_dict = {
        "id": city[0],
        "name": city[1],
        "region": city[2],
        "country": city[3],
        "lat": round(city[4], PRECISION),
        "lng": round(city[5], PRECISION),
        "rank": city[8],
        "maps_place_photo_ref": city[9],
        "past_aq_data": past_aq_dict,
    }

    return jsonify(city_dict)


@app.route("/api/days", methods=["GET"])
def get_days():
    """Gets information about all days."""
    limit_offset = parse_query.get_limit_offset(request.args)

    # check that limit_offset didn't return a response, else error
    if type(limit_offset) is Response:
        return limit_offset

    order_by = parse_query.get_orders(day_fields, request.args)

    if type(order_by) is Response:
        return order_by

    filters = parse_query.get_filters(day_fields, request.args)

    # check that filters didn't return a response, else error
    if type(filters) is Response:
        return filters

    query = session.query(Day).filter(*filters)

    days = query.order_by(*order_by).limit(limit_offset[0]).offset(limit_offset[1])

    total_count = query.count()

    if days.count() == 0 and limit_offset[1] != 0:
        return Response(
            json.dumps(
                {
                    "error": f"{request.args.get('offset')} is an invalid value for offset"
                }
            ),
            404,
        )

    days_dicts = [
        {
            "date": day.id.strftime("%Y-%m-%d"),
            "aqi": round(day.avg_aqi, PRECISION),
            "co": round(day.avg_co, PRECISION),
            "no": round(day.avg_no, PRECISION),
            "no2": round(day.avg_no2, PRECISION),
            "o3": round(day.avg_o3, PRECISION),
            "so2": round(day.avg_so2, PRECISION),
            "pm2_5": round(day.avg_pm2_5, PRECISION),
            "pm10": round(day.avg_pm10, PRECISION),
            "nh3": round(day.avg_nh3, PRECISION),
        }
        for day in days
    ]

    return jsonify(
        {
            "size": len(days_dicts),
            "results": days_dicts,
            "total_count": total_count,
            "limit": limit_offset[0],
            "offset": limit_offset[1],
        }
    )


@app.route("/api/days/<int:year>-<int:month>-<int:day>", methods=["GET"])
def get_day(year, month, day):
    """Gets information about a particular day."""
    try:
        date = datetime.date(year, month, day)
    except ValueError as e:
        return Response(
            json.dumps(
                {
                    "error": f"Date {year}-{month}-{day} is not formatted correctly (try yyyy-mm-dd)"
                }
            ),
            400,
        )

    day_object = session.query(Day).filter(Day.id == date).first()
    if day_object is None:
        return Response(
            json.dumps({"error": f"Could not find day with id {year}-{month}-{day}"}),
            404,
        )

    worst_cities = (
        session.query(
            DailyCityInfo.date,
            DailyCityInfo.aqi,
            DailyCityInfo.city_id,
            City.city_name,
            City.latitude,
            City.longitude,
        )
        .join(City, City.id == DailyCityInfo.city_id)
        .filter(DailyCityInfo.date == date)
        .order_by(DailyCityInfo.aqi.desc().nullslast())
        .limit(10)
    )
    worst_countries = (
        session.query(
            DailyCountryInfo.date,
            DailyCountryInfo.avg_aqi,
            DailyCountryInfo.country_id,
            Country.country_name,
            Country.latitude,
            Country.longitude,
        )
        .join(Country, Country.alpha2code == DailyCountryInfo.country_id)
        .filter(DailyCountryInfo.date == date)
        .order_by(DailyCountryInfo.avg_aqi.desc().nullslast())
        .limit(10)
    )
    worst_cities_list = [
        {
            "id": city[2],
            "name": city[3],
            "lat": round(city[4], PRECISION),
            "lng": round(city[5], PRECISION),
        }
        for city in worst_cities
    ]
    worst_countries_list = [
        {
            "id": country[2],
            "name": country[3],
            "lat": round(country[4], PRECISION),
            "lng": round(country[5], PRECISION),
        }
        for country in worst_countries
    ]
    day_dict = {
        "date": day_object.id.strftime("%Y-%m-%d"),
        "aqi": round(day_object.avg_aqi, PRECISION),
        "co": round(day_object.avg_co, PRECISION),
        "no": round(day_object.avg_no, PRECISION),
        "no2": round(day_object.avg_no2, PRECISION),
        "o3": round(day_object.avg_o3, PRECISION),
        "so2": round(day_object.avg_so2, PRECISION),
        "pm2_5": round(day_object.avg_pm2_5, PRECISION),
        "pm10": round(day_object.avg_pm10, PRECISION),
        "nh3": round(day_object.avg_nh3, PRECISION),
        "worst_cities": worst_cities_list,
        "worst_countries": worst_countries_list,
    }
    return jsonify(day_dict)


# main method - runs the app
if __name__ == "__main__":  # pragma: no cover
    app.run(debug=True, port=8080)
