from init_db import session, Country, City, DailyCountryInfo, DailyCityInfo, Day
from apscheduler.schedulers.blocking import BlockingScheduler
from sqlalchemy import func
from scraper import (
    scrape_city_aq_data,
    calculate_country_aq_data,
    calculate_daily_aq_data,
)
from algoliasearch.search_client import SearchClient
import datetime
import os

"""
A file to create a cron job to update the aqi data.
"""


# initialize scheduler
scheduler = BlockingScheduler()

PRECISION = 3


@scheduler.scheduled_job("cron", id="update_data", hour=5)
def update_data():
    """
    Runs all the routines to pull new AQI data into the database and push these changes to the
    algolia indexes of each of the three models.
    """
    print("Database update started")
    last_entry = session.query(func.max(Day.id)).first()
    last_date = datetime.datetime.combine(last_entry[0], datetime.datetime.min.time())
    scrape_city_aq_data()
    calculate_country_aq_data()
    calculate_daily_aq_data()
    print("Database update complete")

    if "API_DEPLOYED" in os.environ and os.environ["API_DEPLOYED"] == "true":
        print("Starting algolia update")
        client = SearchClient.create(
            os.environ["ALGOLIA_APP_ID"], os.environ["ALGOLIA_API_KEY"]
        )

        index = client.init_index("countries_ALL")
        countries = get_all_country_instances_update()
        index.partial_update_objects(countries)

        index = client.init_index("cities_ALL")
        cities = get_all_cities_instances_update()
        index.partial_update_objects(cities)

        index = client.init_index("days_ALL")
        new_days = get_all_days_instance_update(last_date)
        index.save_objects(new_days)

        print("Finished algolia update")


def get_all_days_instance_update(last_date):
    """
    Create a json object for any new days since the last run. For use with algolia.
    """
    days = session.query(Day).filter(Day.id > last_date)
    algolia_days = []
    for day in days:
        date = day.id
        worst_cities = (
            session.query(
                DailyCityInfo.date,
                DailyCityInfo.aqi,
                DailyCityInfo.city_id,
                City.city_name,
                City.latitude,
                City.longitude,
            )
            .join(City, City.id == DailyCityInfo.city_id)
            .filter(DailyCityInfo.date == date)
            .order_by(DailyCityInfo.aqi.desc().nullslast())
            .limit(10)
        )
        worst_countries = (
            session.query(
                DailyCountryInfo.date,
                DailyCountryInfo.avg_aqi,
                DailyCountryInfo.country_id,
                Country.country_name,
                Country.latitude,
                Country.longitude,
            )
            .join(Country, Country.alpha2code == DailyCountryInfo.country_id)
            .filter(DailyCountryInfo.date == date)
            .order_by(DailyCountryInfo.avg_aqi.desc().nullslast())
            .limit(10)
        )
        worst_cities_list = [
            {
                "id": city[2],
                "name": city[3],
                "lat": round(city[4], PRECISION),
                "lng": round(city[5], PRECISION),
            }
            for city in worst_cities
        ]
        worst_countries_list = [
            {
                "id": country[2],
                "name": country[3],
                "lat": round(country[4], PRECISION),
                "lng": round(country[5], PRECISION),
            }
            for country in worst_countries
        ]
        day_dict = {
            "objectID": day.id.strftime("%Y-%m-%d"),
            "date": day.id.strftime("%Y-%m-%d"),
            "aqi": round(day.avg_aqi, PRECISION),
            "co": round(day.avg_co, PRECISION),
            "no": round(day.avg_no, PRECISION),
            "no2": round(day.avg_no2, PRECISION),
            "o3": round(day.avg_o3, PRECISION),
            "so2": round(day.avg_so2, PRECISION),
            "pm2_5": round(day.avg_pm2_5, PRECISION),
            "pm10": round(day.avg_pm10, PRECISION),
            "nh3": round(day.avg_nh3, PRECISION),
            "worst_cities": worst_cities_list,
            "worst_countries": worst_countries_list,
        }
        algolia_days.append(day_dict)
    return algolia_days


def get_all_cities_instances_update():
    """
    Pull the updated information for a city instance to use for updating objects in the algolia index.
    """
    algolia_cities = []
    latest_day = session.query(func.max(DailyCityInfo.date)).first()

    ranked_cities = (
        session.query(
            DailyCityInfo.city_id,
            DailyCityInfo.aqi,
            func.rank()
            .over(order_by=DailyCityInfo.aqi, partition_by=DailyCityInfo.date)
            .label("rank"),
        )
        .filter(DailyCityInfo.date == latest_day[0])
        .order_by(DailyCityInfo.city_id)
        .all()
    )

    seven_days_latest = latest_day[0] - datetime.timedelta(days=7)

    past_aq_info = (
        session.query(DailyCityInfo)
        .filter(DailyCityInfo.date > seven_days_latest)
        .order_by(DailyCityInfo.city_id, DailyCityInfo.date.desc())
        .all()
    )

    past_aq_index = 0
    for index in range(len(ranked_cities)):
        city_id = ranked_cities[index][0]
        assert past_aq_info[past_aq_index].city_id == city_id
        past_aq_data = []
        while (
            past_aq_index < len(past_aq_info)
            and past_aq_info[past_aq_index].city_id == city_id
        ):
            past_aq_data.append(
                {
                    "date": past_aq_info[past_aq_index].date.strftime("%Y-%m-%d"),
                    "aqi": round(past_aq_info[past_aq_index].aqi, PRECISION),
                    "co": round(past_aq_info[past_aq_index].co, PRECISION),
                    "no": round(past_aq_info[past_aq_index].no, PRECISION),
                    "no2": round(past_aq_info[past_aq_index].no2, PRECISION),
                    "o3": round(past_aq_info[past_aq_index].o3, PRECISION),
                    "so2": round(past_aq_info[past_aq_index].so2, PRECISION),
                    "pm2_5": round(past_aq_info[past_aq_index].pm2_5, PRECISION),
                    "pm10": round(past_aq_info[past_aq_index].pm10, PRECISION),
                    "nh3": round(past_aq_info[past_aq_index].nh3, PRECISION),
                }
            )
            past_aq_index += 1

        city_dict = {
            "objectID": ranked_cities[index][0],
            "rank": ranked_cities[index][2],
            "past_aq_data": past_aq_data,
        }

        algolia_cities.append(city_dict)

    return algolia_cities


def get_all_country_instances_update():
    """
    Get any updated information for a country as json for use to update the algolia index.
    """
    algolia_countries = []

    aqi_ordered_days = (
        session.query(
            DailyCountryInfo.date,
            DailyCountryInfo.country_id,
            DailyCountryInfo.avg_aqi,
            func.rank()
            .over(
                order_by=DailyCountryInfo.avg_aqi.desc(),
                partition_by=DailyCountryInfo.country_id,
            )
            .label("rank"),
        )
        .order_by(DailyCountryInfo.country_id, "rank", DailyCountryInfo.date.desc())
        .subquery()
    )

    worst_days = (
        session.query(aqi_ordered_days).distinct(aqi_ordered_days.c.country_id).all()
    )

    date_ordered_days = session.query(
        DailyCountryInfo.date,
        DailyCountryInfo.country_id,
        DailyCountryInfo.avg_aqi,
        func.rank()
        .over(
            order_by=DailyCountryInfo.date.desc(),
            partition_by=DailyCountryInfo.country_id,
        )
        .label("rank"),
    ).subquery()

    latest_aqis = (
        session.query(date_ordered_days)
        .filter(date_ordered_days.c.rank == 1)
        .order_by(date_ordered_days.c.country_id)
        .all()
    )

    for index in range(len(latest_aqis)):
        assert latest_aqis[index][1] == worst_days[index][1]
        country_dict = {
            "objectID": latest_aqis[index][1],
            "latest_aqi": round(latest_aqis[index][2], PRECISION),
            "worst_day": worst_days[index][0].strftime("%Y-%m-%d"),
        }
        algolia_countries.append(country_dict)
    return algolia_countries


def start():
    """
    Starts the scheduler.
    """
    scheduler.start()


def fix_countries():
    """
    For use in running the countries update manually in case of failure.
    """
    calculate_country_aq_data()
    client = SearchClient.create(
        os.environ["ALGOLIA_APP_ID"], os.environ["ALGOLIA_API_KEY"]
    )
    index = client.init_index("countries_ALL")
    countries = get_all_country_instances_update()
    index.partial_update_objects(countries)


if __name__ == "__main__":
    start()
