# pylint: disable = invalid-name
# pylint: disable = missing-docstring

import csv
import os
import time
from datetime import date, datetime, timezone, timedelta

from sqlalchemy import desc, func  # type: ignore

import aqi
from init_db import session, Country, City, DailyCityInfo, DailyCountryInfo, Day

from requests_futures.sessions import FuturesSession

"""
scraper.py - This file scrapes data from each of the APIs we use and into our database
"""

COUNTRIES_API_URL = "https://restcountries.eu/rest/v2/all"
CITIES_API_URL = "https://public.opendatasoft.com/api/records/1.0/search/"
AQ_API_URL = "http://api.openweathermap.org/data/2.5/air_pollution/history"
AQ_API_KEY = os.environ["AQ_API_KEY"]
GCP_API_KEY = os.environ["GCP_API_KEY"]
NUMBER_CITIES = 2000  # scrape NUMBER_CITIES of the most populous cities in the world
FIRST_DATE = date(2020, 11, 27)


def add_city(response, check_for_country=True):
    """adds a city to our database"""
    if session.query(City).filter_by(
        latitude=response["fields"]["latitude"],
        longitude=response["fields"]["longitude"],
    ).count() == 0 and (
        not check_for_country
        or session.query(Country)
        .filter_by(
            alpha2code=response["fields"]["country"].upper(),
        )
        .count()
        > 0
    ):
        print(
            "adding a city "
            + response["fields"]["accentcity"]
            + ", "
            + response["fields"]["country"].upper()
        )

        city = City(
            city_name=response["fields"]["accentcity"],
            region=response["fields"]["region"],
            country=response["fields"]["country"].upper(),
            latitude=response["fields"]["latitude"],
            longitude=response["fields"]["longitude"],
        )

        session.add(city)
        session.flush()
        return city.id
    return None


def scrape_cities():
    """scrapes cities from the cities API we use"""
    print("\n\n--- SCRAPING CITY DATA ---")

    params = {"dataset": "worldcitiespop", "rows": NUMBER_CITIES, "sort": "population"}

    f_session = FuturesSession()

    future = f_session.get(CITIES_API_URL, params)

    response = future.result().json()

    for r in response["records"]:
        add_city(r)
    session.commit()


def get_eco_status_data():
    """gives use the economic status of a given country from the country API"""
    economic_status = {}
    with open("country_api_data.csv") as csvfile:
        reader = csv.reader(csvfile, delimiter=",", quotechar='"')
        for row in reader:
            if row[2] == "High income":
                economic_status[row[0]] = 3
            elif row[2] == "Upper middle income":
                economic_status[row[0]] = 2
            elif row[2] == "Lower middle income":
                economic_status[row[0]] = 1
            elif row[2] == "Low income":
                economic_status[row[0]] = 0
            else:
                economic_status[row[0]] = -1
    return economic_status


def scrape_countries():
    """scrapes countries from the countries API we use"""
    print("\n\n--- SCRAPING COUNTRY DATA ---")
    exclude_countries = [
        "AX",
        "AS",
        "AQ",
        "BQ",
        "BV",
        "IO",
        "UM",
        "VI",
        "CC",
        "GG",
        "HM",
        "GU",
        "MP",
        "PR",
        "BL",
        "MF",
        "GS",
        "TK",
        "WF",
    ]

    economic_status = get_eco_status_data()

    f_session = FuturesSession()

    future = f_session.get(COUNTRIES_API_URL)
    response = future.result().json()
    for r in response:

        # if this country is already present in the database or it is part of the excluded countries, skip
        if (
            r["alpha2Code"] in exclude_countries
            or session.query(Country).filter_by(alpha2code=r["alpha2Code"]).count() != 0
        ):
            continue

        print("adding a country " + r["alpha2Code"])

        cities_params = {
            "dataset": "worldcitiespop",
            "q": r["capital"],
            "refine.country": r["alpha2Code"].lower(),
        }
        future_2 = f_session.get(CITIES_API_URL, cities_params)
        city_response = future_2.response().json()
        capital_id = None

        if city_response["nhits"] != 0:
            # add this city to the database if it's not present
            capital_id = add_city(city_response["records"][0], check_for_country=False)

            # certain countries are missing lat/lng, account for that
            latitude = None
            longitude = None
            if len(r["latlng"]) != 0:
                latitude = r["latlng"][0]
                longitude = r["latlng"][1]

            country = Country(
                alpha2code=r["alpha2Code"],
                country_name=r["name"],
                capital_id=capital_id,
                region=r["region"],
                population=r["population"],
                latitude=latitude,
                longitude=longitude,
                economic_status=economic_status.get(r["alpha3Code"], -1),
                flag_url=r["flag"],
            )

            session.add(country)

    session.commit()


def scrape_city_aq_data():
    """scrapes AQI data from the air quality API we use"""
    print("\n\n--- SCRAPING CITY AQI DATA ---")
    now = datetime.combine(date.today(), datetime.min.time()).replace(
        tzinfo=timezone.utc
    )
    cities = session.query(City).all()
    index = 1
    f_session = FuturesSession()
    for city in cities:
        print(f"{index}/{len(cities)}: Fetching AQ data for city {city.id}")
        last_entry = (
            session.query(DailyCityInfo)
            .filter_by(city_id=city.id)
            .order_by(desc("date"))
            .first()
        )
        last_date = (
            datetime.combine(FIRST_DATE, datetime.min.time())
            if last_entry is None
            else datetime.combine(
                last_entry.date + timedelta(days=1), datetime.min.time()
            )
        )

        last_date = last_date.replace(tzinfo=timezone.utc)
        if last_date <= now:
            params = {
                "lat": city.latitude,
                "lon": city.longitude,
                "start": int(last_date.timestamp()),
                "end": int(now.timestamp()),
                "appid": AQ_API_KEY,
            }
            start = time.time_ns()
            retries = 0
            success = False
            response = {"list": []}
            while retries < 5 and not success:
                try:
                    future = f_session.get(
                        AQ_API_URL
                        + f"?lat={city.latitude}&lon={city.longitude}&start={int(last_date.timestamp())}&end={int(now.timestamp())}&appid={AQ_API_KEY}"
                    )
                    response = future.result().json()
                    success = True
                except:
                    retries += 1
                    print("!!!!! retrying")

            for day in response["list"]:
                sec_per_day = 86400
                # we want midnight for each day
                if day["dt"] % sec_per_day == 0:
                    co = day["components"]["co"]
                    no = day["components"]["no"]
                    no2 = day["components"]["no2"]
                    o3 = day["components"]["o3"]
                    so2 = day["components"]["so2"]
                    pm2_5 = day["components"]["pm2_5"]
                    pm10 = day["components"]["pm10"]
                    nh3 = day["components"]["nh3"]

                    daily_city_info = DailyCityInfo(
                        city_id=city.id,
                        date=datetime.utcfromtimestamp(day["dt"]).date(),
                        aqi=aqi.calculate_average_aqi(co, no2, o3, so2, pm2_5, pm10),
                        co=co,
                        no=no,
                        no2=no2,
                        o3=o3,
                        so2=so2,
                        pm2_5=pm2_5,
                        pm10=pm10,
                        nh3=nh3,
                    )

                    session.add(daily_city_info)
            end = time.time_ns()
            if end - start < 16666667:
                print("sleeping")
                sleep_in_seconds = (16666667 - (end - start)) / 1000000000
                print(sleep_in_seconds)
                time.sleep(sleep_in_seconds)
        index += 1
    session.commit()


def calculate_country_aq_data():
    """calculates the air quality data for each country"""
    print("\n\n--- UPDATING COUNTRY AQ DATA ---")

    # clear the table
    session.query(DailyCountryInfo).delete()
    session.commit()

    # get the refreshed info
    rows = (
        session.query(
            City.country,
            DailyCityInfo.date,
            func.avg(DailyCityInfo.aqi),
            func.avg(DailyCityInfo.co),
            func.avg(DailyCityInfo.no),
            func.avg(DailyCityInfo.no2),
            func.avg(DailyCityInfo.o3),
            func.avg(DailyCityInfo.so2),
            func.avg(DailyCityInfo.pm2_5),
            func.avg(DailyCityInfo.pm10),
            func.avg(DailyCityInfo.nh3),
        )
        .join(DailyCityInfo, DailyCityInfo.city_id == City.id)
        .group_by(City.country, DailyCityInfo.date)
    )

    for row in rows:
        daily_country_info = DailyCountryInfo(
            country_id=row[0],
            date=row[1],
            avg_aqi=row[2],
            avg_co=row[3],
            avg_no=row[4],
            avg_no2=row[5],
            avg_o3=row[6],
            avg_so2=row[7],
            avg_pm2_5=row[8],
            avg_pm10=row[9],
            avg_nh3=row[10],
        )
        session.add(daily_country_info)
    session.commit()


def calculate_daily_aq_data():
    """calculates the daily air quality data"""
    print("\n\n--- UPDATING DAY AQ DATA ---")

    # clear the table
    session.query(Day).delete()
    session.commit()

    # get the refreshed info
    rows = session.query(
        DailyCityInfo.date,
        func.avg(DailyCityInfo.aqi),
        func.avg(DailyCityInfo.co),
        func.avg(DailyCityInfo.no),
        func.avg(DailyCityInfo.no2),
        func.avg(DailyCityInfo.o3),
        func.avg(DailyCityInfo.so2),
        func.avg(DailyCityInfo.pm2_5),
        func.avg(DailyCityInfo.pm10),
        func.avg(DailyCityInfo.nh3),
    ).group_by(DailyCityInfo.date)

    for row in rows:
        day = Day(
            id=row[0],
            avg_aqi=row[1],
            avg_co=row[2],
            avg_no=row[3],
            avg_no2=row[4],
            avg_o3=row[5],
            avg_so2=row[6],
            avg_pm2_5=row[7],
            avg_pm10=row[8],
            avg_nh3=row[9],
        )

        session.add(day)

    session.commit()


def scraper_main():
    """
    main method that scrapes for our database
    """
    print("Database update started")
    scrape_city_aq_data()
    calculate_country_aq_data()
    calculate_daily_aq_data()
    print("Database update complete")


if __name__ == "__main__":
    scraper_main()
