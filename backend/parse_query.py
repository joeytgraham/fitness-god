from operator import eq, le, ge, lt, gt, ne
from sqlalchemy.sql.expression import or_, and_, not_
import re
import json
from flask import Response

"""
parse_query.py - This file parses in the query parameters given in an endpoint call
"""

PAGESIZE = 20

relational_ops = {
    "eq": eq,
    "lte": le,
    "gte": ge,
    "lt": lt,
    "gt": gt,
    "neq": ne,
}

logical_ops = {"and": and_, "or": or_, "not": not_}

ops = []
ops.extend(logical_ops.keys())
ops.extend(relational_ops)

ops_string = "|".join(ops)

regex_string = f"^({ops_string})\((.*)\)$"
expr_regex = re.compile(regex_string)


def get_orders(fields, args):
    """sorts the list by ascending or descending"""
    sorting_directions = {"asc", "desc"}
    orders = []
    # validate query parameters
    sort_by = args.getlist("sort_by")
    sort_by.extend(args.getlist("sort_by[]"))
    for sort in sort_by:
        sort_split = re.split(r"[()]", sort)
        if (
            len(sort_split) != 3
            or sort_split[0] not in sorting_directions
            or sort_split[1] not in fields
            or sort_split[2] != ""
        ):
            # return error response
            return Response(
                json.dumps({"error": f"{sort} is an invalid value for sort_by"}),
                400,
            )

        order = fields.get(sort_split[1])[0]

        if sort_split[0] == "desc":
            order = order.desc()

        orders.append(order)

    return orders


def top_level_split_by_comma(s):
    """
    Split `s` by top-level commas only. Commas within parentheses are ignored.
    from https://stackoverflow.com/questions/33527245/python-split-by-comma-skipping-the-content-inside-parentheses
    """

    # Parse the string tracking whether the current character is within
    # parentheses.
    balance = 0
    parts = []
    part = ""

    for c in s:
        part += c
        if c == "(":
            balance += 1
        elif c == ")":
            balance -= 1
        elif c == "," and balance == 0:
            parts.append(part[:-1].strip())
            part = ""

    # Capture last part
    if len(part):
        parts.append(part.strip())

    return parts


def error_check_expression(op, body, filter_string):
    """
    checks that the filter given is a correct expression
    """
    # check if the op is valid
    if op not in logical_ops and op not in relational_ops:
        return Response(
            json.dumps({"error": f"{filter_string} unrecognized expression"}), 400
        )
    # not must have 1 arg
    elif op == "not" and len(body) != 1:
        return Response(
            json.dumps({"error": f"{filter_string} too many expressions A"}), 400
        )
    # if not not, must have at least two args
    elif op != "not" and len(body) < 2:
        return Response(
            json.dumps({"error": f"{filter_string} too few expressions"}), 400
        )
    # any relational operator must have only two args
    elif len(body) > 2 and (op != "and" and op != "or"):
        return Response(
            json.dumps({"error": f"{filter_string} too many expressions B"}), 400
        )


def create_filter(filter_string, fields):
    """takes the filter parameters and recursively parses them into expressions"""
    matches = expr_regex.match(filter_string)
    if matches is None:
        return Response(
            json.dumps({"error": f"{filter_string} is invalid filter"}), 400
        )

    body = top_level_split_by_comma(matches.group(2))

    error = error_check_expression(matches.group(1), body, filter_string)
    if error is not None:
        return error

    if matches.group(1) in logical_ops:
        body_expressions = []
        for expression in body:
            result = create_filter(expression, fields)

            # error
            if type(result) is Response:
                return result
            body_expressions.append(result)
        return logical_ops.get(matches.group(1))(*body_expressions)
    else:
        # this is a relational operator, format is column, value
        if body[0] not in fields:
            return Response(json.dumps({"error": f"{body[0]} is invalid field"}), 400)
        # try to cast value into correct type
        try:
            cast_value = fields.get(body[0])[1](body[1])
        except ValueError:
            return Response(
                json.dumps({"error": f"{body[1]} is invalid value for {body[0]}"}), 400
            )

        return relational_ops.get(matches.group(1))(fields.get(body[0])[0], cast_value)


def get_filters(fields, args):
    """
    check if filters were specified in call
    """
    if "filter" in args:
        filters = create_filter(args.get("filter"), fields)
        if type(filters) is Response:
            return filters
        else:
            return [filters]
    else:
        return []


def get_limit_offset(args):
    """
    sets the limit and offset if specified in the call
    """
    limit = PAGESIZE
    offset = 0
    if "limit" in args:
        try:
            limit = int(args.get("limit"))
        except ValueError:
            value = args.get("limit")
            return Response(
                json.dumps({"error": f"{value} is an invalid value for limit"}),
                400,
            )
        if limit < 0:
            return Response(
                json.dumps({"error": f"{limit} is an invalid value for limit"}),
                400,
            )
    if "offset" in args:
        try:
            offset = int(args.get("offset"))
            if limit == 0 and offset != 0:
                return Response(
                    json.dumps({"error": f"{offset} is an invalid value for offset"}),
                    404,
                )
        except ValueError:
            value = args.get("offset")
            return Response(
                json.dumps({"error": f"{value} is an invalid value for offset"}),
                400,
            )
        if offset < 0:
            offset = args.get("offset")
            return Response(
                json.dumps({"error": f"{offset} is an invalid value for offset"}),
                400,
            )
    return limit, offset
