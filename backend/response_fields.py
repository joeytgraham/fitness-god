"""
This file is a collective place to relate response fields to ORM model attributes and their types.
"""
# imports
import datetime
from init_db import (
    session,
    Country,
    City,
    Day,
    DailyCityInfo,
    DailyCountryInfo,
    EconomicStatus,
)


def parse_date_from_string(date_string):
    """
    parses in datetime object from a date string
    """
    return datetime.datetime.strptime(date_string, "%Y-%m-%d").date()


def eco_status_to_int(eco_status_text):
    """
    Converts a string to the int representation of the economic status
    """
    eco_status_int = (
        session.query(EconomicStatus.id)
        .filter(EconomicStatus.text == eco_status_text)
        .first()
    )
    if eco_status_int is None:
        raise ValueError
    return eco_status_int[0]


# field name: [Model attribute, type]
country_fields = {
    "capital": [City.city_name, str],
    "economic_status": [Country.economic_status, eco_status_to_int],
    "flag_url": [Country.flag_url, str],
    "id": [Country.alpha2code, str],
    "lat": [Country.latitude, float],
    "latest_aqi": [DailyCountryInfo.avg_aqi, float],
    "lng": [Country.longitude, float],
    "name": [Country.country_name, str],
    "population": [Country.population, int],
    "region": [Country.region, str],
}

# field name: [Model attribute, type]
city_fields = {
    "country": [City.country, str],
    "id": [City.id, int],
    "lat": [City.latitude, float],
    "latest_aqi": [DailyCityInfo.aqi, float],
    "lng": [City.longitude, float],
    "maps_place_photo_ref": [City.maps_place_photo_reference, str],
    "name": [City.city_name, str],
    "region": [City.region, str],
}

# field name: [Model attribute, type]
day_fields = {
    "date": [Day.id, parse_date_from_string],
    "aqi": [Day.avg_aqi, float],
    "co": [Day.avg_co, float],
    "no": [Day.avg_no, float],
    "no2": [Day.avg_no2, float],
    "o3": [Day.avg_o3, float],
    "so2": [Day.avg_so2, float],
    "pm2_5": [Day.avg_pm2_5, float],
    "pm10": [Day.avg_pm10, float],
    "nh3": [Day.avg_nh3, float],
}
