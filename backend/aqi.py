# pylint: disable = invalid-name
# pylint: disable = missing-docstring

import math

"""
A collection of methods to facilitate aqi calculation from individual pollutant levels.
"""


def truncate(number, decimals=0):
    """
    Returns a value truncated to a specific number of decimal places.
    """
    if not isinstance(decimals, int):
        raise TypeError("decimal places must be an integer.")
    elif decimals < 0:
        raise ValueError("decimal places has to be 0 or more.")
    elif decimals == 0:
        return math.trunc(number)

    factor = 10.0 ** decimals
    return math.trunc(number * factor) / factor


def convert_o3_from_ugm3_to_ppm(ug_m3):
    """
    converts o3 from ug/m^3 to ppm
    """
    return ug_m3 / 2.00 * 0.001


def convert_o3_from_ppm_to_ugm3(ppm):
    """
    converts o3 from ppm to ug/m3
    """
    return ppm * 2.00 / 0.001


def convert_co_from_ugm3_to_ppm(ug_m3):
    """
    converts co from ug/m3 to ppm
    """
    return ug_m3 / 1.145 * 0.001


def convert_co_from_ppm_to_ugm3(ppm):
    """
    converts co from ppm to ug/m3
    """
    return ppm * 1.145 / 0.001


def convert_so2_from_ugm3_to_ppb(ug_m3):
    """
    converts so2 from ug/m3 to ppb
    """
    return ug_m3 / 2.62


def convert_so2_from_ppb_to_ugm3(ppb):
    """
    converts so2 from ppb to ug/m3
    """
    return ppb * 2.62


def convert_no2_from_ugm3_to_ppb(ug_m3):
    """
    converts no2 from ug/m3 to ppb
    """
    return ug_m3 / 1.88


def convert_no2_from_ppb_to_ugm3(ppb):
    """
    converts no2 from ppb to ug/m3
    """
    return ppb * 1.88


def calculate_aqi(c_p, bp, bp_precision):
    """
    calculate the aqi of a given concentration based on breakpoints of a certain precision
    """
    i = [-1, 50, 100, 150, 200, 300, 400, 500]
    upper = 1
    while upper < len(bp) and c_p > bp[upper]:
        upper += 1

    if upper == len(bp):
        return -1

    bp_hi = bp[upper]
    bp_lo = bp[upper - 1] + bp_precision
    i_hi = i[upper]
    i_lo = i[upper - 1] + 1

    return ((i_hi - i_lo) * (c_p - bp_lo) / (bp_hi - bp_lo)) + i_lo


def calculate_aqi_no2(no2):
    """
    calcualte the aqi of no2 given in ppb
    """
    c_p = truncate(no2, 0)
    bp = [-1, 53, 100, 360, 649, 1249, 1649, 2049]

    return calculate_aqi(c_p, bp, 1)


def calculate_aqi_so2(so2):
    """
    calculate the aqi of so2 given in ppb
    """
    c_p = truncate(so2, 0)
    bp = [-1, 35, 75, 185, 304, 604, 804, 1004]

    return calculate_aqi(c_p, bp, 1)


def calculate_aqi_co(co):
    """
    calculate the aqi of co given in ppm
    """
    c_p = truncate(co, 1)
    bp = [-0.1, 4.4, 9.4, 12.4, 15.4, 30.4, 40.4, 50.4]

    return calculate_aqi(c_p, bp, 0.1)


def calculate_aqi_pm10(pm10):
    """
    calculate the aqi of pm10 given in ug/m3
    """
    c_p = truncate(pm10, 0)
    bp = [-1, 54, 154, 254, 354, 424, 504, 604]

    return calculate_aqi(c_p, bp, 1)


def calculate_aqi_pm2_5(pm2_5):
    """
    calculate the aqi of pm2.5 given in ug/m3
    """
    c_p = truncate(pm2_5, 1)
    bp = [-0.1, 12.0, 35.4, 55.4, 150.4, 250.4, 350.4, 500.4]

    return calculate_aqi(c_p, bp, 0.1)


def calculate_aqi_o3(o3):
    """
    calculate the aqi of o3 given in ppm
    """
    c_p = truncate(o3, 3)
    bp = [-0.001, 0.054, 0.070, 0.085, 0.105, 0.200]

    return calculate_aqi(c_p, bp, 0.001)


def calculate_average_aqi(co, no2, o3, so2, pm2_5, pm10):
    """
    calculate aqi based on concentrations of co, no2, o3, so2, pm2.5, pm10 given in ug/m3
    """
    sub_aqis = [0] * 6
    sub_aqis[0] = calculate_aqi_co(convert_co_from_ugm3_to_ppm(co))
    sub_aqis[1] = calculate_aqi_no2(convert_no2_from_ugm3_to_ppb(no2))
    sub_aqis[2] = calculate_aqi_o3(convert_o3_from_ugm3_to_ppm(o3))
    sub_aqis[3] = calculate_aqi_so2(convert_so2_from_ugm3_to_ppb(so2))
    sub_aqis[4] = calculate_aqi_pm2_5(pm2_5)
    sub_aqis[5] = calculate_aqi_pm10(pm10)
    return max(sub_aqis)
