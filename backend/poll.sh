#!/bin/sh

# Run the first command (replace the line below)
gunicorn wsgi:app -w 2 -b 0.0.0.0:8080 -t 30 &
FIRST_PID=$!

# Run the second command (replace the line below)
python scheduler.py &
SECOND_PID=$!

# Poll both processes until either one exits
while ((kill -0 $FIRST_PID 2&> /dev/null) && (kill -0 $SECOND_PID 2&> /dev/null)); do
    sleep 1
done