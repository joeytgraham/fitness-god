.DEFAULT_GOAL := all
MAKEFLAGS += --no-builtin-rules
SHELL         := bash

ifeq ($(shell uname -s), Darwin)
    BLACK         := black
    COVERAGE      := coverage3
    PYLINT        := pylint
    PYTHON        := python3
else ifeq ($(shell uname -p), unknown)
    BLACK         := black
    COVERAGE      := coverage
    PYLINT        := pylint
    PYTHON        := python
else
    BLACK         := black
    COVERAGE      := coverage3
    PYLINT        := pylint3
    PYTHON        := python3
endif

# build the docker images required to deploy locally
build-docker-images:
	cd ./frontend && docker build -t breathe-easy-frontend .
	cd ./backend && docker build -t breathe-easy-backend -f local.dockerfile .

# --------- RUNNING A SINGLE PART ----------

# run only the frontend, available at http://localhost:3000/, remove using docker stop CONTAINER and docker rm CONTAINER
run-frontend:
	docker run -p 3000:3000 -v $(PWD)/frontend:/frontend breathe-easy-frontend

# run only the backend, available at http://localhost:8080/, remove using docker stop CONTAINER and docker rm CONTAINER
run-backend:
	docker run -p 8080:8080 -v $(PWD)/backend:/backend --env-file backend/.env.local breathe-easy-backend


# ----------- RUNNING ENTIRE APPLICATION ------------

# create and start both docker containers for the frontend and the backend
up:
	docker-compose up --abort-on-container-exit

# starts existing frontend and backend containers
start:
	docker-compose start

# stops frontend and backend containers without removing them
stop:
	docker-compose stop

# stops and removes frontend and backend containers
down:
	docker-compose down

format:
	$(BLACK) backend/*.py
	$(BLACK) frontend/*.py

.pylintrc:
	$(PYLINT) --disable=locally-disabled,E1101 --reports=no --generate-rcfile > $@

# execute test harness
test: backend/main.py backend/tests.py .pylintrc
	-$(PYLINT) backend/models.py
	-$(PYLINT) backend/aqi.py
	-$(PYLINT) backend/init.py
	-$(PYLINT) backend/scraper.py
	-$(PYLINT) backend/wsgi.py
	-$(PYLINT) backend/main.py
	-$(PYLINT) backend/tests.py
	source backend/.env.test && $(COVERAGE) run    --branch backend/tests.py
	$(COVERAGE) report --omit=*/venv/* -m

# execute coverage with test.py
coverage: 
	source backend/.env.test && $(COVERAGE) run    --branch backend/tests.py
	$(COVERAGE) report --omit=*/venv/* -m

# remove temporary files
clean:
	rm -f  .coverage
	rm -f  .pylintrc
	rm -f  *.pyc
	rm -f  *.tmp
	rm -rf __pycache__
	rm -rf .mypy_cache

# ----------- MISC -------------
# a command to list all the makefile commands
help:
	@echo Options:
	@echo "  build-docker-images"
	@echo "  run-frontend"
	@echo "  run-backend"
	@echo "  up"
	@echo "  down"
	@echo "  start"
	@echo "  stop"
	@echo "  down"
	@echo "  test"
	@echo "  coverage"
	@echo "  help"