import os
import requests
import json

DATA_PATH = os.path.dirname(os.path.abspath(__file__))
OUR_BASE_URL = "https://breathe-easy.me/api/"
PROVIDER_BASE_URL = "https://www.culturedfoodies.me/api/"

def our_countries_data():
    params = {
        "limit": 50,
        "offset": 0,
        'sort_by': 'desc(latest_aqi)'
    }

    countries = requests.get(OUR_BASE_URL + "countries", params).json()

    top_50 = []
    for country in countries['results']:
        top_50.append({
            'label': country['name'],
            'value': country['latest_aqi']
        })

    with open(DATA_PATH + '/visualization_data/our_top_50_worst_countries.json', 'w') as file:
        file.write(json.dumps(top_50, indent=4))

def our_cities_data():
    aqi_cutoffs = [51, 101, 151, 201, 301]

    params = {
        "limit": 0,
        "offset": 0
    }

    meta_data = requests.get(OUR_BASE_URL + "cities", params).json()

    params['limit'] = meta_data['total_count']

    cities = requests.get(OUR_BASE_URL + "cities", params).json()

    num_per_level = [
        {
            'label': '[0-50]',
            'value': 0
        },
        {
            'label': '(50-100]',
            'value': 0
        },
        {
            'label': '(100-150]',
            'value': 0
        },
        {
            'label': '(150-200]',
            'value': 0
        },
        {
            'label': '(200-300]',
            'value': 0
        },
        {
            'label': '(300-500]',
            'value': 0
        }
    ]
    for city in cities['results']:
        i = 0
        found = False
        while i < len(aqi_cutoffs) and not found:
            if city['latest_aqi'] < aqi_cutoffs[i]:
                found = True
            else:
                i += 1
        num_per_level[i]['value'] += 1

    with open(DATA_PATH + '/visualization_data/our_cities_by_aqi_bracket.json', 'w') as file:
        file.write(json.dumps(num_per_level, indent=4))

def our_days_data():
    params = {
        "limit": 0,
        "offset": 0,
        "sort_by": 'asc(date)'
    }

    meta_data = requests.get(OUR_BASE_URL + "days", params).json()

    params['limit'] = meta_data['total_count']

    days = requests.get(OUR_BASE_URL + "days", params).json()

    formatted_days = []

    for day in days['results']:
        formatted_days.append({
            'label': day['date'],
            'value': day['aqi']
        })

    with open(DATA_PATH + '/visualization_data/our_days_avg_aqi.json', 'w') as file:
        file.write(json.dumps(formatted_days, indent=4))

def provider_cuisines_data():
    country_response = requests.get(PROVIDER_BASE_URL + "countries").json()

    country_to_subregion = {}
    for country in country_response['countries']:
        country_to_subregion[country['id']] = country['subregion']

    # print(country_to_subregion)

    cuisines_response = requests.get(PROVIDER_BASE_URL + "cuisines").json()

    num_cuisines_per_subregion = {}

    for cuisine in cuisines_response['cuisines']:
        countries_string = cuisine['countryID']
        countries_array = countries_string.split(', ')
        for country in countries_array:
            region = country_to_subregion[int(country)]
            if region not in num_cuisines_per_subregion:
                num_cuisines_per_subregion[region] = 0
            num_cuisines_per_subregion[region] += 1

    formatted_results = []

    for subregion in num_cuisines_per_subregion:
        formatted_results.append({
            'label': subregion,
            'value': num_cuisines_per_subregion[subregion]
        })

    with open(DATA_PATH + '/visualization_data/provider_num_cuisines_per_subregion.json', 'w') as file:
        file.write(json.dumps(formatted_results, indent=4))

def provider_cities_data():
    cities_response = requests.get(PROVIDER_BASE_URL + "cities").json()

    state_stats = {}

    for city in cities_response['cities']:
        leisure_score = city['leisure_culture']
        state = city['state']
        if state not in state_stats:
            state_stats[state] = [0, 0]
        state_stats[state][0] += 1
        state_stats[state][1] += leisure_score

    formatted_results = []

    for state in state_stats:
        formatted_results.append({
            'label': state,
            'value': state_stats[state][1] / state_stats[state][0]
        })

    sorted_results = sorted(formatted_results, key=lambda i: i['label'])

    with open(DATA_PATH + '/visualization_data/provider_avg_cities_leisure_by_state.json', 'w') as file:
        file.write(json.dumps(sorted_results, indent=4))

def provider_restaurant_data():
    restaurant_response = requests.get(PROVIDER_BASE_URL + "restaurants").json()

    rating_cutoffs = [1, 2, 3, 4, 5]

    restaurant_ratings = [
        {
            'label': '[0-1]',
            'value': 0
        },
        {
            'label': '(1-2]',
            'value': 0
        },
        {
            'label': '(2-3]',
            'value': 0
        },
        {
            'label': '(3-4]',
            'value': 0
        },
        {
            'label': '(4-5]',
            'value': 0
        }
    ]

    for restaurant in restaurant_response['restaurants']:
        rating = float(restaurant['aggregate_rating'])
        i = 0
        found = False
        while i < len(rating_cutoffs) and not found:
            if rating < rating_cutoffs[i]:
                found = True
            else:
                i += 1
        restaurant_ratings[i]['value'] += 1

    with open(DATA_PATH + '/visualization_data/provider_num_restaurants_per_rating.json', 'w') as file:
        file.write(json.dumps(restaurant_ratings, indent=4))

if __name__ == "__main__":
    # our_countries_data()
    # our_cities_data()
    # our_days_data()
    # provider_cuisines_data()
    provider_cities_data()
    # provider_restaurant_data()


