/* importing tools pictures */
import dockerPic from './images/docker.png';
import postmanPic from './images/postman.png';
import reactPic from './images/react.png';
import bootstrapPic from './images/bootstrap.png';
import vscodePic from './images/vscode.png';
import gitlabPic from './images/gitlab.png';
import teamsPic from './images/teams.png';

/* information for tools for about page */
export const tools = [
    {
        name: "Docker",
        use: "To create consistent environments among team members.",
        link: "https://www.docker.com/",
        pic: dockerPic,
    },
    {
        name: "Postman",
        use: "Testing both our own endpoints and the endpoints of the APIs we are using.",
        link: "https://www.postman.com/",
        pic: postmanPic,
    },
    {
        name: "React",
        use: "To code the front-end.",
        link: "https://reactjs.org/",
        pic: reactPic,
    },
    {
        name: "Bootstrap",
        use: "CSS framework for our website.",
        link: "https://getbootstrap.com/",
        pic: bootstrapPic,
    },
    {
        name: "VSCode",
        use: "The main IDE for coding our project.",
        link: "https://code.visualstudio.com/",
        pic: vscodePic,
    },
    {
        name: "Gitlab",
        use: "Tracking our issues, user stories, and CI / CD pipelines.",
        link: "https://about.gitlab.com/",
        pic: gitlabPic,
    },
    {
        name: "Teams",
        use: "For communication and collaboration.",
        link: "https://www.microsoft.com/en-us/microsoft-teams/group-chat-software",
        pic: teamsPic,
    },
]