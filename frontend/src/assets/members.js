/* importing team member pictures */
import clairePic from '../assets/images/Claire.JPG';
import josephPic from '../assets/images/Joseph.png';
import emaanPic from '../assets/images/Emaan.png';
import shanPic from '../assets/images/Shan.jpeg';
import kianaPic from '../assets/images/Kiana.jpg';

/* team member information for about page */
export const members = [
    {
        id: 0,
        name:   "Claire Mathieu",
        job:    "FullStack Engineer",
        bio:    "Claire is a junior at UT Austin. She is originally from Austin, TX. " + 
                "She works on full stack development for the site and enjoys web and application development. " + 
                "Her hobbies include making crafts, baking, strategy board games, video games and soccer.",
        linkedin: 'https://www.linkedin.com/in/c-mathieu/',
        pic: clairePic,         
        
    },
    {
        id: 1,
        name:   "Joseph Graham",
        job:    "Backend Engineer",
        bio:    "Joseph is originally from Dripping Springs, Texas and is a senior at UT Austin. " +
                "He works on backend development for the site and is interested in cybersecurity " +
                "as well as video game design. His hobbies include skiing, golf, gaming, music and " +
                "board games.",
        linkedin: 'https://www.linkedin.com/in/joseph-graham77/',
        pic: josephPic,         
        
    },
    {
        id: 2,
        name:   "Emaan Haseem",
        job:    "Frontend Engineer",
        bio:    "Emaan is a junior studying Computer Science at UT Austin. " +
                "Emaan is originally from Plano, Texas. She works on the frontend development for the website, " +
                "and is interested in fullstack development, and Cloud Engineering. Emaan enjoys " +
                "cooking, watching Youtube, and drawing in her free time.",
        linkedin: 'https://www.linkedin.com/in/emaanhaseem/',
        pic: emaanPic,         
        
    },
    {
        id: 3,
        name:   "Shan Rizvi",
        job:    "Frontend Engineer",
        bio:    "Shan is a junior CS student at UT Austin from Houston, Texas. " +
                "He works on frontend development for the website and is interested " +
                "in pursuing product management. He enjoys hiking, kayaking, playing " +
                "video games, and watching TV shows in his free time.",
        linkedin: 'https://www.linkedin.com/in/shan-rizvi/',
        pic: shanPic,         
        
    },
    {
        id: 4,
        name:   "Kiana Fithian",
        job:    "Frontend Engineer",
        bio:    "Kiana is a junior at UT Austin. She is from Dallas, TX. " +
                "She is working on the frontend development for this project and " +
                "is interested in web development. Her hobbies include playing the " +
                "piano, going on hikes, and discovering new music.",
        linkedin: 'https://www.linkedin.com/in/c-mathieu/',
        pic: kianaPic,         
        
    }
]