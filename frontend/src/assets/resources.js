/* importing resources pictures */
import earthPic from '../assets/images/earth-api.png';
import weatherPic from '../assets/images/weather-api.png';
import odsPic from '../assets/images/ods-api.png';
import worldbankPic from '../assets/images/worldbank-api.png';
import airPic from '../assets/images/air.png';

/* information for api and data sources for about page */
export const resources = [
    {
        name: "Rest Countries API",
        about: "Information about countries across the world",
        link: 'https://restcountries.eu/#api-endpoints-code',
        pic: earthPic,

    },
    {
        name: "OpenDataSoft’s World Cities Population API",
        about: "Information about cities around the world",
        link: 'https://public.opendatasoft.com/explore/dataset/worldcitiespop/api/?disjunctive.country&sort=population',
        pic: odsPic,

    },
    {
        name: "OpenWeather’s Air Pollution API",
        about: "Pollution information based on longitude and latitude",
        link: 'https://openweathermap.org/api/air-pollution',
        pic: weatherPic,

    },
    {
        name: "The World Bank",
        about: "GDP per capita and economic classifications",
        link: 'https://data.worldbank.org/indicator/NY.GDP.PCAP.KD',
        pic: worldbankPic,

    },
    {
        name: "EPA Technical Assistance Document for the Reporting of Daily Air Quality – the Air Quality Index",
        about: "Used to calculate and qualify aqi data",
        link: 'https://www.airnow.gov/sites/default/files/2020-05/aqi-technical-assistance-document-sept2018.pdf',
        pic: airPic,

    },
]