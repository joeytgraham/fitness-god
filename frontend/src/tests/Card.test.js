import cityCard from '../components/cityCard'
import countriesCard from '../components/countriesCard'
import { shallow, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16';
import city_data from '../assets/data/cities.json'
import country_data from '../assets/data/countries.json'
import toJson from 'enzyme-to-json';


test('Testing cards on city model page', () => {
    configure({ adapter: new Adapter() });
    const wrapper = shallow(<cityCard instance={city_data} />);
    expect(toJson(wrapper)).toMatchSnapshot();
    
});

test('Testing cards on country model page', () => {
    configure({ adapter: new Adapter() });
    const wrapper = shallow(<countriesCard instance={country_data} />);
    expect(toJson(wrapper)).toMatchSnapshot();
    
});
