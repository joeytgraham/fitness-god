import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import toJson from 'enzyme-to-json';
import React from 'react';
import SearchBar from '../components/searchBar';


test('Testing Search Bar renders correctly', () => {
    configure({ adapter: new Adapter() });
    const wrapper = shallow(<SearchBar
    />);
    expect(toJson(wrapper)).toMatchSnapshot();
});
