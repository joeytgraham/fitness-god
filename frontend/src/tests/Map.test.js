import { render } from '@testing-library/react';
import { shallow, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16';
import PinMap from '../components/pinmap'
import GoogleMapReact from 'google-map-react';


beforeEach(() => {
    jest.spyOn(console, 'error').mockImplementation(() => { });
});

test('Testing PinMap with valid location', () => {
    configure({ adapter: new Adapter() });
    const location = [{ "name": "Washington", "lat": 38.895, "lng": -77.0366667 }];
    const wrapper = shallow(<PinMap
        locations={location}
        map_style={{ height: '40vh', width: '68vh', marginLeft: '8vw', marginRight: '8vw' }}
        baseURL='/cities/' />);
    expect(wrapper.contains(<GoogleMapReact />));


});

test('Testing PinMap with invalid location', () => {
    configure({ adapter: new Adapter() });
    const location = [{ "name": "Washington", "lat": 100, "lng": 200 }];
    const wrapper = shallow(<PinMap
        locations={location}
        map_style={{ height: '40vh', width: '68vh', marginLeft: '8vw', marginRight: '8vw' }}
        baseURL='/cities/' />);
    expect(wrapper.contains(<GoogleMapReact />));


});





