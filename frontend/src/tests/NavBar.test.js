import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import toJson from 'enzyme-to-json';
import React from 'react';
import { Nav } from 'react-bootstrap';
import NavBar from '../components/navbar';

test('Testing NavBar contains the proper links', () => {
  configure({ adapter: new Adapter() });
  const wrapper = shallow(<NavBar />);
  expect(wrapper.containsAllMatchingElements([
    <Nav.Link href='/countries'>Countries</Nav.Link>,
    <Nav.Link href='/cities'>Cities</Nav.Link>,
    <Nav.Link href='/days'>Days</Nav.Link>,
    <Nav.Link href='/about'>About</Nav.Link>
  ]));
});

test('Testing Navbar renders correctly', () => {
  configure({ adapter: new Adapter() });
  const wrapper = shallow(<NavBar />);
  expect(toJson(wrapper)).toMatchSnapshot();
});