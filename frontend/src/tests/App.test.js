import { render, screen } from '@testing-library/react';
import LandingPage from '../pages/index'

test('landing page text', () => {
  render(<LandingPage />);
  const pageText = screen.getByText(/air pollution/i);
  expect(pageText).toBeInTheDocument();
});

