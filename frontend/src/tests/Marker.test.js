import Marker from '../components/marker'
import { shallow, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16';
import toJson from 'enzyme-to-json';


test('Testing cards on city model page', () => {
    configure({ adapter: new Adapter() });
    const wrapper = shallow(<Marker />);
    expect(toJson(wrapper)).toMatchSnapshot();
    
});
