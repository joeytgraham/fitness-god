import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import toJson from 'enzyme-to-json';
import React from 'react';
import SearchDayCard from '../components/searchDaysCard';
import { connectHits } from "react-instantsearch-dom";


test('Testing Search Day Card renders correctly', () => {
    configure({ adapter: new Adapter() });
    const daysHits = ({ hits }) => (
        <div>
          <div className="row">
            {currentDays.length === 0 ? setCurrentDays(hits.slice(0, 6)) : null}
            {currentDays.map((hit) => (
              <div key={hit.year_id} style={{ padding: '10px' }}>
                <SearchDayCard hit={hit} />
              </div>
            ))}
          </div>
        </div>
      );
    
    const DayHits = connectHits(daysHits);
    const wrapper = shallow(<DayHits/>);
    expect(toJson(wrapper)).toMatchSnapshot();
});
