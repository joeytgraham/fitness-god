import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import toJson from 'enzyme-to-json';
import React from 'react';
import Pagination from '../components/pagination';


test('Testing Pagination renders correctly', () => {
    configure({ adapter: new Adapter() });
    const totalPosts = 0;
    const postsPerPage = 10;
    const paginate = pageNumber => { }
    const wrapper = shallow(<Pagination
        postsPerPage={postsPerPage}
        totalPosts={totalPosts}
        paginate={paginate}
    />);
    expect(toJson(wrapper)).toMatchSnapshot();
});
