import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import toJson from 'enzyme-to-json';
import React from 'react';
import SearchCityCard from '../components/searchCityCard';
import { connectHits } from "react-instantsearch-dom";


test('Testing Search City Card renders correctly', () => {
    configure({ adapter: new Adapter() });
    const cityHits = ({ hits }) => (
        <div>
          <div className="row">
            {currentCities.length === 0 ? setCurrentCities(hits.slice(0, 6)) : null}
            {currentCities.map((hit) => (
              <div key={hit.country_id} style={{ padding: '10px' }}>
                <SearchCityCard hit={hit} />
              </div>
            ))}
          </div>
        </div>
      );
    const CityHits = connectHits(cityHits);
    const wrapper = shallow(<CityHits/>);
    expect(toJson(wrapper)).toMatchSnapshot();
});
