import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import toJson from 'enzyme-to-json';
import React from 'react'
import MemberCard from '../pages/About/memberCard';
import ResourceCard from '../pages/About/resourceCard';
import ToolCard from '../pages/About/toolCard';
import sky from '../assets/images/sky.jpg';


test('Member Card Renders Correctly', () => {
  configure({ adapter: new Adapter() });
  const name = "John Doe";
  const job = "Lorem ipsum";
  const bio = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do"
    + "eiusmod tempor incididunt ut labore et dolore magna aliqua.";
  const commits = 0;
  const issues = 0;
  const tests = 0;
  const pic = { sky };
  const linkedIn = 'Lorem ipsum dolor sit amet';
  const wrapper = shallow(<MemberCard
    name={name}
    job={job}
    bio={bio}
    commits={commits}
    issues={issues}
    tests={tests}
    pic={pic}
    linkedin={linkedIn}
  />);
  expect(toJson(wrapper)).toMatchSnapshot();
});

test('Resource Card Renders Correctly', () => {
  configure({ adapter: new Adapter() });
  const name = "John Doe";
  const about = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do"
    + "eiusmod tempor incididunt ut labore et dolore magna aliqua.";
  const pic = { sky };
  const link = 'Lorem ipsum dolor sit amet';
  const wrapper = shallow(<ResourceCard
    name={name}
    about={about}
    pic={pic}
    link={link}
  />);
  expect(toJson(wrapper)).toMatchSnapshot();
});

test('Tool Card Renders Correctly', () => {
  configure({ adapter: new Adapter() });
  const name = "John Doe";
  const use = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do"
    + "eiusmod tempor incididunt ut labore et dolore magna aliqua.";
  const pic = { sky };
  const link = 'Lorem ipsum dolor sit amet';
  const wrapper = shallow(<ToolCard
    name={name}
    use={use}
    link={link}
    pic={pic}
  />);
  expect(toJson(wrapper)).toMatchSnapshot();
});










