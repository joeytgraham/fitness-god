import React from 'react';

const DayRow = ({ day }) => {

    return (
        <tr className = "clickableRow" onClick={() => {window.location.assign(`/days/${day['date']}`);}}>
            <td>{day["date"]}</td>
            <td>{Number((day["aqi"]).toFixed(1))}</td>
            <td>{Number((day["co"]).toFixed(1))}</td>
            <td>{Number((day["no2"]).toFixed(1))}</td>
            <td>{Number((day["o3"]).toFixed(1))}</td>
            <td>{Number((day["so2"]).toFixed(1))}</td>
            <td>{Number((day["pm2_5"]).toFixed(1))}</td>
            <td>{Number((day["pm10"]).toFixed(1))}</td>
        </tr>
    );
};

export default DayRow;