import React from "react";
import { Card, Image } from "react-bootstrap";
import { Highlight } from "react-instantsearch-dom";

/* card to display city search result */
function SearchCityCard(props) {
    /* url to get city image */
    let cityImageURL = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&key=" + process.env.REACT_APP_GCP_API_KEY + "&photoreference=";

    return (
        <Card style={{ width: '24rem' }} onClick={() => { window.location.assign(`/cities/${props.hit.id}`); }}>
            {/* take user to city instance page when card is clicked */}
            <Card.Body>
                <Image
                    src={cityImageURL + props.hit.maps_place_photo_ref}
                    alt="Country Flag"
                    style={{ maxWidth: '80%', height: '150px', paddingBottom: '20px' }}>
                </Image>
                {/* city information. attribute that matched search query is highlighted */}
                <Card.Title>
                    <Highlight attribute='name'
                        tagName="mark" hit={props.hit} />
                </Card.Title>
                <Card.Text>
                    <b>Rank: </b>
                    <Highlight attribute='rank'
                        tagName="mark" hit={props.hit} />
                </Card.Text>
                <Card.Text>
                    <div className='row'>
                        <div className='col'>
                            <b>Region: </b>
                            <Highlight attribute='region'
                                tagName="mark" hit={props.hit} />
                        </div>
                        <div className='col'>
                            <b>Country: </b>
                            <Highlight attribute='country'
                                tagName="mark" hit={props.hit} />
                        </div>
                    </div>
                </Card.Text>
                <Card.Text>
                    <div className='row'>
                        <div className='col'>
                            <b>Latitude: </b>
                            <Highlight attribute='lat'
                                tagName="mark" hit={props.hit} />
                        </div>
                        <div className='col'>
                            <b>Longitude: </b>
                            <Highlight attribute='lng'
                                tagName="mark" hit={props.hit} />
                        </div>
                    </div>
                </Card.Text>
                <Card.Text>
                    <b>Latest AQI: </b>
                    {props.hit.past_aq_data.map((day, index) => (
                        <li>
                            <Highlight attribute={`past_aq_data[${index}].date`}
                                tagName="mark" hit={props.hit}>
                            </Highlight>
                        {" "}--{" "}
                            <b>AQI: </b>
                            <Highlight attribute={`past_aq_data[${index}].aqi`}
                                tagName="mark" hit={props.hit}>
                            </Highlight>
                        </li>
                    ))}
                </Card.Text>
            </Card.Body>
        </Card>
    );
}

export default SearchCityCard;
