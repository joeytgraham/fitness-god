import React, {Component} from 'react';
import { BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';

class BarChartComp extends React.Component {
    render() {
		return (
            <ResponsiveContainer width="100%" height = {400} >
              <BarChart
                width={500}
                height={300}
                data={this.props.data}
                margin={{
                  top: 5,
                  right: 30,
                  left: 20,
                  bottom: 5,
                }}
              >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="label" />
                <YAxis dataKey="value"/>
                <Tooltip />
                <Legend />
                <Bar dataKey="value" fill="#8884d8" />
              </BarChart>
            </ResponsiveContainer>
          );
	}
}
export default BarChartComp;