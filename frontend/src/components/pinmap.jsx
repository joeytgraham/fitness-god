import React from 'react';
import GoogleMapReact from 'google-map-react';
import Marker from './marker';

/* from https://levelup.gitconnected.com/"
+"reactjs-google-maps-with-custom-marker-ece0c7d184c4 */

/* displays world map with markers at all locations passed in */
const PinMap = (props) => {

    /* allows marker to be clicked and to redirect to target page */
    function markerClick(key, childProps){
      window.location.assign(props.baseURL + key)
    }

    /* display each location from the array as a marker */
    return (
        <div style={props.map_style !== undefined ?
        props.map_style: { height: '75vh', width: '90vh', margin:'10vh' }}>
          <GoogleMapReact
            bootstrapURLKeys={
              { key: process.env.REACT_APP_GCP_API_KEY }}
            defaultCenter={{lat: props.locations[0]['lat'], lng: props.locations[0]['lng'] }}
            defaultZoom={1}
            onChildClick = {markerClick}
          >
          {props.locations.map(location => (
              <Marker
                lat={location.lat}
                lng={location.lng}
                name={location.name}
                id={location.id}
                key={location.id}
                color="red"/>
          ))}
          </GoogleMapReact>
        </div>
      );
}

export default PinMap;
