import React, { Component } from "react";

/* error page for invalid route */
class NotFound extends Component {
    render() {
        document.title = 'Error 404 (Not Found)';
        return (
            <div className="jumbotron flex-fill">
                <h1 className="display-4 pageTitle">Not Found</h1>
            </div>
        );
    }
}

export default NotFound;