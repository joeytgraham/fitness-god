import React, { Component } from "react";
import { Navbar, Nav } from "react-bootstrap";
import SearchBar from './searchBar'

/* navigation bar for website */
class NavBar extends Component {

  render() {
    return (
      <Navbar expand="lg" className="navbar navbar-expand-lg navbar-dark bg-primary">
        <Navbar.Brand href="/" className="nav-link">
          <b>BreatheEasy</b>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link href="/countries">Countries</Nav.Link>
            <Nav.Link href="/cities">Cities</Nav.Link>
            <Nav.Link href="/days">Days</Nav.Link>
            <Nav.Link href="/our-visualizations">Our Visualizations</Nav.Link>
            <Nav.Link href="/provider-visualizations">Provider Visualizations</Nav.Link>
            <Nav.Link href="/about">About</Nav.Link>
          </Nav>
        </Navbar.Collapse>
        <SearchBar></SearchBar>
      </Navbar>
    );
  }
}

export default NavBar;
