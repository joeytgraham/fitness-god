import React from "react";

/* footer for the website */
class Footer extends React.Component {
  render() {
    return (
      <div>
        <div className="bg-primary footer text-white text-center">Breathe easy with BreatheEasy</div>
      </div>
    );
  }
}

export default Footer;
