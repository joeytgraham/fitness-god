import React from 'react';
import '../styles/marker.css';

/* map marker component for PinMap */
const Marker = (props) => {
    const { color, name } = props;
    return (
        <div>
            <div
                class="marker"
                style={{ backgroundColor: color, cursor: 'pointer' }}
                title={name} />
        </div>
    );
};

export default Marker;
