import React from "react";
import { Card } from "react-bootstrap";
import { Highlight } from "react-instantsearch-dom";

/* card to display day search result */
function SearchDayCard(props) {

    return (
        <Card style={{ width: '24rem' }} onClick={() => { window.location.assign(`/days/${props.hit.date}`); }}>
            {/* take user to day instance page when card is clicked */}
            <Card.Body>
                {/* day information. attribute that matched search query is highlighted */}
                <Card.Title>
                    <Highlight attribute='date'
                        tagName="mark" hit={props.hit} />
                </Card.Title>
                <Card.Text>
                    <b>Global AQI: </b>
                    <Highlight attribute='aqi'
                        tagName="mark" hit={props.hit} />
                </Card.Text>
                <Card.Text>
                    <div className='row'>
                        <div className='col'>
                            <b>Avg CO: </b>
                            <Highlight attribute='co'
                                tagName="mark" hit={props.hit} />
                        </div>
                        <div className='col'>
                            <b>Avg NH3: </b>
                            <Highlight attribute='nh3'
                                tagName="mark" hit={props.hit} />
                        </div>
                    </div>
                </Card.Text>
                <Card.Text>
                    <div className='row'>
                        <div className='col'>
                            <b>Avg NO: </b>
                            <Highlight attribute='no'
                                tagName="mark" hit={props.hit} />
                        </div>
                        <div className='col'>
                            <b>Avg NO2: </b>
                            <Highlight attribute='no2'
                                tagName="mark" hit={props.hit} />
                        </div>
                    </div>
                </Card.Text>
                <Card.Text>
                    <div className='row'>
                        <div className='col'>
                            <b>Avg O3: </b>
                            <Highlight attribute='o3'
                                tagName="mark" hit={props.hit} />
                        </div>
                        <div className='col'>
                            <b>Avg PM10: </b>
                            <Highlight attribute='pm10'
                                tagName="mark" hit={props.hit} />
                        </div>
                    </div>
                </Card.Text>
                <Card.Text>
                    <div className='row'>
                        <div className='col'>
                            <b>Avg PM2.5: </b>
                            <Highlight attribute='pm2_5'
                                tagName="mark" hit={props.hit} />
                        </div>
                        <div className='col'>
                            <b>Avg SO2: </b>
                            <Highlight attribute='so2'
                                tagName="mark" hit={props.hit} />
                        </div>
                    </div>
                </Card.Text>
                <Card.Text>
                    <div className='row'>
                        <div className='col'>
                            <b>Worst Cities: </b>
                            {props.hit.worst_cities.map((city, index) => (
                                <li>
                                    <Highlight attribute={`worst_cities[${index}].name`}
                                        tagName="mark" hit={props.hit}>
                                    </Highlight>
                                </li>
                            ))}
                        </div>
                        <div className='col'>
                            <b>Worst Countries: </b>
                            {props.hit.worst_countries.map((country, index) => (
                                <li>
                                    <Highlight attribute={`worst_countries[${index}].name`}
                                        tagName="mark" hit={props.hit}>
                                    </Highlight>
                                </li>
                            ))}
                        </div>
                    </div>
                </Card.Text>
            </Card.Body>
        </Card>
    );
}

export default SearchDayCard;
