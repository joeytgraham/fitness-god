import React, { useEffect } from 'react';
import * as d3 from 'd3';

/**
 * pie chart visualization
 * displays number of cuisines per subregion
 */
function PieChart(props) {

    /* grab arguments passed in */
    const {
        data,
        outerRadius,
        innerRadius
    } = props;

    const margin = 50;

    const width = 2 * outerRadius + margin + margin;
    const height = 2 * outerRadius + margin + margin;

    /* declare color scheme for pie chart sectors */
    const colorScale = d3
        .scaleOrdinal(d3.schemePaired);


    useEffect(() => {
        drawChart();
    }, [data]);

    /* sort data in non increasing order */
    const my_data = data.sort((a, b) => b.value - a.value);

    /* create the pie chart */
    function drawChart() {
        d3
            .select('#pieChart')
            .select('svg')
            .remove();

        /* create svg */
        const svg = d3
            .select('#pieChart')
            .append('svg')
            .attr('width', width)
            .attr('height', height)
            .append('g')
            .attr('transform', `translate(${width / 2}, ${height / 2})`);

        const arcGenerator = d3
            .arc()
            .innerRadius(innerRadius)
            .outerRadius(outerRadius);

        const pieGenerator = d3
            .pie()
            .padAngle(0)
            .value((d) => d.value);

        const arc = svg
            .selectAll()
            .data(pieGenerator(my_data))
            .enter();

        /* append sectors */
        arc
            .append('path')
            .attr('d', arcGenerator)
            .style('fill', (_, i) => colorScale(i))
            .style('stroke', '#ffffff')
            .style('stroke-width', 0);

        /* create legend for chart */
        const legend = d3
            .select('#pieChart')
            .append('svg')
            .attr('width', width)
            .attr('height', height)
            .selectAll('legendElems')
            .data(my_data)
            .enter()
            .append('g')
            .attr('transform', (d, i) => 'translate('
                + (width - 400) + (',') + (i * 20 + 60) + ')')
            .attr('class', 'legend')
            .style('fill', 'black');

        legend
            .append('rect')
            .attr('width', 10)
            .attr('height', 10)
            .attr('fill', (d, i) => colorScale(i));

        legend
            .append('text')
            .text((d) => `${d.label}: ${d.value}`)
            .style('font-size', 12)
            .style('color', 'black')
            .attr('y', 10)
            .attr('x', 15);
    }

    return <div id='pieChart'></div>;
}

export default PieChart;
