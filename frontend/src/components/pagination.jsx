import React, { useMemo } from 'react';
import Pagination from 'react-bootstrap/Pagination';

/* component to paginate responses */
const PaginationComponent = ({ paginate, currentPage, pageSize, totalInstances }) => {
    let totalPages = Math.ceil(totalInstances / pageSize)
    const paginationItems = useMemo(() => {
        const pages = [];
        /* display all pages if small number of pages */
        if (totalPages <= 7) {
            for (let i = 1; i <= totalPages; ++i) {
                pages.push(
                    <Pagination.Item
                        key={i}
                        active={i === currentPage}
                        onClick={() => paginate(i)}
                    >
                        {i}
                    </Pagination.Item>
                );
            }
        }

        /* starting window to display */
        else if (currentPage < 5) {
            for (let i = 1; i <= 5; i++) {
                pages.push(
                    <Pagination.Item
                        key={i}
                        active={i === currentPage}
                        onClick={() => paginate(i)}
                    >
                        {i}
                    </Pagination.Item>
                );
            }
            pages.push(<Pagination.Ellipsis disabled />);
            pages.push(
                <Pagination.Item
                    key={totalPages}
                    active={false}
                    onClick={() => paginate(totalPages)}
                >
                    {totalPages}
                </Pagination.Item>
            );
        }

        /* end window to display */
        else if (currentPage >= totalPages - 2) {
            pages.push(
                <Pagination.Item
                    key={1}
                    active={false}
                    onClick={() => paginate(1)}
                >
                    {1}
                </Pagination.Item>
            );
            pages.push(<Pagination.Ellipsis disabled />);
            for (let i = totalPages - 4; i <= totalPages; i++) {
                pages.push(
                    <Pagination.Item
                        key={i}
                        active={i === currentPage}
                        onClick={() => paginate(i)}
                    >
                        {i}
                    </Pagination.Item>
                );
            }
        }

        /* truncate both sides, intermediate window */
        else {
            pages.push(
                <Pagination.Item
                    key={1}
                    active={false}
                    onClick={() => paginate(1)}
                >
                    {1}
                </Pagination.Item>
            );
            pages.push(<Pagination.Ellipsis disabled />);
            for (let i = currentPage - 1; i <= currentPage + 1; i++) {
                pages.push(
                    <Pagination.Item
                        key={i}
                        active={i === currentPage}
                        onClick={() => paginate(i)}
                    >
                        {i}
                    </Pagination.Item>
                );
            }
            pages.push(<Pagination.Ellipsis disabled />);
            pages.push(
                <Pagination.Item
                    key={totalPages}
                    active={false}
                    onClick={() => paginate(totalPages)}
                >
                    {totalPages}
                </Pagination.Item>
            );
        }

        return pages;
    }, [totalPages, currentPage]);


    return (
        <div>
            <Pagination>
                {/* go to previous page */}
                <Pagination.Prev
                    onClick={() => paginate(currentPage - 1)}
                    disabled={currentPage === 1}
                />
                {paginationItems}
                {/* go to next page */}
                <Pagination.Next
                    onClick={() => paginate(currentPage + 1)}
                    disabled={currentPage === totalPages}
                />
            </Pagination>
            <div className='d-flex text-center justify-content-center'>
                {/* display how many results currently viewing out of total */}
                <p>{Math.min(totalInstances, pageSize * (currentPage - 1) + 1)}-{Math.min(totalInstances, (pageSize * currentPage))} of {totalInstances}</p>
            </div>
        </div>
    );
};

export default PaginationComponent;