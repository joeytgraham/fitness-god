import React from 'react';
import {Card} from "react-bootstrap";

const CountryCard = ({country}) => {
//   if (loading) {
//     return <h2>Loading...</h2>;
//   }
// const data = require("../assets/data/countries/" + id + ".json");

    return (
        <Card className="card col-sm-3 modelCard" style={{width: '14rem'}} onClick={() => {
            window.location.assign(`/countries/${country['id']}`);
        }}>
            <Card.Body className = "modelCardBody">
                <Card.Title>{country['name']}</Card.Title>
                <Card.Text>
                    <div className="cardData modelCardText">
                        <img className="countries-image" src={country['flag_url']} alt="flag"/>
                        <div>
                            Capital: {country['capital']}
                        </div>
                        <div>
                            Region: {country['region']}
                        </div>
                        <div>
                            Population: {country['population'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                        </div>
                        <div>
                            AQI: {Number((country['latest_aqi']).toFixed(1))}
                            <div className="infoTooltip"><i className='fas fa-info-circle'/>
                                <span className="tooltiptext">The Air Quality Index (AQI) is used for reporting daily air quality. It tells you how clean or polluted your air is, and what associated health effects might be a concern for you.</span>
                            </div>
                        </div>
                        <div>
                            Economic Status: {country['economic_status']}
                        </div>
                    </div>
                </Card.Text>
            </Card.Body>
        </Card>
    );
};

export default CountryCard;