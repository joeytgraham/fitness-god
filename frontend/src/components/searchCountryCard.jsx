import React from "react";
import { Card, Image } from "react-bootstrap";
import { Highlight } from "react-instantsearch-dom";
import DropdownButton from "react-bootstrap/DropdownButton";
import DropdownItem from "react-bootstrap/esm/DropdownItem";

/* card to display country search result */
function SearchCountryCard(props) {

    return (
        <Card style={{ width: '24rem' }}>
            {/* take user to country instance page when card is clicked */}
            <Card.Body onClick={() => { window.location.assign(`/countries/${props.hit.id}`); }}>
                <Image
                    src={props.hit.flag_url}
                    alt="Country Flag"
                    style={{ maxWidth: '80%', height: '150px', paddingBottom: '20px' }}>
                </Image>
                {/* country information. attribute that matched search query is highlighted */}
                <Card.Title>
                    <Highlight attribute='name'
                        tagName="mark" hit={props.hit} />
                </Card.Title>
                <Card.Text>
                    <div className='row'>
                        <div className='col'>
                            <b>Capital: </b>
                            <Highlight attribute='capital'
                                tagName="mark" hit={props.hit} />
                        </div>
                        <div className='col'>
                            <b>Region: </b>
                            <Highlight attribute='region'
                                tagName="mark" hit={props.hit} />
                        </div>
                    </div>
                </Card.Text>
                <Card.Text>
                    <div className='row'>
                        <div className='col'>
                            <b>Latitude: </b>
                            <Highlight attribute='lat'
                                tagName="mark" hit={props.hit} />
                        </div>
                        <div className='col'>
                            <b>Longitude: </b>
                            <Highlight attribute='lng'
                                tagName="mark" hit={props.hit} />
                        </div>
                    </div>
                </Card.Text>
                <Card.Text>
                    <b>Population: </b>
                    <Highlight attribute='population'
                        tagName="mark" hit={props.hit} />
                </Card.Text>
                <Card.Text>
                    <b>AQI: </b>
                    <Highlight attribute='latest_aqi'
                        tagName="mark" hit={props.hit} />
                </Card.Text>
                <Card.Text>
                    <b>Worst Day: </b>
                    <Highlight attribute='worst_day'
                        tagName="mark" hit={props.hit} />
                </Card.Text>
                <Card.Text>
                    <b>Economic Status: </b>
                    <Highlight attribute='economic_status'
                        tagName="mark" hit={props.hit} />
                </Card.Text>
            </Card.Body>
            {/* dropdown to display country's cities without making card long */}
            <DropdownButton className='citiesDrop' title="Cities" style={{ padding: '20px' }}>
                {props.hit.cities.map((city, index) => (
                    <div>
                        {/* take user to city instance page if city is clicked */}
                        <DropdownItem onClick={() => { window.location.assign(`/cities/${city.id}`); }}>
                            <Highlight attribute={`cities[${index}].name`}
                                tagName="mark" hit={props.hit}>
                            </Highlight>

                        </DropdownItem>
                    </div>
                ))}
            </DropdownButton>
        </Card>
    );
}

export default SearchCountryCard;
