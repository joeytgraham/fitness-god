import React from "react";
import { Form, FormControl, Button } from "react-bootstrap";

/* search bar for the nav bar */
const SearchBar = (props) => {

  /* to get user input from search bar */
  const userInput = React.useRef();
  
  /* takes user to search results page */
  function search() {
    window.location.assign("/search/q=" + userInput.current.value + "&filter=ALL");
  }

  return (
    <Form
      inline
      className="nav-search"
      onSubmit={(sub) => {
        sub.preventDefault();
      }}>
      <FormControl
        className="nav-searchbar"
        placeholder="Search"
        type="text"
        ref={userInput}
        onKeyPress={(event) => {
          /* takes user to search results page when 'enter' key is pressed */
          if (event.key === "Enter") {
            search();
          }
        }}>
      </FormControl>
      <Button
        variant="outline-light"
        /* takes user to search results page when submit button is clicked */
        onClick={() => search()}
      >Submit</Button>
    </Form>
  );
}

export default SearchBar;
