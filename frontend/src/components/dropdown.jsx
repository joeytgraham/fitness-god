import React from 'react';
import { Dropdown as BootstrapDropdown } from 'react-bootstrap';

const Dropdown = (props) => {
    return (
        <BootstrapDropdown>
            <BootstrapDropdown.Toggle variant='primary' id='dropdown'>
                <span>{props.label}</span>
            </BootstrapDropdown.Toggle>

            <BootstrapDropdown.Menu>
                {props.items.map((name, index) => (
                    <BootstrapDropdown.Item
                        key={index}
                        onClick={(event) => {
                            console.log(event.target.text);
                            props.setSelected(event.target.text.toLowerCase());
                        }}
                        value={name}
                    >
                        {name}
                    </BootstrapDropdown.Item>
                ))}
            </BootstrapDropdown.Menu>
        </BootstrapDropdown>
    );
};

export default Dropdown