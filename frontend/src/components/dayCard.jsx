import React from 'react';
import PinMap from "./pinmap";
import { Card } from "react-bootstrap";

const dayPosts = ({ data, color_classes, air_values, city_locations, country_locations }) => {
//   if (loading) {
//     return <h2>Loading...</h2>;
//   }
console.log("dataaa:" + JSON.stringify(data) + ".")
console.log("cityyy:" + JSON.stringify(city_locations) + ".")
console.log("countryy:" + country_locations + ".")
// console.log("dataaa:" + data)

    return (
        <div class="jumbotron">
            <h1 class="display-8 pageTitle">{data['date']}</h1>
            <div class='container-fluid' style={{ marginTop: '4%', marginBottom: '2%' }}>
                <div class="card-deck pt-3 mt-3 mx-3 justify-content-center row-cols-1">
                    {color_classes.map((color_class) => (
                        <div class={`card ${color_class['border']} m-0 pt-0 `} style={{minWidth: '10rem'}}>
                            <div class={`card-body scale ${color_class['class']} py-1 px-3`}>
                                <small>{color_class['description']}</small>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
            <div class="card-deck mx-3 mb-3 row-cols-2 row-cols-sm-6 justify-content-center ">
                {air_values.map((air_value) => (

                    <Card className={`card dayDataContainer ${air_value['color_class']}`} style={{ width: '10rem', minWidth: '10rem'}}>
                        <Card.Body>
                            <Card.Text>
                                <img class="card-img-top" src={air_value['src']}
                                    alt={air_value['name']} />
                                <b class="card-title">{air_value['name']}</b>
                                <br />
                                {Number((air_value['value']).toFixed(2))} {air_value['units']}
                            </Card.Text>
                        </Card.Body>
                    </Card>
                ))}
            </div>
            <div class="container-flex justify-content-center">
                <div class="row justify-content-center">
                    <div class="col-md embed-responsive text-center p-3">
                        <h3>Cities with the worst AQI on {data["date"]}</h3>
                        <p>Drag to view all cities. Click a dot to view the city!</p>
                        {<PinMap
                            locations={city_locations}
                            map_style={{ height: '40vh', width: '68vh', marginLeft: '8vw', marginRight: '8vw' }}
                            baseURL='/cities/' />}
                    </div>
                    <div className="col-md embed-responsive text-center p-3">
                        <h3>Countries with the worst AQI on {data["date"]}</h3>
                        <p>Drag to view all countries. Click a dot to view the country!</p>
                        {<PinMap
                            locations={country_locations}
                            map_style={{ height: '40vh', width: '68vh', marginLeft: '8vw', marginRight: '8vw' }}
                            baseURL='/countries/' />}
                    </div>
                </div>
            </div>
            <br/>
            <br/>

            <div className="holidays">
                <h5>Holidays on {data["date"]}</h5>
                <p id="holidays"></p>
            </div>
        </div>
    );
};

export default dayPosts;