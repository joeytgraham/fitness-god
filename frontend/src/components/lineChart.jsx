import React from 'react';
import { LineChart, Line, Tooltip, XAxis, YAxis, ResponsiveContainer } from "recharts";
import { format, parseISO} from "date-fns";
import '../styles/visualizations.css'

/* Line chart visualization to visualize carbon emissions data */
class LineChartComp extends React.Component {
    render() {
		return(
			<div className="LineChart">
				{/* Used line chart from recharts */}
				<ResponsiveContainer width = {"100%"} height = {400}>
					<LineChart
						//width={1000}
						//height={500}
						data={this.props.data}
						//margin={{ top: 20, right: 20, left: 290, bottom: 40 }}
						>
						<XAxis
							dataKey="label"
							tickFormatter={str => {
								const date = parseISO(str);
								return 	format(date, "MMM d");
							}}
							label={{
								value: "Date",
								position: "insideMiddle",
								dy: 20,
								fill: "black",
								fontSize: 20,
							}}
							height={50}
						/>
						<YAxis
							dataKey="value"
							label={{
								value: "AQI",
								position: "insideLeft",
								angle: -90,
								dy: 20,
								fill: "black",
								fontSize: 20,
							}}
						/>
						<Tooltip
							content={<CustomTooltip />} 
						/>
						<Line
							dataKey="value"
							dot={false}
							strokeWidth="3"
						/>
					</LineChart>
				</ResponsiveContainer>
			</div>
		);
	}
}

function CustomTooltip({active, payload, label}) {
	if (active) {
		return (
			<div /*className="tooltip"*/>
				<h4>{format(parseISO(label), "eeee, MMM d, yyyy")}</h4>
				<p>
					{payload[0].value}
				</p>
			</div>
		);
	}
	return null;
}

export default LineChartComp;