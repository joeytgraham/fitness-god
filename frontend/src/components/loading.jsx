import React from "react";

/* loading spinner for images and data */
class Loading extends React.Component {
    render() {
        return (
            <div className="spinner-border align-self-center mx-auto" role="status">
                <span className="sr-only">Loading...</span>
            </div>
        );
    }
}

export default Loading;