import React from 'react';
import { Card, Image, Spinner } from "react-bootstrap";

const CityCard = ({ city, history }) => {
    //   if (loading) {
    //     return <h2>Loading...</h2>;
    //   }

    let cityImageURL = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&key=" + process.env.REACT_APP_GCP_API_KEY + "&photoreference=";

    return (
        <Card className="card col-sm-3 modelCard" style={{ width: '18rem' }} onClick={() => { window.location.assign(`/cities/${city['id']}`); }}>
            <Card.Body className = "modelCardBody">
                <Card.Title>{city['name']}</Card.Title>
                <Card.Text>
                    <div className="cardData modelCardText">
                        <div>
                            {city['maps_place_photo_ref'] === null ?
                                (<Spinner animation="border" />) :
                                (<Image className="cities-image" src={cityImageURL + city['maps_place_photo_ref']} alt="City Image"/>)}

                        </div>
                        <div>
                            Region: {city['region']}
                        </div>
                        <div>
                            Country: {city['country']}
                        </div>
                        <div>
                            Latitude: {city['lat']}
                        </div>
                        <div>
                            Longitude: {city['lng']}
                        </div>
                        <div>
                            AQI: {Number((city['latest_aqi']).toFixed(1))}
                            <div className="infoTooltip"><i className='fas fa-info-circle'></i>
                                <span className="tooltiptext">The Air Quality Index (AQI) is used for reporting daily air quality. It tells you how clean or polluted your air is, and what associated health effects might be a concern for you.</span>
                            </div>
                        </div>
                    </div>
                </Card.Text>
            </Card.Body>
        </Card>
    );
};

export default CityCard;