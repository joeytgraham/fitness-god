import React from "react";

/* splash page for the website */
const LandingPage = () => {
  return (
    <div className='flex-fill'>
      <div className="jumbotron banner">
        <h1 className="display-3">BreatheEasy</h1>
        <p className="lead">Providing you with air pollution information on major cities around the globe</p>
        <p className="lead">
          <a className="btn btn-primary btn-lg" href="/about" role="button">Learn more</a>
        </p>
      </div>

      {/* inform about different information throughout the site  */}
      <div className="jumbotron sec sec1">
        <div className="secContainer">
          <h1 className="display-3 secText">Learn more about air quality in major cities</h1>
          <p className="lead">
            <a className="btn btn-primary btn-lg" href="/cities" role="button">Cities</a>
          </p>
        </div>
      </div>

      <div className="jumbotron sec sec2">
        <div className="secContainer">
          <h1 className="display-3 secText">Learn more about air quality in countries</h1>
          <p className="lead">
            <a className="btn btn-primary btn-lg" href="/countries" role="button">Countries</a>
          </p>
        </div>
      </div>

      <div className="jumbotron sec sec3">
        <div className="secContainer">
          <h1 className="display-3 secText">Learn more about air quality over time</h1>
          <p className="lead">
            <a className="btn btn-primary btn-lg" href="/days" role="button">Days</a>
          </p>
        </div>
      </div>
    </div>
  );
};

export default LandingPage;