import React, { useEffect, useState } from "react";
import PaginationComponent from "../components/pagination"
import axios from "axios";
import CountryCard from "../components/countriesCard"
import Loading from "../components/loading";
import MultiSelect from "react-multi-select-component";
import '../styles/Multiselect.css'
import { Form, FormControl, Button } from "react-bootstrap";


//Functional Component
const CountriesPage = ({ paginationSize }) => {
    const [loading, setLoading] = useState(true);
    const [params, setParams] = useState({'limit': paginationSize});
    const [data, setData] = useState([]);
    const [currentCountries, setCurrentCountries] = useState([]);
    const [selectedFilterLabels, setSelectedFilterLabels] = useState([]); //previously default selected
    const [selectedSortLabels, setSelectedSortLabels] = useState([]); //previously default selected
    const [filterOptions, setFilterOptions] = useState([]);
    const [sortOptions, setSortOptions] = useState([]); //new selections
    const [currentPage, setCurrentPage] = useState(1);
    const [totalInstances, setTotalInstances] = useState(0);
    const [disabledCapitals, setDisabledCapitals] = useState([false, false]);
    const [disabledRegions, setDisabledRegions] = useState([false, false]);
    const [disabledPopulation, setDisabledPopulation] = useState([false, false]);
    const [disabledName, setDisabledName] = useState([false, false]);
    const [disabledAQI, setDisabledAQI] = useState([false, false]);
    const [disabledEcon, setDisabledEcon] = useState([false, false]);
    const pageSize = paginationSize;
    const userInput = React.useRef();


    const optionsFilterArray = [
        { value: "name / gte(name,A) / lte(name,I)", label: "Country A-I"},
        { value: "name / gte(name,J) / lte(name,R)", label: "Country J-R" },
        { value: "name / gte(name,S) / lte(name,Z)", label: "Country S-Z" },
        { value: "capital / gte(capital,A) / lte(capital,I)", label: "Capital A-I" },
        { value: "capital / gte(capital,J) / lte(capital,R)", label: "Capital J-R" },
        { value: "capital / gte(capital,S) / lte(capital,Z)", label: "Capital S-Z" },
        { value: "region / gte(region,A) / lte(region,I)", label: "Region A-I" },
        { value: "region / gte(region,J) / lte(region,R)", label: "Region J-R" },
        { value: "region / gte(region,S) / lte(region,Z)", label: "Region S-Z" },
        { value: "population / gte(population, 50000000)", label: "High Population" },
        { value: "population / gte(population, 10000000) / lte(population, 50000000)", label: "Average Population" },
        { value: "population / lte(population, 10000000)", label: "Low Population" },
        { value: "economic_status / eq(economic_status, High income)", label: "High Economic Status" },
        { value: "economic_status / eq(economic_status, Upper middle income)", label: "Upper Middle Economic Status" },
        { value: "economic_status / eq(economic_status, Lower middle income)", label: "Lower Middle Economic Status" },
        { value: "economic_status / eq(economic_status, Low income)", label: "Low Economic Status" },
    ];

    const optionsSortArray = [
        { label: "Name A - Z", value: "asc(name)", disabled: disabledName["asc(name)"] },
        { label: "Name Z - A", value: "desc(name)", disabled: disabledName["desc(name)"] },
        { label: "Capital A - Z", value: "asc(capital)", disabled: disabledCapitals["asc(capital)"] },
        { label: "Capital Z - A", value: "desc(capital)", disabled: disabledCapitals["desc(capital)"] },
        { label: "Region A - Z", value: "asc(region)", disabled: disabledRegions["asc(region)"] },
        { label: "Region Z - A", value: "desc(region)", disabled: disabledRegions["desc(region)"] },
        { label: "Population high - low", value: "desc(population)", disabled: disabledPopulation["desc(population)"] },
        { label: "Population low - high", value: "asc(population)", disabled: disabledPopulation["asc(population)"] },
        { label: "AQI high - low", value: "desc(latest_aqi)", disabled: disabledAQI["desc(latest_aqi)"] },
        { label: "AQI low - high", value: "asc(latest_aqi)", disabled: disabledAQI["asc(latest_aqi)"] },
        { label: "Economic Status high - low", value: "desc(economic_status)", disabled: disabledEcon["desc(economic_status)"] },
        { label: "Economic Status low - high", value: "asc(economic_status)", disabled: disabledEcon["asc(economic_status)"] },
    ];
    const filterFields = [
        "id", "name", "capital", "region", "population", "lat", "lng", "economic_status", "latest_aqi", "flag_url"
    ];

    // perform search upon searchbar action
    function search() {
        window.location.assign("/search/q=" + userInput.current.value + "&filter=COUNTRIES" );
    }

    // api call with params
    useEffect(() => {
        const getData = async () => {
            setLoading(true);
            const res = await axios.get(`${process.env.REACT_APP_API_URL}/countries`, {
                params: params
            })
            setData(res.data);
            setLoading(false);
        };

        getData();
    }, [params]);


    // set filter parameters
    useEffect(() => {
        const changeParams = () => {
            var filterParams = {}
            var filterKeys = []
            var filterValues = []
            var filterCall = ""
            var tempFilterSelectedSort = []
            var tempFilterOptions = optionsFilterArray.filter(o => filterOptions.includes(o["value"]))
            tempFilterOptions.map((o) => (
                tempFilterSelectedSort.push(o)
            ))
            setSelectedFilterLabels(tempFilterOptions)
            for (var i = 0; i < filterOptions.length; i++) {
                var filterArr = filterOptions[i].split(" / ")
                filterKeys[i] = filterArr[0]
                filterValues[i] = []
                filterValues[i][0] = filterArr[1]
                if (filterArr[2]) {
                    filterValues[i][1] = (filterArr[2])
                }
            }
            filterFields.map((s) => (
                filterParams[s] = []
            ))
            var index = 0;
            filterValues.map((s) => (
                filterParams[filterKeys[index++]].push(s)
            ))
            var expressions = []
            var exp1 = ""
            for (var key in filterParams) {
                if (filterParams[key].length > 0) {
                    exp1 = ""
                    if (filterParams[key].length === 1) {
                        if (filterParams[key][0].length === 1) {
                            exp1 = filterParams[key];
                        } else {
                            exp1 = "and(" + filterParams[key][0][0] + "," + filterParams[key][0][1] + ")"
                        }
                    } else {
                        for (i = 0; i < filterParams[key].length; i++) {
                            var exp2 = ""
                            if (filterParams[key][i].length === 1) {
                                exp2 = filterParams[key];
                            } else {
                                exp2 = "and(" + filterParams[key][i][0] + "," + filterParams[key][i][1] + ")"
                            }
                            exp1 += exp2
                            if (i !== filterParams[key].length - 1) {
                                exp1 += ","
                            }

                        }
                        exp1 = "or(" + exp1 + ")"

                    }
                    expressions.push(exp1)
                }
            }
            if (expressions.length > 1) {
                for (i = 0; i < expressions.length; i++) {
                    filterCall += expressions[i] + ""
                    if (i !== expressions.length - 1) {
                        filterCall += ","
                    }
                }
                filterCall = "and(" + filterCall + ")"
            } else {
                filterCall = exp1 + ""
            }
            var tempParams = new Map()
            for (i in params) {
                tempParams[i] = params[i];
            }
            if (filterCall.length > 0) {
                tempParams['filter'] = filterCall
            }
            setParams(tempParams)
        }
        changeParams();
    }, [filterOptions])

    // set pagination parameters
    useEffect(() => {
        const changeParams = () => {
            var tempParams = {}
            for (var i in params) {
                tempParams[i] = params[i];
            }
            tempParams['limit'] = pageSize
            tempParams['offset'] = (currentPage - 1) * pageSize
            setParams(tempParams)
        }
        changeParams();
    }, [currentPage])

    // set sort parameters
    useEffect(() => {
        const changeParams = () => {
            var tempParams = {}
            const sortValues = []
            var tempSelectedSort = []
            sortOptions.map((s) => (
                sortValues.push(s + "")
            ))
            var tempOptions = optionsSortArray.filter(o => sortOptions.includes(o["value"]))
            tempOptions.map((o) => (
                tempSelectedSort.push(o)
            ))
            for (var i in params) {
                tempParams[i] = params[i];
            }
            tempParams['sort_by'] = sortValues
            setSelectedSortLabels(tempSelectedSort)
            setParams(tempParams)
        }
        changeParams();
    }, [sortOptions])

    useEffect(() => {
        if (!loading) {
            setCurrentCountries(data['results'])
            setTotalInstances(data['total_count'])
        }
    }, [loading]);

    // adjustment for react-multi-select-component
    const parseOptions = (rawOptions) => {
        var temp = []
        const rawNonNull = rawOptions.filter(r => r !== null)
        rawNonNull.map((r) => (
            temp.push(r["value"])
        ))
        return temp
    }

    //disabled option if its opposite is chosen 
    useEffect(() => {
        var tempSelectedVals = []
        selectedSortLabels.map((s) => (
            tempSelectedVals.push(s["value"])
        ))

        setDisabledCapitals(sortDropdownXORLogic(tempSelectedVals, "asc(capital)", "desc(capital)"))
        setDisabledRegions(sortDropdownXORLogic(tempSelectedVals, "asc(region)", "desc(region)"))
        setDisabledPopulation(sortDropdownXORLogic(tempSelectedVals, "asc(population)", "desc(population)"))
        setDisabledAQI(sortDropdownXORLogic(tempSelectedVals, "asc(latest_aqi)", "desc(latest_aqi)"))
        setDisabledEcon(sortDropdownXORLogic(tempSelectedVals, "asc(economic_status)", "desc(economic_status)"))
        setDisabledName(sortDropdownXORLogic(tempSelectedVals, "asc(name)", "desc(name)"))
    }, [selectedSortLabels])

    //helper that XORs
    const sortDropdownXORLogic = (vals, asc, desc) => {
        var temp = []
        if (vals.includes(asc)) {
            temp[desc] = true
            temp[asc] = false
        } else if (vals.includes(desc)) {
            temp[asc] = true
            temp[desc] = false
        } else {
            temp[asc] = false
            temp[desc] = false
        }
        return temp
    }

    return (
        <div className="jumbotron flex-fill">
            <h1 className="display-4 pageTitle">Countries</h1>
            <hr className="my-4" />
            <div class='row no-gutters'>
                <div class='col-md-5 w-auto ml-auto'>
                    <Form 
                        inline 
                        onSubmit={(sub) => {
                            sub.preventDefault();
                        }}>
                        <FormControl
                            className="model-search"
                            placeholder="Search"
                            type="text"
                            ref={userInput}
                            onKeyPress={(event) => {
                                if (event.key === "Enter") {
                                    search();
                                }
                            }}>
                        </FormControl>
                        <Button 
                            className = "search-submit"
                            variant="light"
                            onClick={() => search()}>Submit</Button>
                    </Form>
                </div>
                <div class='col-md-5 w-auto ml-auto'>
                    <MultiSelect
                        options={optionsFilterArray}
                        value={selectedFilterLabels}
                        onChange={(rawOptions) => setFilterOptions(parseOptions(rawOptions))}
                        // labelledBy={"Filter"}
                        hasSelectAll={false}
                        className={"multi-select float-right custom"}
                        overrideStrings={{ "selectSomeItems": "Filter" }}
                    />
                    <MultiSelect
                        options={optionsSortArray}
                        value={selectedSortLabels}
                        onChange={(rawOptions) => setSortOptions(parseOptions(rawOptions))}
                        labelledBy={"Sort By"}
                        hasSelectAll={false}
                        className={"multi-select float-right custom model-sort"}
                        overrideStrings={{ "selectSomeItems": "Sort by..." }}
                    />
                </div>
            </div>
            {/* <h2> TEST Filtering by {selectedFilterLabels}</h2> */}
            <br/>
            {loading ? (<Loading />) :
                <div className='row'>
                    {currentCountries.map((country) => (
                        <CountryCard country={country} />
                    ))}
                </div>
            }
            <br/>
            <PaginationComponent
                paginate={page => {
                    window.scrollTo(0, 0);
                    setCurrentPage(page)
                }}
                currentPage={currentPage}
                pageSize={pageSize}
                totalInstances={totalInstances}
            />
        </div>);

}

export default CountriesPage;
