import React, {useEffect, useState} from "react";
import {Table} from "react-bootstrap";
import axios from "axios";
import DayRow from "../components/daysCard";
import PaginationComponent from "../components/pagination";
import Loading from "../components/loading";
import MultiSelect from "react-multi-select-component";
import '../styles/Multiselect.css'
import { Form, FormControl, Button } from "react-bootstrap";

//Functional Component 
const DaysPage = ({paginationSize}) => {
    const [loading, setLoading] = useState(true);
    const [data, setData] = useState([]);
    const [params, setParams] = useState({'limit': paginationSize});
    const [currentDays, setCurrentDays] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [totalInstances, setTotalInstances] = useState(0);
    const [selectedFilterLabels, setSelectedFilterLabels] = useState([]);
    const [selectedSortLabels, setSelectedSortLabels] = useState([]); //previously default selected
    const [filterOptions, setFilterOptions] = useState([]);
    const [sortOptions, setSortOptions] = useState([]); //new selections
    const [disabledDate, setDisabledDate] = useState([false, false]);
    const [disabledAQI, setDisabledAQI] = useState([false, false]);
    const [disabledCO, setDisabledCO] = useState([false, false]);
    const [disabledNO2, setDisabledNO2] = useState([false, false]);
    const [disabledO3, setDisabledO3] = useState([false, false]);
    const [disabledSO2, setDisabledSO2] = useState([false, false]);
    const [disabledPM2_5, setDisabledPM2_5] = useState([false, false]);
    const [disabledPM10, setDisabledPM10] = useState([false, false]);
    const pageSize = paginationSize;
    const userInput = React.useRef();

    function dateToString(daysAgo) {
        var dateObj = new Date()
        dateObj.setDate(dateObj.getDate() - daysAgo);
        var month = dateObj.getUTCMonth() + 1;
        var day = dateObj.getUTCDate();
        var year = dateObj.getUTCFullYear();
        return year + "-" + month + "-" + day;
    }

    var sevenDaysAgo = dateToString(7);
    var thirtyDaysAgo = dateToString(30);
    
    const optionsFilterArray = [
        { value: "date / gt(date," + sevenDaysAgo + ")", label: "Last 7 Days" },
        { value: "date / gt(date," + thirtyDaysAgo + ")", label: "Last 30 Days" },

        { value: "aqi / lte(aqi,70)", label: "aqi <= 70" },
        { value: "aqi / gt(aqi,70) / lt(aqi,90)", label: "70 < aqi < 90" },
        { value: "aqi / gte(aqi,90)", label: "aqi >= 90" },

        { value: "co / lte(co,600)", label: "co <= 600" },
        { value: "co / gt(co,600) / lt(co,700)", label: "600 < co < 700" },
        { value: "co / gte(co,700)", label: "co >= 700" },

        { value: "no2 / lte(no2,16)", label: "no2 <= 16" },
        { value: "no2 / gt(no2,16) / lt(no2,22)", label: "16 < no2 < 22" },
        { value: "no2 / gte(no2,22)", label: "no2 >= 22" },

        { value: "o3 / lte(o3,30)", label: "o3 <= 30" },
        { value: "o3 / gt(o3,30) / lt(o3,34)", label: "30 < o3 < 34" },
        { value: "o3 / gte(o3,34)", label: "o3 >= 34" },

        { value: "so2 / lte(so2,12)", label: "so2 <= 12" },
        { value: "so2 / gt(so2,12) / lt(so2,14)", label: "12 < so2 < 14" },
        { value: "so2 / gte(so2,14)", label: "so2 >= 14" },

        { value: "pm2_5 / lte(pm2_5,40)", label: "pm2_5 <= 40" },
        { value: "pm2_5 / gt(pm2_5,40) / lt(pm2_5,50)", label: "40 < pm2_5 < 50" },
        { value: "pm2_5 / gte(pm2_5,50)", label: "pm2_5 >= 50" },

        { value: "pm10 / lte(pm10,50)", label: "pm10 <= 50" },
        { value: "pm10 / gt(pm10,50) / lt(pm10,70)", label: "50 < pm10 < 70" },
        { value: "pm10 / gte(pm10,70)", label: "pm10 >= 70" },
      ];

      const filterFields = [
        "date", "aqi", "co", "no2", "o3", "so2", "pm2_5", "pm10"
      ];

    const optionsSortArray = [
        { value: "asc(date)",   label: "Date earliest",     disabled: disabledDate["asc(date)"] },
        { value: "desc(date)",  label: "Date latest",       disabled: disabledDate["desc(date)"] },
        { value: "desc(aqi)",   label: "AQI high - low",    disabled: disabledAQI["desc(aqi)"] },
        { value: "asc(aqi)",    label: "AQI low - high",    disabled: disabledAQI["asc(aqi)"] },
        { value: "desc(co)",    label: "CO high - low",     disabled: disabledCO["desc(co)"] },
        { value: "asc(co)",     label: "CO low - high",     disabled: disabledCO["asc(co)"] },
        { value: "desc(no2)",   label: "NO2 high - low",    disabled: disabledNO2["desc(no2)"] },
        { value: "asc(no2)",    label: "NO2 low - high",    disabled: disabledNO2["asc(no2)"] },
        { value: "desc(o3)",    label: "O3 high - low",     disabled: disabledO3["desc(o3)"] },
        { value: "asc(o3)",     label: "O3 low - high",     disabled: disabledO3["asc(o3)"] },
        { value: "desc(so2)",   label: "SO2 high - low",    disabled: disabledSO2["desc(so2)"] },
        { value: "asc(so2)",    label: "SO2 low - high",    disabled: disabledSO2["asc(so2)"] },
        { value: "desc(pm2_5)", label: "PM2.5 high - low",  disabled: disabledPM2_5["desc(pm2_5)"] },
        { value: "asc(pm2_5)",  label: "PM2.5 low - high",  disabled: disabledPM2_5["asc(pm2_5)"] },
        { value: "desc(pm10)",  label: "PM10 high - low",   disabled: disabledPM10["desc(pm10)"] },
        { value: "asc(pm10)",   label: "PM10 low - high",   disabled: disabledPM10["asc(pm10)"] },
      ];

    // set filter parameters
    useEffect(() => {
        const changeParams = () => {
            var filterParams = {}
            var filterKeys = []
            var filterValues = []
            var filterCall = ""
            var tempFilterSelectedSort = []
            var tempFilterOptions = optionsFilterArray.filter(o => filterOptions.includes(o["value"]))
            tempFilterOptions.map((o) => (
                tempFilterSelectedSort.push(o)
            ))
            setSelectedFilterLabels(tempFilterOptions)
            for (var i = 0; i < filterOptions.length; i++) {
                var filterArr = filterOptions[i].split(" / ")
                filterKeys[i] = filterArr[0]
                filterValues[i] = []
                filterValues[i][0] = filterArr[1]
                if (filterArr[2]) {
                    filterValues[i][1] = (filterArr[2])
                }
            }
            filterFields.map((s) => (
                filterParams[s] = []
            ))
            var index = 0;
            filterValues.map((s) => (
                filterParams[filterKeys[index++]].push(s)
            ))
            var expressions = []
            var exp1 = ""
            for (var key in filterParams) {
                if (filterParams[key].length > 0) {
                    exp1 = ""
                    if (filterParams[key].length === 1) {
                        if (filterParams[key][0].length === 1) {
                            exp1 = filterParams[key];
                        } else {
                            exp1 = "and(" + filterParams[key][0][0] + "," + filterParams[key][0][1] + ")"
                        }
                    } else {
                        for (i = 0; i < filterParams[key].length; i++) {
                            var exp2 = ""
                            if (filterParams[key][i].length === 1) {
                                exp2 = filterParams[key];
                            } else {
                                exp2 = "and(" + filterParams[key][i][0] + "," + filterParams[key][i][1] + ")"
                            }
                            exp1 += exp2
                            if (i !== filterParams[key].length - 1) {
                                exp1 += ","
                            }

                        }
                        exp1 = "or(" + exp1 + ")"

                    }
                    expressions.push(exp1)
                }
            }
            if (expressions.length > 1) {
                for (i = 0; i < expressions.length; i++) {
                    filterCall += expressions[i] + ""
                    if (i !== expressions.length - 1) {
                        filterCall += ","
                    }
                }
                filterCall = "and(" + filterCall + ")"
            } else {
                filterCall = exp1 + ""
            }
            var tempParams = new Map()
            for (i in params) {
                tempParams[i] = params[i];
            }
            if (filterCall.length > 0) {
                tempParams['filter'] = filterCall
            }
            setParams(tempParams)
        }
        changeParams();
    }, [filterOptions])

    // set pagination parameters
    useEffect(() => {
        const changeParams = () => {
            var tempParams = {}
            for (var i in params) {
                tempParams[i] = params[i];
            }
            tempParams['limit'] = pageSize
            tempParams['offset'] = (currentPage - 1) * pageSize
            setParams(tempParams)
        }
        changeParams();
    }, [currentPage])

    // set sort parameters
    useEffect(() => {
        const changeParams = () => {
            var tempParams = {}
            const sortValues = []
            var tempSelectedSort = []
            sortOptions.map((s) => (
                sortValues.push(s + "")
            ))
            var tempOptions = optionsSortArray.filter(o => sortOptions.includes(o["value"]))
            tempOptions.map((o) => (
                tempSelectedSort.push(o)
            ))
            for (var i in params) {
                tempParams[i] = params[i];
            }
            tempParams['sort_by'] = sortValues
            setSelectedSortLabels(tempSelectedSort)
            setParams(tempParams)
        }
        changeParams();
    }, [sortOptions])
    
    
    // adjustment for new multisort component
    const parseOptions = (rawOptions) => {
        var temp = []
        const rawNonNull = rawOptions.filter(r => r !== null)
        rawNonNull.map((r) => (
            temp.push(r["value"])
        ))
        return temp
    }

    //disabled option if its opposite is chosen 
    useEffect(() => {
        var tempSelectedVals = []
        selectedSortLabels.map((s) => (
            tempSelectedVals.push(s["value"])
        ))
        
        setDisabledDate(sortDropdownXORLogic(tempSelectedVals, "asc(date)", "desc(date)" ))
        setDisabledAQI(sortDropdownXORLogic(tempSelectedVals, "asc(aqi)", "desc(aqi)" ))
        setDisabledCO(sortDropdownXORLogic(tempSelectedVals, "asc(co)", "desc(co)" ))
        setDisabledNO2(sortDropdownXORLogic(tempSelectedVals, "asc(no2)", "desc(no2)" ))
        setDisabledO3(sortDropdownXORLogic(tempSelectedVals, "asc(o3)", "desc(o3)" ))
        setDisabledSO2(sortDropdownXORLogic(tempSelectedVals, "asc(so2)", "desc(so2)" ))
        setDisabledPM2_5(sortDropdownXORLogic(tempSelectedVals, "asc(pm2_5)", "desc(pm2_5)" ))
        setDisabledPM10(sortDropdownXORLogic(tempSelectedVals, "asc(pm10)", "desc(pm10)" ))
    }, [selectedSortLabels])

    //helper that XORs
    const sortDropdownXORLogic = (vals, asc, desc) => {
        var temp = []
        if(vals.includes(asc)) {
            temp[desc] = true
            temp[asc] = false
        } else if (vals.includes(desc)){
            temp[asc] = true
            temp[desc] = false
        } else {
            temp[asc] = false
            temp[desc] = false
        }
        console.log("temp: " + asc + " " + temp[asc] + " " + temp[desc])
        return temp
    }

    useEffect(() => {
        const getData = async () => {
            setLoading(true);  
            const res = await axios.get(`${process.env.REACT_APP_API_URL}/days`, {
                params: params
              })
              console.log(res)
              
            setData(res.data);
            setLoading(false);
        };
    
        getData();
    }, [params]);


    useEffect(() => {
        if (!loading) {
            setCurrentDays(data['results'])
            setTotalInstances(data['total_count'])
        }
    }, [loading]);

    function search() {
        window.location.assign("/search/q=" + userInput.current.value + "&filter=DAYS" );
    }

  
    return (
        <div className="jumbotron flex-fill">
            <h1 className="display-4 pageTitle">Days</h1>
            <hr className="my-4"/>
            <div class='row no-gutters'>
            <div class='col-md-5 w-auto ml-auto'>
                    <Form
                        inline
                        onSubmit={(sub) => {
                            sub.preventDefault();
                        }}>
                        <FormControl
                            className = "model-search"
                            placeholder="Search"
                            type="text"
                            ref={userInput}
                            onKeyPress={(event) => {
                                if (event.key === "Enter") {
                                    search();
                                }
                            }}>
                        </FormControl>
                        <Button 
                            className = "search-submit"
                            variant="light"
                            onClick={() => search()}>Submit</Button>
                    </Form>
                </div>
                <div class='col-md-5 w-auto ml-auto'>
                <MultiSelect
                        options={optionsFilterArray}
                        value={selectedFilterLabels}
                        onChange={(rawOptions) => setFilterOptions(parseOptions(rawOptions))}
                        hasSelectAll={false}
                        className={"multi-select float-right custom"}
                        overrideStrings={{ "selectSomeItems": "Filter" }}
                    />
                <MultiSelect
                    options={optionsSortArray}
                    value={selectedSortLabels}
                    onChange={(rawOptions) => setSortOptions(parseOptions(rawOptions))}
                    labelledBy={"Sort By"}
                    hasSelectAll={false}
                    className={"multi-select float-right custom model-sort"}
                    overrideStrings={{"selectSomeItems": "Sort by..."}}
                />  
                </div>
            </div>
            <br/>
            {loading ? (<Loading/>) :
                <Table responsive hover>
                    <thead>
                    <tr>
                        <th>Day</th>
                        <th>Global Avg AQ</th>
                        <th>Avg CO</th>
                        <th>Avg NO2</th>
                        <th>Avg O3</th>
                        <th>Avg SO2</th>
                        <th>Avg PM2.5</th>
                        <th>Avg PM10</th>
                    </tr>
                    </thead>
                    <tbody>
                    {currentDays.map((day) => (
                        <DayRow day={day}/>
                    ))}
                    </tbody>
                </Table>
            }
            <br/>
            <PaginationComponent
                paginate={page => {
                    window.scrollTo(0, 0);
                    setCurrentPage(page)
                }}
                currentPage={currentPage}
                pageSize={pageSize}
                totalInstances={totalInstances}
            />
        </div>
    );
};

export default DaysPage;
