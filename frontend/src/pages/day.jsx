import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import pm2_5 from '../assets/images/pm2_5.png'
import pm10 from '../assets/images/pm10.png'
import aqi from '../assets/images/aqi_cloud.png'
import '../styles/aqi_colors.css'
import { Card } from 'react-bootstrap'
import PinMap from '../components/pinmap'
import useAxios from "axios-hooks";
import axios from "axios";
import Loading from "../components/loading";
import NotFound from "../components/notFound";

//Functional Component

const color_classes = [
    { 'class': 'aqi-green', 'description': 'Good', 'border': 'border-0' },
    { 'class': 'aqi-yellow', 'description': 'Moderate', 'border': 'border-0' },
    { 'class': 'aqi-orange', 'description': 'Moderately Unhealthy', 'border': 'border-0' },
    { 'class': 'aqi-red', 'description': 'Unhealthy', 'border': 'border-0' },
    { 'class': 'aqi-purple', 'description': 'Very unhealthy', 'border': 'border-0' },
    { 'class': 'aqi-maroon', 'description': 'Hazardous', 'border': 'border-0' },
    { 'class': '', 'description': 'Scale not available', 'border': 'rounded-0' }
];

const compound_cutoffs = {
    'aqi': [51, 101, 151, 201, 301],
    'co': [5152.5, 10877.5, 14312.5, 17747.5, 34922.5],
    'no2': [101.52, 189.88, 678.68, 1222.0, 2350.0],
    'o3': [110.0, 141.99999999999997, 171.99999999999997, 212.0],
    'so2': [94.32000000000001, 199.12, 487.32, 799.1, 1585.1000000000001],
    'pm2_5': [12.1, 35.5, 55.5, 150.5, 250.5],
    'pm10': [55, 155, 255, 355, 425]
}

const DayPage = () => {
    const { id } = useParams();

    const [waitingForDay, setWaitingForDay] = useState(true);
    const [waitingForHoliday, setWaitingForHoliday] = useState(true);
    const [day, setDay] = useState([]);
    const [holidays, setHolidays] = useState(['None']);

    const [airValues, setAirValues] = useState({});

    const [{data, loading, error}] = useAxios(`${process.env.REACT_APP_API_URL}/days/${id}`);

    // console.log(day);
    // console.log(cityLocations);
    // console.log(countryLocations);
    // console.log(holidays);

    useEffect(() => {
        if(waitingForHoliday) {
            let year = id.substring(0, 4);
            let month = id.substring(5, 7);
            let day = id.substring(8);

            axios.get('https://holidays.abstractapi.com/v1?api_key=' + process.env.REACT_APP_HOLIDAY_API_KEY + '&country=US'
                        + '&year=' + year + '&month=' + month + '&day=' + day).then((response) => {
                if (response.data.length !== 0) {
                    const day_holidays = [];
                    for (let i = 0; i < response.data.length; i++) {
                        day_holidays.push(response.data[i]['name'])
                    }
                    setHolidays(day_holidays);
                }
                setWaitingForHoliday(false);
                // console.log(response);
            })
        }
    }, [waitingForHoliday]);

    // fetch data for this date
    useEffect(() => {
        if (!loading) {
            const air_values = {
                'aqi': { 'name': 'Air quality index', 'value': -1, 'color_class': '', 'src': aqi, 'units': '' },
                "co": { 'name': 'Carbon monoxide', 'value': -1, 'color_class': '', 'src': 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2e/Carbon-monoxide-3D-balls.png/800px-Carbon-monoxide-3D-balls.png', 'units': 'μg/m³' },
                "no": { 'name': 'Nitrogen monoxide', 'value': -1, 'color_class': '', 'src': 'https://i2.wp.com/perpetualminds.com/wp-content/uploads/2011/09/Nitric-Oxide.png', 'units': 'μg/m³' },
                "no2": {'name': 'Nitrogen dioxide', 'value': -1, 'color_class': '', 'src': 'https://img.pngio.com/filenitrogen-dioxide-3d-ballspng-wikimedia-commons-nitrogen-molecule-png-900_650.png', 'units': 'μg/m³' },
                "o3": { 'name': 'Ozone', 'value': -1, 'color_class': 'aqi-green', 'src': 'https://upload.wikimedia.org/wikipedia/commons/8/8c/Ozone-CRC-MW-3D-balls.png', 'units': 'μg/m³' },
                "so2": { 'name': 'Sulphur dioxide', 'value': -1, 'color_class': '', 'src': 'http://upload.wikimedia.org/wikipedia/commons/9/94/Sulfur-dioxide-3D-balls.png', 'units': 'μg/m³' },
                "pm2_5": { 'name': 'Fine particulate matter', 'value': -1, 'color_class': '', 'src': pm2_5, 'units': 'μg/m³' },
                "pm10": { 'name': 'Coarse particulate matter', 'value': -1, 'color_class': '', 'src': pm10, 'units': 'μg/m³' },
                "nh3": { 'name': 'Ammonia', 'value': -1, 'color_class': '', 'src': 'https://upload.wikimedia.org/wikipedia/commons/0/05/Ammonia-3D-balls-A.png', 'units': 'μg/m³' }
            }
            for (let type in air_values) {
                console.log(type);
                air_values[type]['value'] = data[type];
                if (type in compound_cutoffs) {
                    let y = 0;
                    let found = false;
                    while (y < compound_cutoffs[type].length && !found) {
                        if (+air_values[type]['value'] < +compound_cutoffs[type][y]) {
                            air_values[type]['color_class'] = color_classes[y]['class'];
                            found = true;
                        }
                        y++;
                    }

                    if (found === false) {
                        air_values[type]['color_class'] = color_classes[color_classes.length - 1]['class'];
                    }
                }
            }
            console.log(air_values);
            setAirValues(air_values);
            setDay(data);
            setWaitingForDay(false);
        }
    }, [data])

    if (error) {
        return <NotFound/>;
    }

    return waitingForHoliday || waitingForDay? (<Loading/>) : (

        <div class="jumbotron flex-fill">
            <h1 class="display-8 pageTitle">{day['date']}</h1>
            <div class='container-fluid' style={{ marginTop: '4%', marginBottom: '2%' }}>
                <div class="card-deck pt-3 mt-3 mx-3 justify-content-center row-cols-1">
                    {color_classes.map((color_class) => (
                        <div class={`card ${color_class['border']} m-0 pt-0 `} style={{ minWidth: '10rem' }}>
                            <div class={`card-body scale ${color_class['class']} py-1 px-3`}>
                                <small>{color_class['description']}</small>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
            <div class="card-deck mx-3 mb-3 row-cols-2 row-cols-sm-6 justify-content-center ">
                {Object.keys(airValues).map((type) => (

                    <Card className={`card dayDataContainer ${airValues[type]['color_class']}`} style={{ width: '10rem', minWidth: '10rem' }}>
                        <Card.Body>
                            <Card.Text>
                                <img class="card-img-top" src={airValues[type]['src']}
                                    alt={airValues[type]['name']} />
                                <b class="card-title">{airValues[type]['name']}</b>
                                <br />
                                {Number((airValues[type]['value']).toFixed(2))} {airValues[type]['units']}
                            </Card.Text>
                        </Card.Body>
                    </Card>
                ))}
            </div>
            <div class="container-flex justify-content-center">
                <div class="row justify-content-center">
                    <div class="col-md embed-responsive text-center p-3">
                        <h3>Cities with the worst AQI on {day["date"]}</h3>
                        <p>Drag to view all cities. Click a dot to view the city!</p>
                        {<PinMap
                            locations={day['worst_cities']}
                            map_style={{ height: '40vh', width: '68vh', marginLeft: '8vw', marginRight: '8vw' }}
                            baseURL='/cities/' />}
                    </div>
                    <div className="col-md embed-responsive text-center p-3">
                        <h3>Countries with the worst AQI on {day["date"]}</h3>
                        <p>Drag to view all countries. Click a dot to view the country!</p>
                        {<PinMap
                            locations={day['worst_countries']}
                            map_style={{ height: '40vh', width: '68vh', marginLeft: '8vw', marginRight: '8vw' }}
                            baseURL='/countries/' />}
                    </div>
                </div>
            </div>
            <br />
            <br />

            <div className="holidays">
                <h5>Holidays on {day["date"]}</h5>
                <p>{holidays.join(', ')}</p>
            </div>
        </div>
    );
};

export default DayPage;