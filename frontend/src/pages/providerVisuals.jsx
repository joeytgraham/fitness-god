import React from "react";

/* importing charts */
import PieChart from '../components/pieChart.jsx';
import BubbleChart from '../components/bubbleChart.jsx';
import BarChart from '../components/barChart.jsx';

/* importing data */
import cuisines from '../assets/data/visualization_data/provider_num_cuisines_per_subregion.json'
import ratings from '../assets/data/visualization_data/provider_num_restaurants_per_rating.json'
import leisure from '../assets/data/visualization_data/provider_avg_cities_leisure_by_state.json'


/* page to display our visualizations */
const ProviderVisuals = () => {
    return (
        <div className="jumbotron flex-fill flex-fill flex-column align-content-center text-center">
            <h1 className="display-4 pageTitle">Provider Visualizations</h1>
            
            <div style={{marginTop: '100px'}}>
                <h2>Number of Cuisines Per Subregion</h2>
                <PieChart 
                    data={cuisines}
                    innerRadius={0}
                    outerRadius={200}/>
            </div>
            
            <div style={{marginTop: '100px'}}>
                <h2>Restaurants Grouped by Rating</h2>
                <BubbleChart 
                    data={ratings}/>
            </div>

            <div style={{marginTop: '100px'}}>
                <h2>Average Leisure Score by State</h2>
                <BarChart
                    data={leisure}
                />
            </div>
        </div>
    );
};

export default ProviderVisuals;
