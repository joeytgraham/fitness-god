import React from "react";

/* importing charts */
import LineChartComp from '../components/lineChart.jsx';
import BubbleChart from '../components/bubbleChart.jsx';
import BarChart from '../components/barChart.jsx';

/* importing data */
import days_avg_aqi from '../assets/data/visualization_data/our_days_avg_aqi.json';
import city_recent_aqi from '../assets/data/visualization_data/our_cities_by_aqi_bracket.json';
import worst_countries from '../assets/data/visualization_data/our_top_50_worst_countries.json';


/* page to display our visualizations */
const OurVisuals = () => {
    return (
        <div className="jumbotron d-flex flex-fill flex-column align-content-center text-center">
            <h1 className="display-4 pageTitle">Our Visualizations</h1>
            
            <div style={{marginTop: '100px'}} className="flex-fill align-content-center">
                <h2>Global AQI Over Time</h2>
                < LineChartComp
                    data={days_avg_aqi}
                />
            </div>
            
            <div style={{marginTop: '100px'}}>
                <h2>Recent City AQIs Grouped by Range</h2>
                <BubbleChart
                    data={city_recent_aqi}
                />
            </div>

            <div style={{marginTop: '100px'}}>
                <h2>Top 50 Worst Countries by AQI</h2>
                <BarChart
                    data={worst_countries}
                />
            </div>
        </div>
    );
};

export default OurVisuals;