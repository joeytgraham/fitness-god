import React, {useEffect, useState} from "react";
import {Link, useParams} from "react-router-dom";
import '../styles/City.css'
import {Col, Container, Image, ResponsiveEmbed, Row, Spinner, Table} from "react-bootstrap";
import useAxios from "axios-hooks";
import Loading from "../components/loading";
import NotFound from "../components/notFound";


//Functional Component
const CityCard = () => {
    const {id} = useParams();

    const [waitingForCity, setWaitingForCity] = useState(true);
    const [city, setCity] = useState([]);

    const [{data, loading, error}] = useAxios(`${process.env.REACT_APP_API_URL}/cities/${id}`);
    document.title = city['name'];

    useEffect(() => {
        if (!loading) {
            if (data['country'] === 'US') {
                data['mapURL'] = `https://www.google.com/maps/embed/v1/place?key=${process.env.REACT_APP_GCP_API_KEY}&q=${data['name']},${data['region']},${data['country']}`;
            } else {
                data['mapURL'] = `https://www.google.com/maps/embed/v1/place?key=${process.env.REACT_APP_GCP_API_KEY}&q=${data['name']},${data['country']}`;
            }

            data['imageURL'] = `https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=${data['maps_place_photo_ref']}&key=${process.env.REACT_APP_GCP_API_KEY}`;

            setCity(data);
            setWaitingForCity(false);
        }
    }, [data])

    if (error) {
        return <NotFound/>
    }

    return waitingForCity ? (<Loading/>) : (
        <div class="jumbotron flex-fill remove-overflow">
            <h1 class="display-4 pageTitle">{city['name']}</h1>
            <Container className="city-media">
                <Row xs={1} md={2}>
                    <Col className='d-flex align-content-center justify-content-center'>
                    {city['maps_place_photo_ref'] === null ?
                                (<div class='justify-content-center'>
                                    <div class='row text-center justify-content-center'>
                                        <Spinner animation="border"/>
                                    </div>
                                    <div class='row text-center'>
                                        <p>Loading image...</p>
                                    </div>
                                </div>) :
                                (<Image className = "city-image" src={city['imageURL']} alt="City Image"/>)}
                    </Col>
                    <Col>
                        <div class='d-flex justify-content-center align-content-center'>
                            <ResponsiveEmbed aspectRatio="16by9">
                                <iframe title='map' class="responsive-embed-item" src={city['mapURL']}/>
                            </ResponsiveEmbed>
                        </div>
                    </Col>
                </Row>
            </Container>

            <br/>
            <br/>

            <Container>
                <Row xs={1} md={3} lg={5} className="city-info">
                    <Col className="info-col">
                        <h4 className="info-sect">Region</h4>
                        <h4>{city['region']}</h4>
                    </Col>

                    <Col className="info-col">
                        <h4 className="info-sect">Country</h4>
                        <h4 className="citysCountry"><Link
                            to={`/countries/${city['country']}`}>{city['country']}</Link></h4>
                    </Col>

                    <Col className="info-col">
                        <h4 className="info-sect">Latitude</h4>
                        <h4>{city['lat']}° N</h4>
                    </Col>

                    <Col className="info-col">
                        <h4 className="info-sect">Longitude</h4>
                        <h4>{city['lng']}° E</h4>
                    </Col>

                    <Col className="info-col">
                        <h4 className="info-sect">
                            Rank
                            <div className="infoTooltip">
                                <i className='fas fa-info-circle'></i>
                                <span className="tooltiptext">This rank compares this city to the other cities monitored by our website. The rank 1 represents the best air quality.</span>
                            </div>
                        </h4>
                        <h4>{city['rank']}</h4>
                    </Col>
                </Row>
            </Container>

            <br/>

            <div>
                <h3 style={{ textAlign: 'center', marginBottom: '2%'}}>Recent Air Quality Data</h3>
                <Table responsive hover>
                    <thead>
                    <tr>
                        <th>Date</th>
                        <th>Aqi
                            <div className="infoTooltip"><i className='fas fa-info-circle'></i>
                                <span className="tooltiptext">The Air Quality Index (AQI) is used for reporting daily air quality. It tells you how clean or polluted your air is, and what associated health effects might be a concern for you.</span>
                            </div>
                        </th>
                        <th>CO</th>
                        <th>NO</th>
                        <th>NO2</th>
                        <th>O3</th>
                        <th>SO2</th>
                        <th>PM2.5</th>
                        <th>PM10</th>
                        <th>NH3</th>
                    </tr>
                    </thead>
                    <tbody>
                    {city['past_aq_data'].map((day) => (
                        <tr className="clickableRow" onClick={() => {
                            window.location.assign(`/days/${day['date']}`)
                        }}>
                            <td>{day['date']}</td>
                            <td>{day['aqi']}</td>
                            <td>{day['co']}</td>
                            <td>{day['no']}</td>
                            <td>{day['no2']}</td>
                            <td>{day['o3']}</td>
                            <td>{day['so2']}</td>
                            <td>{day['pm2_5']}</td>
                            <td>{day['pm10']}</td>
                            <td>{day['nh3']}</td>
                        </tr>
                    ))}
                    </tbody>
                </Table>
            </div>
        </div>
    );
};

export default CityCard;