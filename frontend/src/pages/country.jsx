import React, {useEffect, useState} from "react";
import {Link, useParams} from "react-router-dom";
import '../styles/Country.css'
import {Col, Container, ResponsiveEmbed, Row} from "react-bootstrap";
import useAxios from "axios-hooks";
import Loading from "../components/loading"
import NotFound from "../components/notFound";

//Functional Component 
const CountryPage = () => {


    const {id} = useParams();
    document.title = id;

    var [waitingForCountry, setWaitingForCountry] = useState(true);
    var [country, setCountry] = useState([]);


    const [{data, loading, error}] = useAxios(`${process.env.REACT_APP_API_URL}/countries/${id}`);

    useEffect(() => {
        if (!loading) {
            data['mapURL'] = `https://www.google.com/maps/embed/v1/place?key=${process.env.REACT_APP_GCP_API_KEY}&q=${data['name']}`;
            setCountry(data);
            setWaitingForCountry(false);
        }
    }, [data])

    if (error) {
        return <NotFound/>;
    }

    return waitingForCountry ? (<Loading/>) : (
        <div className="jumbotron flex-fill">
            <h1 className="display-4 pageTitle">{country['name']}</h1>
            <br/>
            <Container>
                <Row xs={1} md={2}>
                    <Col>
                        <img className="flag" src={country['flag_url']} alt="flag"></img>
                    </Col>
                    <Col style={{ textAlign: 'center' }}>
                        <Col className="info-col">
                            <h5>Latest Average Air Quality</h5>
                            <h3>{Number((country['latest_aqi']).toFixed(5))}</h3>
                        </Col>
                        <Col className="info-col">
                            <h5>Worst Air Quality Day</h5>
                            <Link to={`/days/${country['worst_day']}`}>
                                <h3 className="countryDate">{country['worst_day']}</h3>
                            </Link>
                        </Col>
                    </Col>
                </Row>
            </Container>

            <br/>
            <br/>

            {/* <hr className="my-4"/> */}
            <Container>
            <Row xs={1} md={3} lg={5} className="city-info">
                    <Col className="info-col">
                        <h5 className="info-sect">Alpha 2 Code</h5>
                        <h5>{country['id']}</h5>
                    </Col>

                    <Col className="info-col">
                        <h5 className="info-sect">Capital</h5>
                        <h5>{country['capital']}</h5>
                    </Col>

                    <Col className="info-col">
                    <h5 className="info-sect">Region</h5>
                    <h5>{country['region']}</h5>
                    </Col>

                    <Col className="info-col">
                    <h5 className="info-sect">Population</h5>
                    <h5>{country['population'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</h5>
                    </Col>
                </Row>
                <Row xs={1} md={3} lg={5} className="city-info">
                    <Col className="info-col">
                    <h5 className="info-sect">Latitude</h5>
                    <h5>{country['lat']}° N</h5>
                    </Col>

                    <Col className="info-col">
                    <h5 className="info-sect">Longitude</h5>
                    <h5>{country['lng']}° E</h5>
                    </Col>

                    <Col className="info-col">
                    <h5 className="info-sect">Economic Status</h5>
                    <h5>{country['economic_status']}</h5>
                    </Col>

                    <Col className="info-col">
                    <h5 className="info-sect">Cities</h5>
                    <h5 className="countrysCity">
                        {country['cities'].map((city) => (
                            <Link to={`/cities/${city['id']}`}>{city['name']} </Link>
                        ))}
                    </h5>
                    </Col>
                </Row>
                <br/>
                <br/>
                <Row>
                    <ResponsiveEmbed aspectRatio="16by9">
                        <iframe title='map' class="responsive-embed-item" src={country['mapURL']}/>
                    </ResponsiveEmbed>
                </Row>
            </Container>

            
            
        </div>
    );
};

export default CountryPage;