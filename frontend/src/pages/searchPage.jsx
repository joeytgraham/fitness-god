import React from "react";
import { useState } from "react";
import { Image } from 'react-bootstrap'
import Dropdown from "react-bootstrap/Dropdown";
import DropdownButton from "react-bootstrap/DropdownButton";
import algoliasearch from "algoliasearch";
import { SearchBox, connectHits, connectStateResults, Index, InstantSearch } from "react-instantsearch-dom";
import PaginationComponent from "../components/pagination"

/* cards for displaying search results for each model */
import SearchCityCard from "../components/searchCityCard";
import SearchCountryCard from "../components/searchCountryCard";
import SearchDayCard from "../components/searchDaysCard";

/* image to display for algolia */
import algoliaLogo from '../assets/images/algolialogo2.webp';


/* page to display search results */
const SearchPage = ({ id, filter }) => {

  /* to be able to paginate search results for each model */
  const [currentCountryPage, setCountryCurrentPage] = useState(1);
  const [currentCityPage, setCityCurrentPage] = useState(1);
  const [currentDayPage, setDayCurrentPage] = useState(1);
  const [currentCountries, setCurrentCountries] = useState([]);
  const [currentCities, setCurrentCities] = useState([]);
  const [currentDays, setCurrentDays] = useState([]);


  /* gather search result hits for countries and display as cards */
  const countryHits = ({ hits }) => (
    <div>
      <div className="row">
        {currentCountries.length === 0 ? setCurrentCountries(hits.slice(0, 6)) : null}
        {currentCountries.map((hit) => (
          <div key={hit.city_id} style={{ padding: '10px' }}>
            <SearchCountryCard hit={hit} />
          </div>
        ))}
      </div>
    </div>

  );

  const CountryHits = connectHits(countryHits);

  const CountryResults = ({ searchResults }) => {
    const hasResults = searchResults && searchResults.nbHits !== 0;
    
    return (
      /* display results if there were any hits */
      hasResults ? (
        <div>
          <CountryHits />
          <PaginationComponent
            paginate={page => {
              setCountryCurrentPage(page)
              setCurrentCountries(searchResults.hits.slice((page - 1) * 6, page * 6))
            }}
            currentPage={currentCountryPage}
            pageSize={6}
            totalInstances={searchResults.nbHits}
          />
        </div>
        /* display "No Results" if there were not any results for the query */
      ) : (<div><h2>No Results</h2></div>)
    );

  };

  /* displaying country search results with pagination */
  const CountryContent = connectStateResults(CountryResults);


  /* gather search result hits for cities and display as cards */
  const cityHits = ({ hits }) => (
    <div>
      <div className="row">
        {currentCities.length === 0 ? setCurrentCities(hits.slice(0, 6)) : null}
        {currentCities.map((hit) => (
          <div key={hit.country_id} style={{ padding: '10px' }}>
            <SearchCityCard hit={hit} />
          </div>
        ))}
      </div>
    </div>
  );

  const CityHits = connectHits(cityHits);

  const CityResults = ({ searchResults }) => {
    const hasResults = searchResults && searchResults.nbHits !== 0;
    const myHits = searchResults && searchResults.hits ? searchResults.hits : null;
    return (
      /* display results if there were any hits */
      hasResults ? (
        <div>
          <CityHits />
          <PaginationComponent
            paginate={page => {
              setCityCurrentPage(page)
              setCurrentCities(myHits.slice((page - 1) * 6, page * 6))
            }}
            currentPage={currentCityPage}
            pageSize={6}
            totalInstances={searchResults.nbHits}
          />
        </div>
        /* display "No Results" if there were not any results for the query */
      ) : (<div><h2>No Results</h2></div>)
    );
  };

  /* displaying city search results with pagination */
  const CityContent = connectStateResults(CityResults);


  /* gather search result hits for days and display as cards */
  const daysHits = ({ hits }) => (
    <div>
      <div className="row">
        {currentDays.length === 0 ? setCurrentDays(hits.slice(0, 6)) : null}
        {currentDays.map((hit) => (
          <div key={hit.year_id} style={{ padding: '10px' }}>
            <SearchDayCard hit={hit} />
          </div>
        ))}
      </div>
    </div>
  );

  const DayHits = connectHits(daysHits);

  const DaysResults = ({ searchResults }) => {
    const hasResults = searchResults && searchResults.nbHits !== 0;
    const myHits = searchResults && searchResults.hits ? searchResults.hits : null;
    return (
      /* display results if there were any hits */
      hasResults ? (
        <div>
          <DayHits />
          <PaginationComponent
            paginate={page => {
              setDayCurrentPage(page)
              setCurrentDays(myHits.slice((page - 1) * 6, page * 6))
            }}
            currentPage={currentDayPage}
            pageSize={6}
            totalInstances={searchResults.nbHits}
          />
        </div>
        /* display "No Results" if there were not any results for the query */
      ) : (<div><h2>No Results</h2></div>)
    );
  };

  /* displaying day search results with pagination */
  const DayContent = connectStateResults(DaysResults);

  /* connecting to algolia database */
  const searchClient = algoliasearch(
    `${process.env.REACT_APP_ALGOLIA_ID}`,
    `${process.env.REACT_APP_ALGOLIA_ADMIN_API_KEY}`
  );

  /* show search results across all models if no filter is declared */
  if (filter === undefined) {
    filter = "ALL";
  }

  /* used for filtering search results by model */
  const [filterType, setFilterType] = React.useState(filter);
  const [filterTitle, setFilterTitle] = React.useState("Filter by Model ");

  /* changing the filter to just city results */
  function filterCities() {
    setFilterType("CITIES");
    setFilterTitle("Cities ");
  }

  /* changing the filter to just country results */
  function filterCountries() {
    setFilterType("COUNTRIES");
    setFilterTitle("Countries ");
  }

  /* changing the filter to just day results */
  function filterDays() {
    setFilterType("DAYS");
    setFilterTitle("Days ");
  }

  /* changing the filter to all results */
  function filterAll() {
    setFilterType("ALL");
    setFilterTitle("Filter by Model ");
  }


  return (
    <div className="jumbotron flex-fill">
      <div className="Search">
        <h1>SEARCH RESULTS</h1>
        {/* display the query */}
        <h2>{id}</h2>
        <br />

        {/* dropdown for filter. displays only when user searches in the navbar */}
        {filter === "ALL" ? (<DropdownButton id="dropdown-basic-button" title={filterTitle}>
          <Dropdown.Item onClick={filterCountries}>Countries</Dropdown.Item>
          <Dropdown.Item onClick={filterCities}>Cities</Dropdown.Item>
          <Dropdown.Item onClick={filterDays}>Days</Dropdown.Item>
          <Dropdown.Item onClick={filterAll}>All</Dropdown.Item>
        </DropdownButton>) : (<div></div>)}
        <br />

        <InstantSearch
          indexName="countries_ALL"
          searchClient={searchClient}
          searchState={{ query: id }}>

          <div style={{ display: "none" }}><SearchBox /></div>

          {/* display country searcg results */}
          <Index indexName="countries_ALL">
            {filterType === "COUNTRIES" ||
              filterType === "ALL" ? (
              <div style={{ paddingBottom: '40px' }}>
                <h1>Countries</h1>
                <main><CountryContent /></main>
              </div>) : (<div></div>)}
          </Index>
          
          {/* display city search results */}
          <Index indexName="cities_ALL">
            {filterType === "CITIES" ||
              filterType === "ALL" ? (
              <div style={{ paddingBottom: '40px' }}>
                <h1>Cities</h1>
                <main><CityContent /></main>
              </div>) : (<div></div>)}
          </Index>
          
          {/* display day search results */}
          <Index indexName="days_ALL">
            {filterType === "DAYS" || filterType === "ALL" ? (
              <div style={{ paddingBottom: '40px' }}>
                <h1>Days</h1>
                <main><DayContent /></main>
              </div>) : (<div></div>)}
          </Index>
        </InstantSearch>
        
        {/* have to display algolia logo when using the free version */}
        <div>
          <Image
            src={algoliaLogo}
            width="200px" />
        </div>

        <br />
      </div>
    </div>
  );
}

export default SearchPage;
