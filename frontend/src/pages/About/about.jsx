import React, { useEffect, useState } from 'react';
import { Card, CardDeck, Row, Col, Container } from 'react-bootstrap';
import MemberCard from './memberCard';
import ResourceCard from './resourceCard';
import ToolCard from './toolCard';
import { members } from '../../assets/members';
import { resources } from '../../assets/resources';
import { tools } from '../../assets/tools';
import '../../styles/About.css';

/* importing gitlab and postman images */
import gitlabPic from '../../assets/images/gitlab.png'
import postmanPic from '../../assets/images/postman.png'

/* information about team members, resources, tool, and statistics */
const AboutPage = () => {
    document.title = "About";

    /* to keep track of individual commits/issues and total commits/issues */
    const [memberCommits, setMemberCommits] = useState([0, 0, 0, 0, 0]);
    const [memberIssues, setMemberIssues] = useState([0, 0, 0, 0, 0]);
    const [commitTotals, setCommitTotals] = useState(0);
    const [issueTotals, setIssueTotals] = useState(0);
    const memberTests = [34, 34, 6, 21, 7];
    const testTotals = memberTests[0] + memberTests[1] + memberTests[2] + memberTests[3] + memberTests[4];

    const ISSUES_API = "https://gitlab.com/api/v4/projects/24707067/issues";
    const COMMITS_API = "https://gitlab.com/api/v4/projects/24707067/repository/commits";

    /* get commits from gitlab */
    useEffect(() => {
        const getCommits = async () => {
            let stats = [0, 0, 0, 0, 0, 0];
            /* to handle paginated response from gitlab */
            let xNextPage = "1";
            while (xNextPage) {
                const response = await fetch(COMMITS_API + '?per_page=100&page=' + xNextPage);
                const OUR_COMMITS = await response.json();
                OUR_COMMITS.forEach((commit) => {
                    let name = commit['author_name'];
                    /* update individual commit numbers */
                    if (name === "Claire Mathieu") {
                        stats[0] += 1;
                    } else if (name === "Joseph Graham") {
                        stats[1] += 1;
                    } else if (name === "Emaan Haseem" || name === "Emaan  Haseem" || name === "Emaan") {
                        stats[2] += 1;
                    } else if (name === "Shan Rizvi") {
                        stats[3] += 1;
                    } else if (name === "Kiana") {
                        stats[4] += 1;
                    }
                    stats[5] += 1;
                })
                xNextPage = response.headers.get('x-next-page')
            }
            setMemberCommits(stats);
            setCommitTotals(stats[5]);
        };
        getCommits();
    });

    /* get issues from gitlab */
    useEffect(() => {
        const getIssues = async () => {
            let issues = [0, 0, 0, 0, 0, 0];
            /* to handle paginated response from gitlab */
            let xNextPage = "1";
            while (xNextPage) {
                const response = await fetch(ISSUES_API + '?per_page=100&page=' + xNextPage);
                const OUR_ISSUES = await response.json();
                OUR_ISSUES.forEach((issue) => {
                    let assignees = issue.assignees;
                    assignees.forEach((assignee) => {
                        let name = assignee.name;
                        /* update individual issues numbers */
                        if (name === "Claire Mathieu") {
                            issues[0] += 1;
                        } else if (name === "Joseph Graham") {
                            issues[1] += 1;
                        } else if (name === "Emaan Haseem" || name === "Emaan  Haseem" || name === "Emaan") {
                            issues[2] += 1;
                        } else if (name === "Shan Rizvi") {
                            issues[3] += 1;
                        } else if (name === "Kiana Fithian") {
                            issues[4] += 1;
                        }
                        issues[5] += 1;
                    })
                })
                xNextPage = response.headers.get('x-next-page')
            }
            setMemberIssues(issues);
            setIssueTotals(issues[5]);
        };
        getIssues();
    });


    return (
        <div>
            <div className="jumbotron">
                <h1 className="display-4">About Us</h1>

                {/* the purpose of our site */}
                <h2 className='subtitles'>What is BreatheEasy?</h2>
                <div style={{ paddingBottom: '100px' }}>
                    <p>
                        BreatheEasy provides air pollution information on major cities around the globe.
                        We want people to have ready access to air pollution information in their cities to
                        be able to make decisions about their health. We also want to provide an easy way to
                        view changes in air quality over time so that as a planet we can make informed decisions
                        about legislation and the future. Our goal is to ultimately bring awareness to how
                        urbanization is affecting our air quality and encourage people to avoid activities
                        that pollute the air. This site is for every day people who are curious about air quality
                        around the world and how it is impacting them.
                    </p>
                    <p>
                        Our data allows you to learn more about the air quality in major cities and countries and
                        provides information on air quality over time.
                    </p>
                </div>


                <h2 className='subtitles2'>Meet the Team</h2>
                {/* our personal information */}
                <Container xl={3}>
                    <CardDeck className="member-deck">
                        {members.map((member) => (
                            <MemberCard
                                name={member.name}
                                job={member.job}
                                bio={member.bio}
                                commits={memberCommits[member.id]}
                                issues={memberIssues[member.id]}
                                tests={memberTests[member.id]}
                                pic={member.pic}
                                linkedin={member.linkedin}>
                            </MemberCard>
                        ))}
                    </CardDeck>
                </Container>

                {/* total commits, issues, and unit tests */}
                <div style={{ paddingBottom: '100px', paddingTop: '50px' }}>
                    <Row xs={1} sm={3} className="stats-row">
                        <Col className="stats-col">
                            <h4>Total Commits</h4>
                            <h4>{commitTotals}</h4>
                        </Col>

                        <Col className="stats-col">
                            <h4>Total Issues</h4>
                            <h4>{issueTotals}</h4>
                        </Col>
                        <Col className="stats-col">
                            <h4>Total Tests</h4>
                            <h4>{testTotals}</h4>
                        </Col>
                    </Row>
                </div>

                {/* api and data sources information */}
                <div style={{ paddingBottom: '100px' }}>
                    <h2 className='subtitles2'>Our APIs and Data Sources</h2>
                    <br />
                    <CardDeck className="data-deck">
                        {resources.map((resource) => (
                            <ResourceCard
                                name={resource.name}
                                about={resource.about}
                                link={resource.link}
                                pic={resource.pic}>
                            </ResourceCard>
                        ))}
                    </CardDeck>
                </div>

                {/* information about the tools we used */}
                <div style={{ paddingBottom: '100px' }}>
                    <h2 className='subtitles2'>Our Tools</h2>
                    <br />
                    <CardDeck className="tool-deck">
                        {tools.map((tool) => (
                            <ToolCard
                                name={tool.name}
                                use={tool.use}
                                link={tool.link}
                                pic={tool.pic}>
                            </ToolCard>
                        ))}
                    </CardDeck>
                </div>

                {/* our gitlab and postman references */}
                <div className="git-post" style={{ paddingBottom: '100px' }}>
                    <CardDeck className="our-work-deck">
                        <Card className="our-work">
                            <a href="https://gitlab.com/joeytgraham/fitness-god" target='_blank' rel="noreferrer">
                                <Card.Img className="our-work-img" src={gitlabPic} alt='gitlab'></Card.Img>
                            </a>
                            <br />
                            <Card.Text className="our-work-text">
                                Our GitLab Repository
                            </Card.Text>

                        </Card>
                        <Card className="our-work">
                            <a href="https://documenter.getpostman.com/view/14788906/Tz5iC25X" target='_blank' rel="noreferrer">
                                <Card.Img className="our-work-img" src={postmanPic} alt='postman'></Card.Img>
                            </a>
                            <br />
                            <Card.Text className="our-work-text">
                                Our Postman API
                            </Card.Text>
                        </Card>
                    </CardDeck>
                </div>

                {/* video presentation for website */}
                <div className='flex-fill flex-column align-content-center text-center'>
                    <h2 className='subtitles2'>Website Presentation</h2>
                    <br />
                    <iframe
                        width="560"
                        height="315"
                        src="https://www.youtube.com/embed/2gEPISYYN4E"
                        title="YouTube video player"
                        frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen>
                    </iframe>
                </div>
            </div>
        </div>
    );
};

export default AboutPage;