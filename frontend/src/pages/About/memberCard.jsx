import React from 'react';
import { Card, Image, Container, Row, Col } from "react-bootstrap";
import '../../styles/About.css';

/* icons for individual member stats */
import commitIcon from '../../assets/images/commits-icon.png';
import issueIcon from '../../assets/images/issues-icon.png';
import testIcon from '../../assets/images/tests-icon.png';

/* card to display team member info */
const MemberCard = ({ name, job, bio, commits, issues, tests, pic, linkedin }) => {

    return (
        <Card className='member-card'>
            <a href={linkedin} target='_blank' rel="noreferrer">
                <Card.Img className="member-img" variant="top" src={pic} />
            </a>
            <Card.Body>
                <Card.Title className='mem-card-text'>{name}</Card.Title>
                <Card.Subtitle className='mb-2'>{job}</Card.Subtitle>
                <Card.Text>{bio}</Card.Text>
            </Card.Body>
            {/* display member commits, issues, and tests */}
            <Container className='mem-stats'>
                <Row>
                    <Col>
                        <p>Commits:</p>
                        <Image className='stat-icon' src={commitIcon}></Image>
                        <p className='mem-stats-text'>{commits}</p>
                    </Col>
                    <Col>
                        <p>Issues:</p>
                        <Image className='stat-icon' src={issueIcon}></Image>
                        <p className='mem-stats-text'>{issues}</p>
                    </Col>
                    <Col>
                        <p>Tests:</p>
                        <Image className='stat-icon' src={testIcon}></Image>
                        <p className='mem-stats-text'>{tests}</p>
                    </Col>
                </Row>
            </Container>
        </Card>
    );
};

export default MemberCard;