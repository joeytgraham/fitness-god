import React from 'react';
import { Card } from "react-bootstrap";
import '../../styles/About.css';

/* card to display tool information */
const ToolCard = ({ name, use, link, pic }) => {

    return (
        <Card className='tool-card'>
            <a href={link} target='_blank' rel="noreferrer">
                <Card.Img className="tool-img" variant='top' src={pic}></Card.Img>
            </a>
            <Card.Text style={{ paddingTop: '10px' }}>
                <b>{name}</b>
                <p>{use}</p>
            </Card.Text>
        </Card>
    );
};

export default ToolCard;