import React from 'react';
import { Card } from "react-bootstrap";
import '../../styles/About.css';

/* card to display resource information */
const ResourceCard = ({ name, about, link, pic }) => {
    return (
        <Card className='data-card'>
            <a href={link} target='_blank' rel="noreferrer">
                <Card.Img className='data-img' variant='top' src={pic}></Card.Img>
            </a>
            <Card.Text style={{ paddingTop: '10px' }}>
                <b>{name}</b>
                <p>{about}</p>
            </Card.Text>
        </Card>

    );
};

export default ResourceCard;