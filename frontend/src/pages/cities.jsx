import React, { useEffect, useState } from "react";
import PaginationComponent from "../components/pagination"
import axios from "axios";
import CityCard from "../components/cityCard"
import Loading from "../components/loading";
import MultiSelect from "react-multi-select-component";
import '../styles/Multiselect.css'
import { Form, FormControl, Button } from "react-bootstrap";


//Functional Component
const CitiesPage = ({ paginationSize }) => {
    const [loading, setLoading] = useState(true);
    const [params, setParams] = useState({'limit': paginationSize});
    const [data, setData] = useState([]);
    const [currentCities, setCurrentCities] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [selectedFilterLabels, setSelectedFilterLabels] = useState([]);
    const [totalInstances, setTotalInstances] = useState(0);
    const [selectedSortLabels, setSelectedSortLabels] = useState([]); //previously default selected
    const [filterOptions, setFilterOptions] = useState([]);
    const [sortOptions, setSortOptions] = useState([]); //new selections
    const [disabledCountry, setDisabledCountry] = useState([false, false]);
    const [disabledRegions, setDisabledRegions] = useState([false, false]);
    const [disabledLat, setDisabledLat] = useState([false, false]);
    const [disabledLng, setDisabledLng] = useState([false, false]);
    const [disabledName, setDisabledName] = useState([false, false]);
    const pageSize = paginationSize;
    const userInput = React.useRef();

    const optionsFilterArray = [
        { value: "name / gte(name,A) / lte(name,I)", label: "City A-I" },
        { value: "name / gte(name,J) / lte(name,R)", label: "City J-R" },
        { value: "name / gte(name,S) / lte(name,Z)", label: "City S-Z" },
        { value: "region / gte(region,A) / lte(region,I)", label: "Region A-I" },
        { value: "region / gte(region,J) / lte(region,R)", label: "Region J-R" },
        { value: "region / gte(region,S) / lte(region,Z)", label: "Region S-Z" },
        { value: "country / gte(country,A) / lte(country,I)", label: "Country A-I" },
        { value: "country / gte(country,J) / lte(country,R)", label: "Country J-R" },
        { value: "country / gte(country,S) / lte(country,Z)", label: "Country S-Z" },
        { value: "lat / gte(lat, 0)", label: "Above the Equator" },
        { value: "lat / lte(lat, 0)", label: "Below the Equator" },
        { value: "lng / gte(lng, 0)", label: "East of the Prime Meredian" },
        { value: "lng / lte(lng, 0)", label: "West of the Prime Meredian" },
    ];

    const optionsSortArray = [
        { value: "asc(name)", label: "Name A - Z", disabled: disabledName["asc(name)"] },
        { value: "desc(name)", label: "Name Z - A", disabled: disabledName["desc(name)"] },
        { value: "asc(country)", label: "Country A - Z", disabled: disabledCountry["asc(country)"] },
        { value: "desc(country)", label: "Country Z - A", disabled: disabledCountry["desc(country)"] },
        { value: "asc(region)", label: "Region A - Z", disabled: disabledRegions["asc(region)"] },
        { value: "desc(region)", label: "Region Z - A", disabled: disabledRegions["desc(region)"] },
        { value: "desc(lat)", label: "Latitude high - low", disabled: disabledLat["desc(lat)"] },
        { value: "asc(lat)", label: "Latitude low - high", disabled: disabledLat["asc(lat)"] },
        { value: "desc(lng)", label: "Longitude high - low", disabled: disabledLng["desc(lng)"] },
        { value: "asc(lng)", label: "Longitude low - high", disabled: disabledLng["asc(lng)"] },
    ];
    const filterFields = [
        "id", "name", "region", "country", "population", "lat", "lng"
    ];


    function search() {
        window.location.assign("/search/q=" + userInput.current.value + "&filter=CITIES");
    }

    useEffect(() => {
        const getData = async () => {
            setLoading(true);
            const res = await axios.get(`${process.env.REACT_APP_API_URL}/cities`, {
                params: params
            })
            setData(res.data);
            setLoading(false);
        };

        getData();
    }, [params, currentPage]);


    useEffect(() => {
        if (!loading) {
            setCurrentCities(data['results'])
            setTotalInstances(data['total_count'])
        }
    }, [loading]);

    // set filter parameters
    useEffect(() => {
        const changeParams = () => {
            var filterParams = {}
            var filterKeys = []
            var filterValues = []
            var filterCall = ""
            var tempFilterSelectedSort = []
            var tempFilterOptions = optionsFilterArray.filter(o => filterOptions.includes(o["value"]))
            tempFilterOptions.map((o) => (
                tempFilterSelectedSort.push(o)
            ))
            setSelectedFilterLabels(tempFilterOptions)
            for (var i = 0; i < filterOptions.length; i++) {
                var filterArr = filterOptions[i].split(" / ")
                filterKeys[i] = filterArr[0]
                filterValues[i] = []
                filterValues[i][0] = filterArr[1]
                if (filterArr[2]) {
                    filterValues[i][1] = (filterArr[2])
                }
            }
            filterFields.map((s) => (
                filterParams[s] = []
            ))
            var index = 0;
            filterValues.map((s) => (
                filterParams[filterKeys[index++]].push(s)
            ))
            var expressions = []
            var exp1 = ""
            for (var key in filterParams) {
                if (filterParams[key].length > 0) {
                    exp1 = ""
                    if (filterParams[key].length === 1) {
                        if (filterParams[key][0].length === 1) {
                            exp1 = filterParams[key];
                        } else {
                            exp1 = "and(" + filterParams[key][0][0] + "," + filterParams[key][0][1] + ")"
                        }
                    } else {
                        for (i = 0; i < filterParams[key].length; i++) {
                            var exp2 = ""
                            if (filterParams[key][i].length === 1) {
                                exp2 = filterParams[key];
                            } else {
                                exp2 = "and(" + filterParams[key][i][0] + "," + filterParams[key][i][1] + ")"
                            }
                            exp1 += exp2
                            if (i !== filterParams[key].length - 1) {
                                exp1 += ","
                            }

                        }
                        exp1 = "or(" + exp1 + ")"

                    }
                    expressions.push(exp1)
                }
            }
            if (expressions.length > 1) {
                for (i = 0; i < expressions.length; i++) {
                    filterCall += expressions[i] + ""
                    if (i !== expressions.length - 1) {
                        filterCall += ","
                    }
                }
                filterCall = "and(" + filterCall + ")"
            } else {
                filterCall = exp1 + ""
            }
            var tempParams = new Map()
            for (i in params) {
                tempParams[i] = params[i];
            }
            if (filterCall.length > 0) {
                tempParams['filter'] = filterCall
            }
            setParams(tempParams)
        }
        changeParams();
    }, [filterOptions])

    // set pagination parameters
    useEffect(() => {
        const changeParams = () => {
            var tempParams = {}
            for (var i in params) {
                tempParams[i] = params[i];
            }
            tempParams['limit'] = pageSize
            tempParams['offset'] = (currentPage - 1) * pageSize
            setParams(tempParams)
        }
        changeParams();
    }, [currentPage])

    // set sort parameters
    useEffect(() => {
        const changeParams = () => {
            var tempParams = {}
            const sortValues = []
            var tempSelectedSort = []
            sortOptions.map((s) => (
                sortValues.push(s + "")
            ))
            var tempOptions = optionsSortArray.filter(o => sortOptions.includes(o["value"]))
            tempOptions.map((o) => (
                tempSelectedSort.push(o)
            ))
            for (var i in params) {
                tempParams[i] = params[i];
            }
            tempParams['sort_by'] = sortValues
            setSelectedSortLabels(tempSelectedSort)
            setParams(tempParams)
        }
        changeParams();
    }, [sortOptions])

    // adjustment for new multisort component
    const parseOptions = (rawOptions) => {
        var temp = []
        const rawNonNull = rawOptions.filter(r => r !== null)
        rawNonNull.map((r) => (
            temp.push(r["value"])
        ))
        return temp
    }

    //disabled option if its opposite is chosen 
    useEffect(() => {
        console.log("selected labels: " + JSON.stringify(selectedSortLabels))
        var tempSelectedVals = []
        selectedSortLabels.map((s) => (
            tempSelectedVals.push(s["value"])
        ))

        setDisabledCountry(sortDropdownXORLogic(tempSelectedVals, "asc(country)", "desc(country)"))
        setDisabledRegions(sortDropdownXORLogic(tempSelectedVals, "asc(region)", "desc(region)"))
        setDisabledName(sortDropdownXORLogic(tempSelectedVals, "asc(name)", "desc(name)"))
        setDisabledLat(sortDropdownXORLogic(tempSelectedVals, "asc(lat)", "desc(lat)"))
        setDisabledLng(sortDropdownXORLogic(tempSelectedVals, "asc(lng)", "desc(lng)"))
    }, [selectedSortLabels])

    //helper that XORs
    const sortDropdownXORLogic = (vals, asc, desc) => {
        var temp = []
        if (vals.includes(asc)) {
            temp[desc] = true
            temp[asc] = false
        } else if (vals.includes(desc)) {
            temp[asc] = true
            temp[desc] = false
        } else {
            temp[asc] = false
            temp[desc] = false
        }
        console.log("temp: " + asc + " " + temp[asc] + " " + temp[desc])
        return temp
    }


    return (
        // return ( 
        <div className="jumbotron flex-fill">
            <h1 className="display-4 pageTitle">Cities</h1>
            <hr className="my-4" />
            <div class='row no-gutters'>
                <div class='col-md-5 w-auto ml-auto'>
                    <Form
                        inline
                        onSubmit={(sub) => {
                            sub.preventDefault();
                        }}>
                        <FormControl
                            className = "model-search"
                            placeholder="Search"
                            type="text"
                            ref={userInput}
                            onKeyPress={(event) => {
                                if (event.key === "Enter") {
                                    search();
                                }
                            }}>
                        </FormControl>
                        <Button
                            className = "search-submit"
                            variant="light"
                            onClick={() => search()}>Submit</Button>
                    </Form>
                </div>
                <div class='col-md-5 w-auto ml-auto'>
                    {/* <DropdownMultiselect options={optionsFilterArray} name="citiesFilter" handleOnChange={setFilterOptions} selected={selectedFilterLabels} /> */}
                    <MultiSelect
                        options={optionsFilterArray}
                        value={selectedFilterLabels}
                        onChange={(rawOptions) => setFilterOptions(parseOptions(rawOptions))}
                        // labelledBy={"Filter"}
                        hasSelectAll={false}
                        className={"multi-select float-right custom"}
                        overrideStrings={{ "selectSomeItems": "Filter" }}
                    />
                    <MultiSelect
                        options={optionsSortArray}
                        value={selectedSortLabels}
                        onChange={(rawOptions) => setSortOptions(parseOptions(rawOptions))}
                        labelledBy={"Sort By"}
                        hasSelectAll={false}
                        className={"multi-select float-right custom model-sort"}
                        overrideStrings={{ "selectSomeItems": "Sort by..." }}
                    />
                </div>
            </div>
            {/* <h2> TEST Filtering by {filter}</h2> */}
            <br/>
            {loading ? (<Loading />) : 
                <div class='row'>
                    {currentCities.map((city) => (
                        <CityCard city={city} />
                    ))}
                </div>
            }
            <br/>
            <PaginationComponent
                paginate={page => {
                    window.scrollTo(0, 0);
                    setCurrentPage(page)
                }}
                currentPage={currentPage}
                pageSize={pageSize}
                totalInstances={totalInstances}
            />
        </div>
    );

}

export default CitiesPage;
