import React from 'react';
import NavBar from "./components/navbar";
import Footer from "./components/footer";
import './styles/App.css';
import {
    BrowserRouter as Router,
    Route,
    Switch
} from "react-router-dom";
import LandingPage from "./pages";
import AboutPage from "./pages/About/about";
import CountriesPage from "./pages/countries";
import CountryPage from "./pages/country";
import CitiesPage from "./pages/cities";
import CityPage from "./pages/city";
import DaysPage from "./pages/days";
import Days1Page from "./pages/day";
import SearchPage from "./pages/searchPage";
import NotFound from "./components/notFound";
import OurVisuals from './pages/ourVisuals';
import ProviderVisuals from './pages/providerVisuals';


export default function App() {
    return (
        <div className="background min-vh-100 d-flex flex-column">
            <div>
                <NavBar />
            </div>
            <div className='flex-fill d-flex h-auto background-jumbotron'>
                <Router>
                    <Switch>
                        <Route exact path="/">
                            <LandingPage />
                        </Route>
                        <Route path="/about">
                            <AboutPage />
                        </Route>
                        <Route exact path="/countries">
                            <CountriesPage  paginationSize={21}/>
                        </Route>
                        <Route path="/countries/:id">
                            <CountryPage/>
                        </Route>
                        <Route exact path="/cities">
                            <CitiesPage paginationSize={21}/>
                        </Route>
                        <Route path="/cities/:id">
                            <CityPage />
                        </Route>
                        <Route exact path="/days">
                            <DaysPage  paginationSize={21}/>
                        </Route>
                        <Route exact path="/days/:id">
                            <Days1Page />
                        </Route>
                        <Route exact path='/our-visualizations'>
                            <OurVisuals/>
                        </Route>
                        <Route exact path='/provider-visualizations'>
                            <ProviderVisuals/>
                        </Route>
                        <Route exact path="/search/q=:id&filter=:filter"
                            render={(props) => <SearchPage id={props.match.params.id} filter={props.match.params.filter}/>}>
                        </Route>
                        <Route path="/search/q=&filter:filter"
                            render={(props) => <SearchPage id="" filter={props.match.params.filter}/>}></Route>
                        <Route component={NotFound}/>
                    </Switch>
                </Router>
            </div>
            <div>
                <Footer/>
            </div>
        </div>
    );
}