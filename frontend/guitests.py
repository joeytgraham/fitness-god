import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import (
    TimeoutException,
    ElementNotVisibleException,
    ElementNotSelectableException,
)
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options

navbar = "//*[@id='root']/div[1]/div/nav"
timeout = 10
cities_timeout = 120


class SeleniumTests(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--disable-dev-shm-usage")
        self.driver = webdriver.Chrome("./chromedriver", options=chrome_options)
        self.driver.get("https://breathe-easy.me/")
        # self.driver.get("http://localhost:3000/")

    @classmethod
    def tearDownClass(self):
        self.driver.quit()

    def test_home(self):
        self.driver.get("https://breathe-easy.me")
        assert "BreatheEasy" in self.driver.title
        assert "Providing" in self.driver.page_source
        assert "NOT THERE" not in self.driver.page_source

    def test_countries(self):
        self.driver.get("https://breathe-easy.me/countries")
        expected_condition = EC.presence_of_element_located(
            (By.XPATH, "/html/body/div/div/div[2]/div/h1")
        )
        wait = WebDriverWait(
            self.driver,
            cities_timeout,
            poll_frequency=1,
            ignored_exceptions=[
                ElementNotVisibleException,
                ElementNotSelectableException,
            ],
        )
        wait.until(expected_condition)

        assert "Countries" in self.driver.page_source
        assert "NOT THERE" not in self.driver.page_source

    def test_countries_links(self):
        self.driver.get("http://breathe-easy.me/countries")
        element_present = EC.element_to_be_clickable((By.CLASS_NAME, "card"))
        wait = WebDriverWait(
            self.driver,
            timeout,
            poll_frequency=1,
            ignored_exceptions=[
                ElementNotVisibleException,
                ElementNotSelectableException,
            ],
        )
        to_click = wait.until(element_present)
        to_click.click()
        assert "AD" in self.driver.title
        assert "NOT THERE" not in self.driver.page_source

    def test_cities(self):
        self.driver.get("https://breathe-easy.me/cities")
        expected_condition = EC.presence_of_element_located(
            (By.XPATH, "/html/body/div/div/div[2]/div/h1")
        )
        wait = WebDriverWait(
            self.driver,
            cities_timeout,
            poll_frequency=1,
            ignored_exceptions=[
                ElementNotVisibleException,
                ElementNotSelectableException,
            ],
        )
        wait.until(expected_condition)
        assert "Cities" in self.driver.page_source
        assert "NOT THERE" not in self.driver.page_source

    def test_cities_links(self):
        self.driver.get("https://breathe-easy.me/cities")
        element_present = EC.element_to_be_clickable((By.CLASS_NAME, "card-body"))
        wait = WebDriverWait(
            self.driver,
            cities_timeout,
            poll_frequency=1,
            ignored_exceptions=[
                ElementNotVisibleException,
                ElementNotSelectableException,
            ],
        )
        to_click = wait.until(element_present)
        to_click.click()
        element_present = EC.element_to_be_clickable((By.CLASS_NAME, "table"))
        wait = WebDriverWait(
            self.driver,
            cities_timeout,
            poll_frequency=1,
            ignored_exceptions=[
                ElementNotVisibleException,
                ElementNotSelectableException,
            ],
        )
        wait.until(element_present)
        assert "Recent Air Quality Data" in self.driver.page_source
        assert "NOT THERE" not in self.driver.page_source

    def test_days(self):
        self.driver.get("https://breathe-easy.me/days")
        expected_condition = EC.presence_of_element_located(
            (By.XPATH, "/html/body/div/div/div[2]/div/h1")
        )
        wait = WebDriverWait(
            self.driver,
            cities_timeout,
            poll_frequency=1,
            ignored_exceptions=[
                ElementNotVisibleException,
                ElementNotSelectableException,
            ],
        )
        wait.until(expected_condition)
        assert "Days" in self.driver.page_source
        assert "NOT THERE" not in self.driver.page_source

    def test_days_links(self):
        self.driver.get("https://breathe-easy.me/days")
        element_present = EC.element_to_be_clickable((By.CLASS_NAME, "clickableRow"))
        wait = WebDriverWait(
            self.driver,
            timeout,
            poll_frequency=1,
            ignored_exceptions=[
                ElementNotVisibleException,
                ElementNotSelectableException,
            ],
        )
        to_click = wait.until(element_present)
        to_click.click()
        element_present = EC.element_to_be_clickable((By.CLASS_NAME, "card"))
        wait = WebDriverWait(
            self.driver,
            timeout,
            poll_frequency=1,
            ignored_exceptions=[
                ElementNotVisibleException,
                ElementNotSelectableException,
            ],
        )
        wait.until(element_present)
        assert "Cities with the worst AQI" in self.driver.page_source
        assert "NOT THERE" not in self.driver.page_source

    def test_countries_cities_link(self):
        self.driver.get("https://breathe-easy.me/countries")
        element_present = EC.element_to_be_clickable((By.CLASS_NAME, "card"))
        wait = WebDriverWait(
            self.driver,
            timeout,
            poll_frequency=1,
            ignored_exceptions=[
                ElementNotVisibleException,
                ElementNotSelectableException,
            ],
        )
        to_click = wait.until(element_present)
        to_click.click()
        element_present = EC.element_to_be_clickable(
            (By.CLASS_NAME, "embed-responsive")
        )
        wait = WebDriverWait(
            self.driver,
            timeout,
            poll_frequency=1,
            ignored_exceptions=[
                ElementNotVisibleException,
                ElementNotSelectableException,
            ],
        )
        wait.until(element_present)
        assert "Worst Air Quality Day" in self.driver.page_source
        assert "NOT THERE" not in self.driver.page_source
        element_present = EC.element_to_be_clickable((By.CLASS_NAME, "countrysCity"))
        wait = WebDriverWait(
            self.driver,
            timeout,
            poll_frequency=1,
            ignored_exceptions=[
                ElementNotVisibleException,
                ElementNotSelectableException,
            ],
        )
        to_click = wait.until(element_present)
        to_click.click()
        element_present = EC.element_to_be_clickable((By.CLASS_NAME, "table"))
        wait = WebDriverWait(
            self.driver,
            timeout,
            poll_frequency=1,
            ignored_exceptions=[
                ElementNotVisibleException,
                ElementNotSelectableException,
            ],
        )
        wait.until(element_present)
        assert "Recent Air Quality Data" in self.driver.page_source
        assert "NOT THERE" not in self.driver.page_source

    def test_countries_days_link(self):
        self.driver.get("https://breathe-easy.me/countries")
        element_present = EC.element_to_be_clickable((By.CLASS_NAME, "card"))
        wait = WebDriverWait(
            self.driver,
            timeout,
            poll_frequency=1,
            ignored_exceptions=[
                ElementNotVisibleException,
                ElementNotSelectableException,
            ],
        )
        to_click = wait.until(element_present)
        to_click.click()
        element_present = EC.element_to_be_clickable(
            (By.CLASS_NAME, "embed-responsive")
        )
        wait = WebDriverWait(
            self.driver,
            timeout,
            poll_frequency=1,
            ignored_exceptions=[
                ElementNotVisibleException,
                ElementNotSelectableException,
            ],
        )
        wait.until(element_present)
        assert "Worst Air Quality Day" in self.driver.page_source
        assert "NOT THERE" not in self.driver.page_source
        element_present = EC.element_to_be_clickable((By.CLASS_NAME, "countryDate"))
        wait = WebDriverWait(
            self.driver,
            timeout,
            poll_frequency=1,
            ignored_exceptions=[
                ElementNotVisibleException,
                ElementNotSelectableException,
            ],
        )
        to_click = wait.until(element_present)
        to_click.click()
        element_present = EC.element_to_be_clickable((By.CLASS_NAME, "card"))
        wait = WebDriverWait(
            self.driver,
            timeout,
            poll_frequency=1,
            ignored_exceptions=[
                ElementNotVisibleException,
                ElementNotSelectableException,
            ],
        )
        wait.until(element_present)
        assert "Cities with the worst" in self.driver.page_source
        assert "NOT THERE" not in self.driver.page_source

    def test_cities_country_link(self):
        self.driver.get("https://breathe-easy.me/cities")
        element_present = EC.element_to_be_clickable((By.CLASS_NAME, "card"))
        wait = WebDriverWait(
            self.driver,
            cities_timeout,
            poll_frequency=1,
            ignored_exceptions=[
                ElementNotVisibleException,
                ElementNotSelectableException,
            ],
        )
        to_click = wait.until(element_present)
        to_click.click()
        element_present = EC.element_to_be_clickable((By.CLASS_NAME, "table"))
        wait = WebDriverWait(
            self.driver,
            timeout,
            poll_frequency=1,
            ignored_exceptions=[
                ElementNotVisibleException,
                ElementNotSelectableException,
            ],
        )
        wait.until(element_present)
        assert "Recent Air Quality Data" in self.driver.page_source
        assert "NOT THERE" not in self.driver.page_source
        element_present = EC.element_to_be_clickable((By.CLASS_NAME, "citysCountry"))
        wait = WebDriverWait(
            self.driver,
            timeout,
            poll_frequency=1,
            ignored_exceptions=[
                ElementNotVisibleException,
                ElementNotSelectableException,
            ],
        )
        to_click = wait.until(element_present)
        to_click.click()
        element_present = EC.element_to_be_clickable(
            (By.CLASS_NAME, "embed-responsive")
        )
        wait = WebDriverWait(
            self.driver,
            cities_timeout,
            poll_frequency=1,
            ignored_exceptions=[
                ElementNotVisibleException,
                ElementNotSelectableException,
            ],
        )
        wait.until(element_present)
        assert "Worst Air Quality Day" in self.driver.page_source
        assert "NOT THERE" not in self.driver.page_source

    def test_cities_days_link(self):
        self.driver.get("https://breathe-easy.me/cities")
        element_present = EC.element_to_be_clickable((By.CLASS_NAME, "card"))
        wait = WebDriverWait(
            self.driver,
            cities_timeout,
            poll_frequency=1,
            ignored_exceptions=[
                ElementNotVisibleException,
                ElementNotSelectableException,
            ],
        )
        to_click = wait.until(element_present)
        to_click.click()
        element_present = EC.element_to_be_clickable((By.CLASS_NAME, "table"))
        wait = WebDriverWait(
            self.driver,
            timeout,
            poll_frequency=1,
            ignored_exceptions=[
                ElementNotVisibleException,
                ElementNotSelectableException,
            ],
        )
        wait.until(element_present)
        assert "Recent Air Quality Data" in self.driver.page_source
        assert "NOT THERE" not in self.driver.page_source
        element_present = EC.element_to_be_clickable((By.CLASS_NAME, "clickableRow"))
        wait = WebDriverWait(
            self.driver,
            timeout,
            poll_frequency=1,
            ignored_exceptions=[
                ElementNotVisibleException,
                ElementNotSelectableException,
            ],
        )
        to_click = wait.until(element_present)
        to_click.click()
        element_present = EC.element_to_be_clickable((By.CLASS_NAME, "card"))
        wait = WebDriverWait(
            self.driver,
            timeout,
            poll_frequency=1,
            ignored_exceptions=[
                ElementNotVisibleException,
                ElementNotSelectableException,
            ],
        )
        wait.until(element_present)
        assert "Cities with the worst" in self.driver.page_source
        assert "NOT THERE" not in self.driver.page_source

    def test_about(self):
        self.driver.get("https://breathe-easy.me/about")
        element_present = EC.element_to_be_clickable((By.CLASS_NAME, "member-deck"))
        wait = WebDriverWait(
            self.driver,
            timeout,
            poll_frequency=1,
            ignored_exceptions=[
                ElementNotVisibleException,
                ElementNotSelectableException,
            ],
        )
        wait.until(element_present)
        assert "About" in self.driver.title
        assert "Meet the Team" in self.driver.page_source
        assert "NOT THERE" not in self.driver.page_source

    def test_countries_search(self):
        self.driver.get("https://breathe-easy.me/countries")
        element_present = EC.element_to_be_clickable((By.CLASS_NAME, "search-submit"))
        wait = WebDriverWait(
            self.driver,
            timeout,
            poll_frequency=1,
            ignored_exceptions=[
                ElementNotVisibleException,
                ElementNotSelectableException,
            ],
        )
        search_submit = wait.until(element_present)
        search_submit.click()
        assert "SEARCH RESULTS" in self.driver.page_source
        assert "NOT THERE" not in self.driver.page_source

    def test_cities_search(self):
        self.driver.get("https://breathe-easy.me/cities")
        element_present = EC.element_to_be_clickable((By.CLASS_NAME, "search-submit"))
        wait = WebDriverWait(
            self.driver,
            timeout,
            poll_frequency=1,
            ignored_exceptions=[
                ElementNotVisibleException,
                ElementNotSelectableException,
            ],
        )
        search_submit = wait.until(element_present)
        search_submit.click()
        assert "SEARCH RESULTS" in self.driver.page_source
        assert "NOT THERE" not in self.driver.page_source

    def test_days_search(self):
        self.driver.get("https://breathe-easy.me/days")
        element_present = EC.element_to_be_clickable((By.CLASS_NAME, "search-submit"))
        wait = WebDriverWait(
            self.driver,
            timeout,
            poll_frequency=1,
            ignored_exceptions=[
                ElementNotVisibleException,
                ElementNotSelectableException,
            ],
        )
        search_submit = wait.until(element_present)
        search_submit.click()
        assert "SEARCH RESULTS" in self.driver.page_source
        assert "NOT THERE" not in self.driver.page_source


if __name__ == "__main__":
    unittest.main()
