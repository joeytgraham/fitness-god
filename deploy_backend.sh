echo "Deploying Backend..."
cd backend
aws ecr get-login-password --region us-west-2 | docker login --username AWS --password-stdin 898865685982.dkr.ecr.us-west-2.amazonaws.com
docker build -t fitness-god-backend .
docker tag fitness-god-backend:latest 898865685982.dkr.ecr.us-west-2.amazonaws.com/fitness-god-backend:latest
docker push 898865685982.dkr.ecr.us-west-2.amazonaws.com/fitness-god-backend:latest
cd aws_deploy
eb deploy